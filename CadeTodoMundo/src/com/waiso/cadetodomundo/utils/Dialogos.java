/**
 * 
 */
package com.waiso.cadetodomundo.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;







//import com.waiso.kidz.CadastroMovimentoActivity;
import com.waiso.cadetodomundo.R;
import com.waiso.cadetodomundo.abstratas.Classe;
import com.waiso.cadetodomundo.abstratas.ClasseActivity;
import com.waiso.cadetodomundo.abstratas.ClasseFragmentoActivity;
import com.waiso.cadetodomundo.abstratas.ContextoPadrao;


/**
 * @author Alex
 *
 */
public class Dialogos extends Classe{

	public static class Alerta {
		static AlertDialog dialogo;

		/**
		 * Exibe mensagem de Alerta.<br/>
		 * <b>Lembre-se de chamar o metodo fecharDialogo()</b>
		 * @param contexto
		 * @param cancelavel
		 * @param mensagem
		 * @param titulo
		 * @param escutadorOk
		 */
		public static void exibirMensagemInformacao(
				final Context contexto, 
				final boolean cancelavel, 
				final CharSequence mensagem, 
				final CharSequence titulo, 
				final OnClickListener escutadorOk){
			AlertDialog.Builder construtor = new AlertDialog.Builder(contexto).setIconAttribute(android.R.drawable.ic_dialog_info)
					.setCancelable(cancelavel)
					.setMessage(mensagem)
					.setTitle(titulo)
					.setPositiveButton("Ok!", escutadorOk);

			(dialogo = construtor.create()).show();
		}
		
		/**
		 * Exibe mensagem de Alerta.<br/>
		 * <b>Lembre-se de chamar o metodo fecharDialogo()</b>
		 * @param contexto
		 * @param cancelavel
		 * @param mensagem
		 * @param titulo
		 * @param escutadorOk
		 */
		public static void exibirMensagemInformacao(ContextoPadrao contexto, boolean cancelavel, 
				CharSequence mensagem, OnClickListener escutadorOk){
			exibirMensagemInformacao(contexto.getContexto(), cancelavel, mensagem, contexto.getContexto().getString(R.string.app_name), escutadorOk);			
		}
		public static void exibirMensagemPergunta(Context contexto, boolean cancelavel, 
				CharSequence mensagem, CharSequence titulo, 
				OnClickListener comportamentoSim, OnClickListener comportamentoNao, 
				OnClickListener comportamentoCancelar){
			AlertDialog.Builder construtor = new AlertDialog.Builder(contexto).setIconAttribute(android.R.drawable.ic_menu_zoom)
					.setCancelable(cancelavel)
					.setMessage(mensagem)
					.setPositiveButton("Sim", comportamentoSim)
					.setNeutralButton("Cancelar", comportamentoCancelar)
					.setNegativeButton("Não", comportamentoNao);
			if (existe(titulo)) {
				construtor.setTitle(titulo);
			} else {
				construtor.setTitle(contexto.getResources().getString(R.string.app_name));
			}

			(dialogo = construtor.create()).show();

		}

		public static void fecharDialogo(){
			if (existe(dialogo)) {
				dialogo.dismiss();
			}
		}

		public static void exibirMensagemErro(Throwable e, Context contexto, OnClickListener acao) {
			exibirMensagemInformacao(contexto, false, "Um erro ocorreu: \n"+e.getMessage(), "Ocorreu um erro durante a operação", acao);
		}
	}
	
	public static class Progresso {
//		static View dialogoProcessamento = null;
		static ProgressDialog dialogo;
		static Activity telaAlvo = null;
		
		/**
		 * Mostra um Dialogo de Progresso de Processamento<br>
		 * <b>Lembre-se de chamar o metodo de fechamento</b>
		 * @param contextoPadrao
		 */
		@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
		public static void exibirDialogoProgresso(ClasseActivity tela){
			telaAlvo = tela;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
//				LayoutInflater inflador = (LayoutInflater) tela.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//				dialogoProcessamento = inflador.inflate(R.layout.dialogo_progresso, null);
//				tela.addContentView(dialogoProcessamento, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//				int shortAnimTime = tela.getResources().getInteger(android.R.integer.config_shortAnimTime);
//				dialogoProcessamento.setVisibility(View.VISIBLE);
//				dialogoProcessamento.animate().setDuration(shortAnimTime)
//					.alpha(1)
//					.setListener(new AnimatorListenerAdapter() {
//						@Override
//						public void onAnimationEnd(Animator animation) {
//							dialogoProcessamento.setVisibility(View.VISIBLE);
//						}
//				});
				
				dialogo = new ProgressDialog(tela);
				dialogo.setTitle("Processando");
			}
		}
		
		/**
		 * Mostra um Dialogo de Progresso de Processamento<br>
		 * <b>Lembre-se de chamar o metodo de fechamento</b>
		 * @param contextoPadrao
		 */
		@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
		public static void exibirDialogoProgresso(ClasseFragmentoActivity tela){
			telaAlvo = tela;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
//				LayoutInflater inflador = (LayoutInflater) tela.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//				dialogoProcessamento = inflador.inflate(R.layout.dialogo_progresso, null);
//				tela.addContentView(dialogoProcessamento, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//				int shortAnimTime = tela.getResources().getInteger(android.R.integer.config_shortAnimTime);
//				dialogoProcessamento.setVisibility(View.VISIBLE);
//				dialogoProcessamento.animate().setDuration(shortAnimTime)
//				.alpha(1)
//				.setListener(new AnimatorListenerAdapter() {
//					@Override
//					public void onAnimationEnd(Animator animation) {
//						dialogoProcessamento.setVisibility(View.VISIBLE);
//					}
//				});
				
				dialogo = new ProgressDialog(tela);
				dialogo.setTitle("Processando");
			}
		}
		
		@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
		public static void fecharDialogoProgresso(){
			if (existe(dialogo)) {
//			if (existe(dialogoProcessamento)) {
//				final int shortAnimTime = telaAlvo.getResources().getInteger(android.R.integer.config_shortAnimTime);
//				
//				Runnable animacao = new Runnable() {
//					@Override
//					public void run() {
//						dialogoProcessamento.animate().setDuration(shortAnimTime)
//						.alpha(0)
//						.setListener(new AnimatorListenerAdapter() {
//							@Override
//							public void onAnimationEnd(Animator animation) {
//								dialogoProcessamento.setVisibility(View.GONE);
//							}
//						});
//					}
//				};
//				
//				AsyncTask.execute(animacao);
				if (existe(dialogo)) {
					dialogo.dismiss();
				}
			}
		}
	}
	
	public static class Notificacao{

		public static void exibir(Activity tela, String string, Bundle parametros) {
			/*NotificationManager notificationManager ;
			String serName = Context.NOTIFICATION_SERVICE ;
			notificationManager = ( NotificationManager ) tela.getSystemService( serName ) ;

			int icon = R.drawable.btn_star_big_on_selected ;
			String tickerText = "1. Minha notificacao" ;
			long when = System.currentTimeMillis( ) ;
			Notification notification = new Notification( icon, tickerText, when ) ;

			String extendedTitle = "2. Meu titulo" ;
			String extendedText = "3. Essa � uma mensagem muito importante" ;

			Intent intent = new Intent( tela.getApplicationContext( ), CadastroMovimentoActivity.class ) ;
			intent.putExtras(parametros);
			PendingIntent launchIntent = PendingIntent.getActivity( tela, 0, intent, 0 ) ;
			notification.setLatestEventInfo( tela.getApplicationContext( ), extendedTitle,	extendedText, launchIntent ) ;
			int notificationId = 1 ;
			notificationManager.notify( notificationId, notification ) ;*/
			
		}
		
	}
}
