package com.waiso.cadetodomundo.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.waiso.cadetodomundo.abstratas.Classe;
import com.j256.ormlite.field.DatabaseField;

/**
 * Esta classe realiza a leitura do Bean passado
 * e cria as queries de criação de entidade baseado
 * nos atributos dela.
 * @author Alex
 *
 */
public class GeradorSQLBean<T> extends Classe {
	
	private static final String CREATE_TABLE_SIMPLES = "create table ";
	private static final String DROP_TABLE_SIMPLES = "drop table ";
	private static final String INSERT_INTO_SIMPLES = "insert into ";
	private static final String UPDATE_SIMPLES = "update ";
	private static final String DELETE_FROM_SIMPLES = "delete from ";
	private static final String SELECT_FROM_SIMPLES = "select from ";
	private final Class<T> objeto;
	private Class pai;
	
	private final List<Field> camposPK = new ArrayList<Field>();
	
	GeradorSQLBean(Class objeto){
		this.objeto = objeto;
		if(this.objeto != null){
			for(Field f: objeto.getDeclaredFields()){
				if(getCampo(f).contains("primary key")){
					camposPK.add(f);
				}
			}
		}
	}
	
	public String getCreateTable() {
		//Iniciando o comando
		StringBuilder comando = getComandoInicial(CREATE_TABLE_SIMPLES).append(" (");
		
		//Colocando os nomes dos campos
//		System.out.println("Campos: ");
		Field[] campos = objeto.getDeclaredFields();
		List<Field> fks = new ArrayList<Field>();
		
		/**
		 * 1° Parte - Cuidamos dos campos
		 */
		int quantidadeDeCampos = 0;
		for(int cont=0; cont < campos.length; cont++){
			
			Field campo = campos[cont];
			Annotation[] anotacoes = campo.getAnnotations();
			if(!getCampo(campo).contains("serialVersionUID") && getCampo(campo).trim().length()!=0){
				String nomeCampo = campo.getName();
				String tipoCampo = campo.getType().getName();
//				System.out.println(nomeCampo + " (" + campo.getType().getSimpleName() + ")");
				
				StringBuilder linhaCampo = new StringBuilder();
				
				linhaCampo.append(getCampo(campo));
				
				if(linhaCampo.toString().trim().length()!=0){
					if(quantidadeDeCampos > 0){
						comando.append(", \n");
					} else{
						comando.append(" \n");
					}
					comando.append(linhaCampo.toString());
				}
				
				/** Verificamos se o tipo do campo começa com 'com',
				 * um indicativo de que estamos lidando com uma chave
				 * Estrangeira (FK)
				 */
				if(tipoCampo.startsWith("com")){
					fks.add(campo);
				}
				
				quantidadeDeCampos++;
			}
		}
		
//		System.out.println();
		/**
		 * 2ª Parte: Agora lidamos com as FKs
		 */
		/*for(Field f: fks){
			comando.append(",\n").append(getChaveEstrangeira(f));
		}*/
		
		comando.append(")");
		
//		System.out.println(comando);
		
		log(comando.toString());
		return comando.toString();
	}
	

	public String getComandoInsercao(T o){
		StringBuilder comando = new StringBuilder(getComandoInicial(INSERT_INTO_SIMPLES));
		
		//Primeiro, cuidamos de pegar todas as colunas
		comando.append(" (");
		boolean primeiroCampo = true;
		for (Field campo : objeto.getDeclaredFields()) {
			if (!primeiroCampo) {
				 comando.append(", ");
			}
			comando.append(getCampoNormal(campo));
			primeiroCampo = false;
		}
		comando.append(") ");
		comando.append("values");

		//Entao, pegamos seus respectivos valores
		comando.append(" (");
		//Neste passo, pegamos os getters
		primeiroCampo = true;
		for (Field campo : objeto.getDeclaredFields()) {
			if (!primeiroCampo) {
				 comando.append(", ");
			}
			String campoGetter = (getCampoNormal(campo));
			Character primeiroCaracter = campoGetter.charAt(0);
			campoGetter = "get"+primeiroCaracter.toUpperCase(primeiroCaracter)+campoGetter.substring(1);
			Object valor = null;
			try {
				Method metodo = o.getClass().getMethod(campoGetter, null);
				valor = metodo.invoke(null, new Object[]{});
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				System.out.println("O Metodo nao existe ou esta com outra nomenclatura. Implemente o getter com a IDE Eclipse");
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			comando.append(valor);
			primeiroCampo = false;
		}
		comando.append(") ");
		
		//
		return comando.toString();
	}
	
	
	public String getComandoAtualizacao(){
		String comando = getComandoInicial(UPDATE_SIMPLES).toString();
		return comando;
	}
	
	public String getComandoExclusao(){
		String comando = getComandoInicial(DELETE_FROM_SIMPLES).toString();
		return comando;
	}
	
	public String getComandoSelecao(){
		String comando = getComandoInicial(SELECT_FROM_SIMPLES).toString();
		return comando;
	}

	/**
	 * @param comando
	 * @param fks
	 * @param campo
	 * @param anotacoes
	 * @param tipoCampo
	 */
	private String getCampo(Field campo){
		StringBuilder txtcampo = new StringBuilder();
		String tipoCampo = campo.getType().getName();
		
		/*boolean ehChavePrimaria = false;
		ehChavePrimaria = campo.getAnnotation(DatabaseField.class).id();
		String chavePrimaria = "";
		if (ehChavePrimaria) {
			chavePrimaria = " primary key ";
		}
		
		boolean ehAutoIncremental = false;
		ehAutoIncremental = campo.getAnnotation(DatabaseField.class).generatedId();
		String autoIncremental = "";
		if (ehAutoIncremental) {
			autoIncremental = " autoincrement ";
		}*/
		
		/** Verificamos se o tipo do campo começa com 'com',
		 * um indicativo de que estamos lidando com uma chave
		 * Estrangeira (FK)
		 */
		if(!campo.getName().contains("serialVersionUID") && !tipoCampo.contains("List")){
			if((!tipoCampo.startsWith("com") || tipoCampo.startsWith("java.util") && !tipoCampo.contains("List") || 
					campo.getAnnotations().length == 0) || campo.getType().isPrimitive()){
				txtcampo.append(getCampoNormal(campo).trim()).append(" ").append(getTipoCampo(campo).trim()).append(" ").append(getAtributosDeAnotacoes(campo.getAnnotations()).trim());
			}/*else {
				txtcampo.append(getCampoChaveEstrangeira(campo).trim()).append(" ").append(getTipoCampo(campo).trim()).append(" ").append(getAtributosDeAnotacoes(campo.getAnnotations()).trim());
			}*/
		}
		
		return txtcampo.toString();
	}
	
	private String getCampoNormal(Field campo){
		/**
		 * No caso de Chave Estrangeira, fazemos a extração do ID
		 * do tipo informado
		 */
		StringBuilder txtcampo = new StringBuilder();
		txtcampo.append(campo.getName());
		return txtcampo.toString();
	}
	private String getCampoChaveEstrangeira(Field campo){
		/**
		 * No caso de Chave Estrangeira, fazemos a extração do ID
		 * do tipo informado
		 */
		StringBuilder txtcampo = new StringBuilder();
		if(!(campo.getType().isInterface() && (campo.getType().getSimpleName().equalsIgnoreCase("List") || 
				campo.getType().getSimpleName().equalsIgnoreCase("List")))){
			txtcampo.append(campo.getName()).append("_id ");
		}
		return txtcampo.toString();
	}
	
	/**
	 * Campo que interpreta a chave estrangeira,
	 * construindo a constraint
	 * @param campo
	 * @return
	 */
	private String getChaveEstrangeira(Field campo){
		if(campo != null){
			String nomeCampo = campo.getName();
			
			StringBuilder constraint = new StringBuilder();
			/**
			 * Se o nosso campo tiver implementado uma Interface com Generics,
			 * detectamos aqui e já extraimos o tipo do campo.
			 */
			
			String tipoCampo = campo.getType().getSimpleName();

			constraint.append("constraint fk_").append(tipoCampo).append("_").append(campo.getDeclaringClass().getSimpleName());
			constraint.append(" (").append(nomeCampo).append(") references ").append("tb_").append(tipoCampo);
			constraint.append(" (id)");

			return constraint.toString();
		}
		return "";
	}
	
	/**
	 * Metodo que transforma os tipos de dados
	 * do Java em tipos de dado de Banco
	 * @param campo
	 * @return
	 */
	private String getTipoCampo(Field campo) {
		Class classeTipo = null;
		if(!campo.getType().isPrimitive()){
			classeTipo = campo.getType();
			if(classeTipo == String.class){
				return "text";
			} else if(classeTipo == Long.class || classeTipo == Boolean.class || classeTipo == Integer.class){
				return "INTEGER";
			} else if(classeTipo.getName().contains("com.")){
				return "INTEGER";
			} else if (classeTipo == Calendar.class) {
				return "text";
			} else if (classeTipo == BigDecimal.class) {
				return "text";
			}
		} else {
			if(!campo.getName().contains("serialVersionUID")){
				if(campo.getType().getName().equalsIgnoreCase("long") ||
						campo.getType().getName().equalsIgnoreCase("boolean") ||
						campo.getType().getName().equalsIgnoreCase("int")){
					return "INTEGER";
				} else if (campo.getType().getName().equalsIgnoreCase("double") ||
						campo.getType().getName().equalsIgnoreCase("long")) {
					return "text";
				}
			}
		}
		return "";
	}
	
	/**
	 * Caso o Bean tenha anotações, estas são interpretadas
	 * para atributos de coluna no banco de dados.
	 * @param anotacoes
	 * @return
	 */
	private String getAtributosDeAnotacoes(Annotation[] anotacoes) {
		StringBuilder sb = new StringBuilder();
		String pk = "";
		StringBuilder nullable = new StringBuilder("");
		String auto_incremento = "";
		String unique = "";
		
		if(anotacoes != null && anotacoes.length > 0){
			for(Annotation anotacao: anotacoes){
				if(anotacao instanceof DatabaseField){
					DatabaseField a = (DatabaseField) anotacao;
					if(a.id()){
						pk = " primary key";
					}
					if(!a.canBeNull()){
						nullable.insert(0, " not null");
					}
					if(a.generatedId()){
						auto_incremento =" autoincrement";
					}
					if(a.unique()){
						unique = " unique";
					}
				}
			}
		}
		
		sb.append(pk).append(nullable.toString()).append(auto_incremento).append(unique);
		
		return sb.toString();
	}
	
	public String getDropTable(){
		return getComandoInicial(DROP_TABLE_SIMPLES).toString();
	}
	
	public List<Field> getCampos(){
		List<Field> campos = Arrays.asList(objeto.getDeclaredFields());
		return campos;
	}
	
	/**
	 * @return
	 */
	public String getNomeTabela() {
		return "tb_"+objeto.getSimpleName().toLowerCase(new Locale("pt", "BR"));
	}
	
	private StringBuilder getComandoInicial(String qualComando){
		StringBuilder sb = new StringBuilder(qualComando);
		sb.append(getNomeTabela());
		return sb;
	}
	
	public static GeradorSQLBean getInstancia(Class tipo){
		return new GeradorSQLBean(tipo);
	}
	
	public static void main(String[] args) {
//		System.out.println(GeradorSQLBean.getInstancia(Usuario.class).getCreateTable());
//		System.out.println(GeradorSQLBean.getInstancia(Usuario.class).getDropTable());
//		System.out.println(GeradorSQLBean.getInstancia(Usuario.class).getInsert());
	}

}
