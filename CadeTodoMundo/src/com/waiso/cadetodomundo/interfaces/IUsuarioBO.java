/**
 * 
 */
package com.waiso.cadetodomundo.interfaces;

import com.waiso.cadetodomundo.orm.modelos.Usuario;


/**
 * @author Alex
 *
 */
public interface IUsuarioBO extends IBO<Usuario> {
	
}
