package com.waiso.cadetodomundo.interfaces;

import com.waiso.cadetodomundo.excecoes.Erro;

public interface IBO<T> {

	public T inserir(T o) throws Erro;
	public void alterar(T o) throws Erro;
	public void excluir(T o) throws Erro;
	
	
}
