package com.waiso.cadetodomundo.controladores;

import com.waiso.cadetodomundo.abstratas.Classe;
import com.waiso.cadetodomundo.abstratas.ContextoGenerico;
import com.waiso.cadetodomundo.abstratas.ContextoPadrao;

public abstract class ControladorDeVO<T> extends Classe{
	
	public ContextoPadrao contextoPadrao;
	public ContextoGenerico contextoGenerico;
	public abstract void encriptaVO(T o);
	
	public ContextoPadrao getContextoPadrao() {
		return contextoPadrao;
	}
	public void setContextoPadrao(ContextoPadrao contextoPadrao) {
		this.contextoPadrao = contextoPadrao;
	}
	public ContextoGenerico getContextoGenerico() {
		return contextoGenerico;
	}
	public void setContextoGenerico(ContextoGenerico contextoGenerico) {
		this.contextoGenerico = contextoGenerico;
	}
	
}
