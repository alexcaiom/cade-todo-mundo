package com.waiso.cadetodomundo.controladores;

import java.util.Calendar;
import java.util.List;

import com.waiso.cadetodomundo.abstratas.ContextoGenerico;
import com.waiso.cadetodomundo.abstratas.ContextoPadrao;
import com.waiso.cadetodomundo.enums.EnumUsuarioAutenticado;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.excecoes.SysErr;
import com.waiso.cadetodomundo.facade.FacadeUsuario;
import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;
import com.waiso.cadetodomundo.orm.modelos.Usuario;

public class ControladorDeUsuario extends ControladorDeVO<Usuario>{
	
	private static ControladorDeUsuario instancia;
	
	
	public void login(String login, String senha) throws Erro {
		try{
			FacadeUsuario.getInstancia(contextoPadrao).logar(login, senha);
		}catch (Erro e){
			throw e;
		}
	}
	
	public void logout(String login) throws Erro {
		FacadeUsuario.getInstancia(contextoPadrao).deslogar(login);
		
	}
	
	public boolean sessaoValida(String login, String senha) throws Erro {
		boolean usuarioEstaLogadoNoServidor = FacadeUsuario.getInstancia(contextoPadrao).verificaSessao(login, senha);
		return usuarioEstaLogadoNoServidor;
	}
	
	public void cadastrar(String nome, String email, String login, String senha, Calendar data, String imei) throws Erro{
		Usuario usuario = new Usuario(login, senha, EnumUsuarioAutenticado.SUCESSO.getCodigo());
		usuario.comDataDeNascimento(data);
		usuario.comNome(nome);
		usuario.comEmail(email);
		usuario.comImei(imei);
		//TODO Bloco de Desenvolvimento
//		if (usuario.getLogin().contains("mah")) {
//			usuario.comLatitude();
//			usuario.comLongitude();
//		} else if (usuario.getLogin().contains("thiagao")) {
//			usuario.comLatitude();
//			usuario.comLongitude();
//		}
		//TODO Bloco de Desenvolvimento
		encriptaVO(usuario);
		FacadeUsuario.getInstancia(contextoPadrao).cadastrar(usuario);
		
	}
	
	public void alterar(long id, String nome, String email, String login, String senha, Calendar data, String imei) throws Erro{
		Usuario usuario = new Usuario(login, senha, EnumUsuarioAutenticado.SUCESSO.getCodigo());
		usuario.comId(id);
		usuario.comDataDeNascimento(data);
		usuario.comNome(nome);
		usuario.comEmail(email);
		usuario.comImei(imei);
		encriptaVO(usuario);
		FacadeUsuario.getInstancia(contextoPadrao).alterar(usuario);
	}
	
	public boolean associarUsuarios(Usuario eu, Usuario amigo) throws Erro {
		return FacadeUsuario.getInstancia(contextoPadrao).associarUsuarios(eu, amigo);
	}
	
	public boolean confirmarSolicitacao(Usuario eu, Usuario amigo) throws Erro {
		return FacadeUsuario.getInstancia(contextoPadrao).confirmarSolicitacao(eu, amigo);
	}
	
	public List<Usuario> listar() throws Exception, Erro  {
		if (existe(contextoGenerico)) {
			return FacadeUsuario.getInstancia(contextoGenerico).listar();
		}
		return FacadeUsuario.getInstancia(contextoPadrao).listar();
	}
	
	public List<Usuario> listarAmigosComNomeComo(String nome) throws Exception, Erro {
		return FacadeUsuario.getInstancia(contextoGenerico).listarAmigosComNomeComo(nome);
	}
	
	public List<Usuario> listarOutrosUsuariosComNomeComo(String nome) throws Exception, Erro {
		return FacadeUsuario.getInstancia(contextoGenerico).listarOutrosUsuariosComNomeComo(nome);
	}
	
	public List<SolicitacaoDeAmizade> listarSolicitacoesDeAmizade() throws Exception, Erro {
		return FacadeUsuario.getInstancia(contextoGenerico).listarSolicitacoesDeAmizade();
	}
	
	public static ControladorDeUsuario getInstancia(ContextoGenerico contexto){
		if (naoExiste(instancia)) {
			instancia = new ControladorDeUsuario();
			instancia.setContextoGenerico(contexto);
		}
		return instancia;
	}
	
	public static ControladorDeUsuario getInstancia(ContextoPadrao contexto){
		if (naoExiste(instancia)) {
			instancia = new ControladorDeUsuario();
			instancia.setContextoPadrao(contexto);
		}
		return instancia;
	}

	@Override
	public void encriptaVO(Usuario o) {
		
		
	}
}
