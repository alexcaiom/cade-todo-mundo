/**
 * 
 */
package com.waiso.cadetodomundo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.waiso.cadetodomundo.abstratas.ClasseActivity;
import com.waiso.cadetodomundo.controladores.ControladorDeUsuario;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.interfaces.ClasseActivityInterface;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Dialogos;

/**
 * @author AlexDell
 *
 */
public class LoginActivity extends ClasseActivity  implements ClasseActivityInterface{
	
	private static final Class DESTINO = MainActivity.class;
	Button btnLogin, btnCadastro;//, btnListarTodos, btnListarSolicitacoes;
	EditText txtUsuario,  txtSenha;
	TextView lblDadosInvalidos;
	Handler handler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		carregarTela();
	}
	
	public void carregarTela(){
		btnLogin = (Button) findViewById(R.id.login_btnLogin);
		btnCadastro = (Button) findViewById(R.id.login_btnCadastro);
//		btnListarTodos = (Button) findViewById(R.id.login_btnListarTodos);
//		btnListarSolicitacoes = (Button) findViewById(R.id.login_btnListarSolicitacoes);
		txtUsuario = (EditText) findViewById(R.id.txtLogin);
		txtSenha = (EditText) findViewById(R.id.txtSenha);
		lblDadosInvalidos = (TextView) findViewById(R.id.lblLoginInvalido);
		ocultarBarraDeAcoes();
		carregarEventos();
	}
	
	public void carregarEventos(){
		txtUsuario.requestFocus();
		btnLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (txtUsuario.getText().toString().isEmpty()
						|| txtSenha.getText().toString().isEmpty()) {
					exibirLabelErroLoginComMensagem("Login e senha devem ser preenchidos.");
					return;
				}
				logar();
//				Dialogos.Alerta.exibirMensagemInformacao(contexto, false, getString(R.string.mensagem_app_em_desenvolvimento), null);
			}
		});
		
		btnCadastro.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				irPara(CadastroUsuarioActivity.class);
			}
		});
		
		txtUsuario.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				ocultarLabelErro();
				return false;
			}
		});
		
		txtSenha.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				lblDadosInvalidos.setVisibility(View.GONE);
				if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {
					btnLogin.requestFocus();
				} else {
					ocultarLabelErro();
				}
				return false;
			}
		});
		
//		btnListarTodos.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				StringBuilder sb = new StringBuilder();
//				try {
//					String params = "";
//					Object resultado = new TarefaListagem().execute(params).get();
//					if (existe(resultado)) {
//						if (resultado instanceof ArrayList<?>) {
//							List<Usuario> usuarios = (List<Usuario>) resultado; 
//							boolean primeiro = true;
//							for (Usuario usuario : usuarios) {
//								if (!primeiro) {
//									sb.append("\n");
//								}
//								sb.append(usuario.getLogin()).append(" - ").append(usuario.getSenha());
//								primeiro = false;
//							}
//						} else if (resultado instanceof Exception) {
//							((Exception)resultado).printStackTrace();
//						}
//					}
//				} catch (Exception e) {
//					Dialogos.Alerta.exibirMensagemErro(e, contexto.getContexto(), null);
//				}
//				Dialogos.Alerta.exibirMensagemInformacao(getContexto(), false, sb.toString(), null);
//			}
//		});
		
//		btnListarSolicitacoes.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				StringBuilder sb = new StringBuilder();
//				try {
//					List<Object[]> solicitacoes = FinderUsuario.getInstancia(contexto).listarTodasAsSolicitacoesDeAmizade();
//					if (existe(solicitacoes)) {
//						boolean primeiro = true;
//						for (Object[] solicitacao : solicitacoes) {
//							if (!primeiro) {
//								sb.append("\n");
//							}
//							
//							Usuario usuario = (Usuario) solicitacao[0];
//							Usuario amigo = (Usuario) solicitacao[1];
//							Boolean jaEhAmigo = (Boolean) solicitacao[2];
//							
//							sb.append(usuario.getLogin()).append(" - ").append(amigo.getLogin()).append(" - ").append(((jaEhAmigo)?"Confirmado":"Solicitacao"));
//							primeiro = false;
//						}
//					}
//				} catch (Exception e) {
//					Dialogos.Alerta.exibirMensagemErro(e, contexto.getContexto(), null);
//				}
//				Dialogos.Alerta.exibirMensagemInformacao(getContexto(), false, sb.toString(), null);
//			}
//		});
	}

	private void logar() {
		final String login = txtUsuario.getText().toString().trim();
		final String senha = txtSenha.getText().toString().trim();
		
//		try {
//			
//		} catch (Erro e) {
//			Dialogos.Progresso.fecharDialogoProgresso();
//			if (e instanceof ErroNegocio) {
//				/**
//				 * Aqui podemos ter um login invalido
//				 */
//				if (e.getMessage().equals(Constantes.DADOS_LOGIN_INVALIDOS)) {
//					Dialogos.Alerta.exibirMensagemInformacao(LoginActivity.this, false, e.getMessage(), "Atenção!", null);
//					txtSenha.requestFocus();
//					exibirLabelErroLoginComMensagem(Constantes.DADOS_LOGIN_INVALIDOS);
//				} else {
//					String aviso = e.getMessage();
//					aviso = aviso.replace("{", "").replace("}", "").replace("erro", "").replace("\"", "").replace(":", "");
//					Dialogos.Alerta.exibirMensagemInformacao(LoginActivity.this, false, aviso, "Atenção!", null);
//				}
//			} else if (e instanceof SysErr) {
//				Dialogos.Alerta.exibirMensagemErro(e, LoginActivity.this, null);
//				Dialogos.Alerta.fecharDialogo();
//			}
			
			
//		} catch (Exception e) {
//			Dialogos.Progresso.fecharDialogoProgresso();
//			Dialogos.Alerta.exibirMensagemErro(e, LoginActivity.this, null);
//		}
		
		try {
			Object resultado = new TarefaLogin().execute(login, senha).get();
			if (resultado instanceof Erro) {
				Dialogos.Alerta.exibirMensagemErro((Throwable)resultado, contexto.getClasseContexto(), null);
				return;
			}
			irPara(DESTINO);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		Runnable thread = new Runnable() {
//			public void run() {
//				try {
//					Dialogos.Progresso.fecharDialogoProgresso();
//					
//					android.content.DialogInterface.OnClickListener acaoOK = new android.content.DialogInterface.OnClickListener() {
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							irPara(DESTINO);
//						}
//					};
//					Dialogos.Alerta.exibirMensagemInformacao(getContexto(), false, "Logado com sucesso", acaoOK);
//					} catch (Erro e) {
//						
//					}
//			}
//		};
		
//		new Thread(thread).start();
//		new Handler(Looper.getMainLooper()).post(thread);
		
	}
	
	private void exibirLabelErroLoginComMensagem(String texto){
		lblDadosInvalidos.setBackgroundColor(Color.RED);
		lblDadosInvalidos.setTextColor(Color.WHITE);
		lblDadosInvalidos.setText(texto);
		lblDadosInvalidos.setVisibility(View.VISIBLE);
	}
	
	private void ocultarLabelErro(){
		lblDadosInvalidos.setVisibility(View.GONE);
	}
	
	class TarefaListagem extends AsyncTask<String, Void, Object> {

		@Override
		protected Object doInBackground(String... params) {
			List<Usuario> usuarios = new ArrayList<Usuario>();
			try {
				usuarios = ControladorDeUsuario.getInstancia(getContexto()).listar();
			} catch (Exception e) {
				e.printStackTrace();
				return e;
			} catch (Erro e) {
				e.printStackTrace();
				return e;
			}
			return usuarios;
		}
		
	}
	
	class TarefaLogin extends AsyncTask<String, Void, Object> {
		
		@Override
		protected void onPreExecute() {
//			Dialogos.Progresso.exibirDialogoProgresso(LoginActivity.this);
			super.onPreExecute();
		}
		
		@Override
		protected Object doInBackground(String... params) {
			Void resultado = null;
			String login = params[0];
			String senha = params[1];
			try {
				ControladorDeUsuario.getInstancia(contexto).login(login, senha);
			} catch (Erro e) {
				e.printStackTrace();
				return e;
			}
			return resultado;
		}
		
		@Override
		protected void onPostExecute(Object result) {
//			Dialogos.Progresso.fecharDialogoProgresso();
			super.onPostExecute(result);
		}
		
	}
	
}
