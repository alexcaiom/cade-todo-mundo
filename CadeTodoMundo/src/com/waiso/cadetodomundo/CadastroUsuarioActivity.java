package com.waiso.cadetodomundo;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.ExecutionException;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.waiso.cadetodomundo.abstratas.ClasseActivity;
import com.waiso.cadetodomundo.controladores.ControladorDeUsuario;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.excecoes.ErroNegocio;
import com.waiso.cadetodomundo.excecoes.SysErr;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Dialogos;
import com.waiso.cadetodomundo.utils.Sessao;
import com.waiso.cadetodomundo.utils.UtilsData;
import com.waiso.cadetodomundo.utils.UtilsDispositivo;

public class CadastroUsuarioActivity extends ClasseActivity {

	EditText txtNome, txtEmail, txtLogin, txtSenha, txtConfirmaSenha, txtDataNascimento;
	Button btnCadastrar;
	double longitude, latitude;
	EnumVisualizacaoCadastroUsuario visualizacao = null;
	Usuario usuario = null;

	private View[] camposDeCadastro = null;
	private View[] camposDeEdicao = null;
	private View[] camposDeExibicao = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro_usuario);
		executarAnimacao(R.anim.fade_in);
		carregarTela();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cadastro_usuario, menu);
		return true;
	}

	@Override
	public void carregarTela() {
		txtNome = (EditText) findViewById(R.id.txtUsuario_nome);
		txtEmail = (EditText) findViewById(R.id.txtUsuario_email);
		txtLogin = (EditText) findViewById(R.id.txtUsuario_login);
		txtSenha = (EditText) findViewById(R.id.txtUsuario_senha);
		txtConfirmaSenha = (EditText) findViewById(R.id.txtUsuario_confirmaSenha);
		txtDataNascimento = (EditText) findViewById(R.id.txtUsuario_dataNascimento);
		btnCadastrar = (Button) findViewById(R.id.usuario_btnCadastrar);
		camposDeCadastro = new View[] { txtNome, txtEmail, txtLogin, txtSenha, txtConfirmaSenha, btnCadastrar };
		camposDeEdicao = new View[] { txtNome, txtEmail, txtLogin, txtSenha, txtConfirmaSenha, txtDataNascimento, btnCadastrar };
		camposDeExibicao = new View[] { txtNome, txtEmail, txtLogin, txtDataNascimento };


		/**
		 * Podemos usar esta tela em 3 configuracoes diferentes:
		 */
		// 1 - Cadastro (Nao eh tomada nenhuma acao). A tela fica vazia
		// 2 - Visualizacao de Cadastro de Amigo
		if (existe(getParametros()) && getParametros().containsKey(Constantes.ITEM_AMIGO)) {
			usuario = (Usuario) getParametros().get(Constantes.ITEM_AMIGO);
			visualizacao = EnumVisualizacaoCadastroUsuario.VISUAZIZACAO_DE_AMIGO;
			regularCamposVisiveisDaTela();
			preencheCampos();
		}
		// 3 - Edicao de Cadastro do Usuario
		else if (Sessao.temParametro(Constantes.USUARIO)) {
			usuario = (Usuario) Sessao.getParametro(Constantes.USUARIO);
			visualizacao = EnumVisualizacaoCadastroUsuario.EDICAO_DE_USUARIO;
			regularCamposVisiveisDaTela();
			preencheCampos();
		} else if (!Sessao.usuarioLogado()) {
			visualizacao = EnumVisualizacaoCadastroUsuario.CADASTRO_DE_USUARIO;
			regularCamposVisiveisDaTela();
		}


		carregarEventos();
	}

	private void regularCamposVisiveisDaTela() {
		//Primeiro escondemos todos
		for (View view : camposDeEdicao) {
			if (existe(view)) {
				view.setVisibility(View.GONE);
			}
		}
		
		//Depois decidimos o que ficara visivel, ou nao
		switch (visualizacao) {
		case CADASTRO_DE_USUARIO:
			for (View view : camposDeCadastro) {
				if (existe(view)) {
					view.setVisibility(View.VISIBLE);
				}
			}
			break;
		case EDICAO_DE_USUARIO:
			for (View view : camposDeEdicao) {
				if (existe(view)) {
					view.setVisibility(View.VISIBLE);
				}
			}
			botaoHomeAtivo(true);
			break;
		case VISUAZIZACAO_DE_AMIGO:
			for (View view : camposDeExibicao) {
				if (existe(view)) {
					view.setVisibility(View.VISIBLE);
					view.setEnabled(false);
				}
			}
		default:
			break;
		}
	}

	private void preencheCampos() {
		if (existe(usuario)) {
			txtNome.setText(usuario.getNome());
			txtLogin.setText(usuario.getLogin());
			txtEmail.setText(usuario.getEmail());
			Calendar data = usuario.getDataDeNascimento();
			if (existe(data)) {
				int ano = data.get(GregorianCalendar.YEAR);
				int mes = data.get(GregorianCalendar.MONTH);
				int dia = data.get(GregorianCalendar.DAY_OF_MONTH);
				String dataTxt = ((dia < 10) ? "0" + dia : dia) + "/" + (((mes + 1) < 10) ? "0" + (mes + 1) : mes + 1)
						+ "/" + ano;
				
				txtDataNascimento.setText(dataTxt);
			} else {
				if (visualizacao == EnumVisualizacaoCadastroUsuario.CADASTRO_DE_USUARIO
						|| visualizacao == EnumVisualizacaoCadastroUsuario.EDICAO_DE_USUARIO) {
					txtDataNascimento.setVisibility(View.VISIBLE);
				} else {
					txtDataNascimento.setVisibility(View.GONE);
				}
			}
		}
	}

	@Override
	public void carregarEventos() {
		btnCadastrar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gravar();
			}
		});
		txtDataNascimento.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					OnDateSetListener resposta = new OnDateSetListener() {
						@Override
						public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
							String data = ((dayOfMonth < 10) ? "0" + dayOfMonth : dayOfMonth) + "/"
									+ (((monthOfYear + 1) < 10) ? "0" + (monthOfYear + 1) : monthOfYear + 1) + "/"
									+ year;
							txtDataNascimento.setText(data);

						}
					};
					String dataTxt = txtDataNascimento.getText().toString();
					Calendar data = null;
					if (dataTxt.isEmpty()) {
						data = GregorianCalendar.getInstance();
					} else {
						data = UtilsData.strToCalendar(dataTxt);
					}
					int ano = data.get(GregorianCalendar.YEAR);
					int mes = data.get(GregorianCalendar.MONTH);
					int dia = data.get(GregorianCalendar.DAY_OF_MONTH);
					DatePickerDialog dialogo = new DatePickerDialog(CadastroUsuarioActivity.this, resposta, ano, mes,
							dia);
					dialogo.show();
				}
			}
		});
	}

	private void gravar() {
		try {
			String nome = txtNome.getText().toString();
			String email = txtEmail.getText().toString();
			String login = txtLogin.getText().toString();
			String senha = txtSenha.getText().toString();
			String confirmacaoSenha = txtConfirmaSenha.getText().toString();
			Calendar data = GregorianCalendar.getInstance();
			data = UtilsData.strToCalendar(txtDataNascimento.getText().toString());
			String imei = UtilsDispositivo.getInstancia(CadastroUsuarioActivity.this).getIDAndroid();
			
			boolean camposDeSenhaEConfirmacaoDeSenhaSaoCorrespondentes = verificaSeSenhaEConfimacaoDeSenhaSaoIguais(
					senha, confirmacaoSenha);
			
			if (camposDeSenhaEConfirmacaoDeSenhaSaoCorrespondentes) {
				boolean ehCadastro = naoExiste(usuario);
				if (ehCadastro) {
					new TarefaCadastroUsuario().execute(nome, email, login, senha, data, imei).get();
				} else {
					new TarefaEdicaoUsuario().execute(nome, email, login, senha, data, imei).get();
				}
			} else {
				avisar("Senha e Confirmacao de Senha nao conferem!");
				txtSenha.requestFocus();
			}
			
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private boolean verificaSeSenhaEConfimacaoDeSenhaSaoIguais(String senha, String confirmacaoSenha) {
		return senha.equals(confirmacaoSenha);
	}

	class TarefaCadastroUsuario extends AsyncTask<Object, Void, Void> {

		@Override
		protected Void doInBackground(Object... params) {
			String nome = (String) params[0];
			String email = (String) params[1];
			String login = (String) params[2];
			String senha = (String) params[3];
			Calendar data = (Calendar) params[4];
			String imei = (String) params[5];
			try {
				ControladorDeUsuario.getInstancia(contexto).cadastrar(nome, email, login, senha, data, imei);
				irPara(MainActivity.class);
			} catch (Erro e) {
				if (e instanceof ErroNegocio) {

					/**
					 * Aqui podemos ter um login invalido
					 */
					String aviso = e.getMessage();
					if (aviso.contains("{") || aviso.contains("}")) {
						aviso = aviso.replace("{\"erro\"", "").replace("}", "");
					}
					avisar(aviso);
				} else if (e instanceof SysErr) {
					Dialogos.Alerta.exibirMensagemErro(e, CadastroUsuarioActivity.this, null);
				}
			}
			return null;
		}

	}
	
	class TarefaEdicaoUsuario extends AsyncTask<Object, Void, Void> {
		
		@Override
		protected Void doInBackground(Object... params) {
			String nome = (String) params[0];
			String email = (String) params[1];
			String login = (String) params[2];
			String senha = (String) params[3];
			Calendar data = (Calendar) params[4];
			String imei = (String) params[5];
			try {
				ControladorDeUsuario.getInstancia(contexto).alterar(usuario.getId(), nome, email, login, senha,
						data, imei);
				onBackPressed();
			} catch (Erro e) {
				if (e instanceof ErroNegocio) {
					
					/**
					 * Aqui podemos ter um login invalido
					 */
					String aviso = e.getMessage();
					if (aviso.contains("{") || aviso.contains("}")) {
						aviso = aviso.replace("{\"erro\"", "").replace("}", "");
					}
					avisar(aviso);
				} else if (e instanceof SysErr) {
					Dialogos.Alerta.exibirMensagemErro(e, CadastroUsuarioActivity.this, null);
				}
			}
			return null;
		}
		
	}

}
