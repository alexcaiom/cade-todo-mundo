/**
 * 
 */
package com.waiso.cadetodomundo.orm.dao.interfaces;

import com.waiso.cadetodomundo.excecoes.SysErr;

/**
 * @author Alex
 *
 */
public interface IGenericDAO<T> {

	public T incluir(T orm) throws SysErr;
	public void atualizar(T orm) throws SysErr;
	public void excluir(T orm) throws SysErr;

}
