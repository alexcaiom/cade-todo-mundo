package com.waiso.cadetodomundo.orm.dao;

import android.content.ContentValues;
import android.widget.Toast;

import com.waiso.cadetodomundo.abstratas.ContextoPadrao;
import com.waiso.cadetodomundo.excecoes.SysErr;
import com.waiso.cadetodomundo.orm.dao.interfaces.GenericDAO;
import com.waiso.cadetodomundo.orm.modelos.Usuario;

public class DAOUsuario extends GenericDAO<Usuario> {
	
	private static DAOUsuario instancia;

	@Override
	public Usuario incluir(Usuario orm) throws SysErr {
		try {
			ContentValues values = new ContentValues();
			values.put("nome", orm.getNome());
			values.put("login", orm.getLogin());
			values.put("senha", orm.getSenha());
			values.put("imei", orm.getImei());
			values.put("bloqueado", 0);
			
			String mensagem = "Inserindo "+orm.getLogin();
			contexto.getContexto().avisar(mensagem, Toast.LENGTH_SHORT);
			long insertId = 0;
			insertId = incluir(orm, values);

			if(insertId > -1){
				orm.comId(insertId);
			} else {
				throw new SysErr("Erro ao tentar inserir "+getNomeTabela());
			}
		} catch (android.database.SQLException e) {
			orm = null;
			e.printStackTrace();
		} catch (SysErr e) {
			e.printStackTrace();
		} catch (Exception e) {
			throw new SysErr(e);
		}
	    return orm;
	}

	@Override
	public void atualizar(Usuario orm) throws SysErr {
		try {
			ContentValues values = new ContentValues();
			values.put("nome", orm.getNome());
			values.put("login", orm.getLogin());
			if (existe(orm) && existe(orm.getSenha()) && !orm.getSenha().isEmpty() && !orm.getSenha().equals("null")) {
				values.put("senha", orm.getSenha());
			}
			values.put("email", orm.getEmail());
			values.put("idade", orm.getIdade());
			values.put("dataDeNascimento", orm.getDataDeNascimento().getTimeInMillis());
			values.put("imei", orm.getImei());
			
			values.put("bloqueado", 0);
			
			String mensagem = "Inserindo "+orm.getLogin();
//			contexto.getContexto().avisar(mensagem, Toast.LENGTH_SHORT);
			String whereClause = "id=?";
			String[] whereArgs = {orm.getId().toString()};
			atualizar(values, whereClause, whereArgs);

		} catch (android.database.SQLException e) {
			orm = null;
			e.printStackTrace();
		} catch (SysErr e) {
			e.printStackTrace();
		} catch (Exception e) {
			throw new SysErr(e);
		}
	}
	
	public boolean associarUsuarios(Usuario eu, Usuario amigo) throws SysErr {
		boolean sucesso = true;
		
		try{
			ContentValues values = new ContentValues();
			values.put("id_usuario", eu.getId());
			values.put("id_amigo", amigo.getId());
			values.put("jaEhAmigo", 0);
			
			String mensagem = "Associando "+eu.getLogin()+" a "+amigo.getLogin();
			contexto.getContexto().avisar(mensagem, Toast.LENGTH_SHORT);
			String tabela = "TB_AMIZADES";
			long status = getBD().insert(tabela, null, values);
			if (status == -1) {
				sucesso = false;
			}

		} catch (android.database.SQLException e) {
			sucesso = false;
			e.printStackTrace();
		} catch (SysErr e) {
			sucesso = false;
			e.printStackTrace();
		} catch (Exception e) {
			sucesso = false;
			throw new SysErr(e);
		}
		
		return sucesso;
	}
	
	public boolean confirmarAmizade(Usuario eu, Usuario amigo) throws SysErr {
		boolean sucesso = true;
		String tabela = "TB_AMIZADES";
		
		try{
			ContentValues values = new ContentValues();
			values.put("jaEhAmigo", 1);
			String[] whereArgs = {
					eu.getId().toString(),
					amigo.getId().toString()
			}; 
			
			
			String whereClause = " id_usuario = ? and id_amigo = ?";
			
			String mensagem = "Confirmando amizade entre "+eu.getLogin()+" e "+amigo.getLogin();
			contexto.getContexto().avisar(mensagem, Toast.LENGTH_SHORT);
			getBD().update(tabela, values, whereClause, whereArgs);
			
		} catch (android.database.SQLException e) {
			sucesso = false;
			e.printStackTrace();
		} catch (SysErr e) {
			sucesso = false;
			e.printStackTrace();
		} catch (Exception e) {
			sucesso = false;
			throw new SysErr(e);
		}
		
		return sucesso;
	}

	@Override
	public void excluir(Usuario orm) throws SysErr {
		// TODO Auto-generated method stub
		
	}
	

	public static DAOUsuario getInstancia(ContextoPadrao contexto){
		if (naoExiste(instancia)) {
			instancia = new DAOUsuario();
			instancia.contexto = contexto;
		}
		return instancia;
	}
	

}
