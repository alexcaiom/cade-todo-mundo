package com.waiso.cadetodomundo.orm.dao.finder;

import java.util.ArrayList;
import java.util.List;

import android.database.sqlite.SQLiteException;

import com.waiso.cadetodomundo.abstratas.ContextoPadrao;
import com.waiso.cadetodomundo.excecoes.SysErr;
import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Dialogos;
import com.waiso.cadetodomundo.utils.GeradorSQLBean;
import com.waiso.cadetodomundo.utils.Sessao;

public class FinderSolicitacao extends Finder<SolicitacaoDeAmizade> {

	public FinderSolicitacao(ContextoPadrao contexto) {
		this.contexto = contexto;
	}	

	public List<SolicitacaoDeAmizade> listarSolicitacoesDeAmizade() {
		List<SolicitacaoDeAmizade> lista = new ArrayList<SolicitacaoDeAmizade>();
		Usuario eu = (Usuario) Sessao.getParametro(Constantes.USUARIO);
		try {
			String subQueryNaoEhAmigo = " select id_usuario "
					+	"\n from TB_AMIZADES "
					+   "\n where id_amigo = ? "
					+   "\n and jaEhAmigo = 0";
			String sql = "select * "
					+ "\n from "+GeradorSQLBean.getInstancia(Usuario.class).getNomeTabela()
					+ "\n where id != ?"
					+ "\n and id in (" + subQueryNaoEhAmigo + ")";
			List<String> argumentos = new ArrayList<String>();
			argumentos.add(eu.getId().toString());
			argumentos.add(eu.getId().toString());
			String[] selectionArgs = argumentos.toArray(new String[argumentos.size()]);
			cursor = getBD().rawQuery(sql, selectionArgs);
			
			if(cursor.getCount() > 0 && cursor.getColumnCount() >= 10 && cursor.moveToFirst()){
				while(!cursor.isAfterLast()){
//					Long idUsuario = resultado.getLong(resultado.getColumnIndex("id_usuario"));
					
					SolicitacaoDeAmizade s = new SolicitacaoDeAmizade();
//					usuarioQueSolicitouAmizade.comId(idUsuario);
//					usuarioQueSolicitouAmizade = findById(usuarioQueSolicitouAmizade);
					preencheVO(s);
					
					lista.add(s);
					cursor.moveToNext();
				}
			}
			
		} catch (SQLiteException e) {
			Dialogos.Alerta.exibirMensagemErro(e, contexto.getContexto(), null);
		} catch (SysErr e) {
			e.printStackTrace();
			Dialogos.Alerta.exibirMensagemErro(e, contexto.getContexto(), null);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			finalizar();
		}
		return lista;
	}
	
	@Override
	void preencheVO(SolicitacaoDeAmizade o) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
