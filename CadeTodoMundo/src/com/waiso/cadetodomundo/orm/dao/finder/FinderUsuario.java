/**
 * 
 */
package com.waiso.cadetodomundo.orm.dao.finder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import com.google.android.gms.internal.ja;
import com.waiso.cadetodomundo.abstratas.ContextoPadrao;
import com.waiso.cadetodomundo.excecoes.SysErr;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Dialogos;
import com.waiso.cadetodomundo.utils.GeradorSQLBean;
import com.waiso.cadetodomundo.utils.Sessao;
import com.waiso.cadetodomundo.utils.UtilsData;
import com.waiso.cadetodomundo.utils.UtilsNumero;

/**
 * @author Alex
 *
 */
public class FinderUsuario extends Finder<Usuario>{

	private static FinderUsuario instancia;

	public FinderUsuario(){}

	public FinderUsuario(ContextoPadrao contexto) throws SysErr {
		log("Instanciando...");
		this.contexto = contexto;
	}

	public Usuario findByLogin(Usuario u) {
		log("Consultando "+getNomeEntidade());
		try {
			cursor = getBD().query(GeradorSQLBean.getInstancia(Usuario.class).getNomeTabela(), null, 
					"login=? and senha=?", new String[]{u.getLogin(), u.getSenha()}, null, null, null);

			if(cursor.getCount() > 0 && cursor.getColumnCount() >= 10 && cursor.moveToFirst()){
				preencheVO(u);
			}else{
				u = null;
			}

			
		} catch (SysErr e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			finalizar();
		}
		return u;
	}

	public Usuario findById(Usuario u) {
		log("Consultando "+getNomeEntidade());
		try {
			//		String sql = "select * from tb_usuario where id = "+u.getIp();//GeradorSQLBean.getInstancia(u.getClass()).getCreateTable();
			
			cursor = getBD().query(GeradorSQLBean.getInstancia(Usuario.class).getNomeTabela(), null, 
					"id=?", new String[]{u.getId().toString()}, null, null, null);
			
			if(cursor.getCount() > 0 && cursor.getColumnCount() >= 10 && cursor.moveToFirst()){
				preencheVO(u);
			}else{
				u = null;
			}
			
		} catch (SysErr e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			finalizar();
		}
		return u;
	}

	public List<Usuario> listar() throws Exception  {
		List<Usuario> lista = new ArrayList<Usuario>();
		try {
			cursor = getBD().query(GeradorSQLBean.getInstancia(Usuario.class).getNomeTabela(), null, 
					null, null, null, null, null);
			Usuario u = null;
			if(cursor.getCount() > 0 && cursor.getColumnCount() >= 10 && cursor.moveToFirst()){
				while(!cursor.isAfterLast()){
					u = new Usuario();
					preencheVO(u);
					lista.add(u);
					cursor.moveToNext();
				}
			}else{
				u = null;
			}

		} catch (SysErr e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			finalizar();
		}
		return lista;
	}
	
	public List<Usuario> listarAmigosComNomeComo(String usuario) {
		List<Usuario> lista = new ArrayList<Usuario>();
		Usuario eu = (Usuario) Sessao.getParametro(Constantes.USUARIO);
		try {
			boolean usarUsuario = existe(usuario) && !usuario.isEmpty();
			String whereUsuario = "";
			if (usarUsuario) {
				whereUsuario = "\n and nome like '%"+usuario+"%'";
			}
			String subQueryEhAmigo = " select id_amigo "
					+	"\n from TB_AMIZADES "
					+   "\n where id_usuario = ?"
					+   "\n and jaEhAmigo = 1 ";
			String sql = "select * "
					+ "\n from "+GeradorSQLBean.getInstancia(Usuario.class).getNomeTabela()
					+ "\n where id != ?"
					+ "\n and id in (" + subQueryEhAmigo + ")"
					+ whereUsuario;
			List<String> argumentos = new ArrayList<String>();
			argumentos.add(eu.getId().toString());
			argumentos.add(eu.getId().toString());
			if (usarUsuario) {
				argumentos.add(usuario);
			}
			String[] selectionArgs = argumentos.toArray(new String[argumentos.size()]);
			cursor = getBD().rawQuery(sql, selectionArgs);
			
			Usuario u = null;
			if(cursor.getCount() > 0 && cursor.getColumnCount() >= 10 && cursor.moveToFirst()){
				while(!cursor.isAfterLast()){
					u = new Usuario();
					preencheVO(u);
					lista.add(u);
					cursor.moveToNext();
				}
			}else{
				u = null;
			}
			
		} catch (SQLiteException e) {
			Dialogos.Alerta.exibirMensagemErro(e, contexto.getContexto(), null);
		} catch (SysErr e) {
			e.printStackTrace();
			Dialogos.Alerta.exibirMensagemErro(e, contexto.getContexto(), null);
		} finally {
			finalizar();
		}
		return lista;
	}
	
	public List<Usuario> listarOutrosUsuariosComNomeComo(String usuario) {
		List<Usuario> lista = new ArrayList<Usuario>();
		Usuario eu = (Usuario) Sessao.getParametro(Constantes.USUARIO);
		try {
			boolean usarUsuario = existe(usuario) && !usuario.isEmpty();
			String whereUsuario = "";
			if (usarUsuario) {
				whereUsuario = "\n and nome like '%"+usuario+"%'";
			}
			String subQueryNaoEhAmigo = " select id_amigo "
									+	"\n from TB_AMIZADES "
									+   "\n where id_usuario = ?"
									/*+   "\n and jaEhAmigo = 0 "*/;
			String sql = "select * "
						+ "\n from "+GeradorSQLBean.getInstancia(Usuario.class).getNomeTabela()
						+ "\n where id != ?"
						+ "\n and id in (" + subQueryNaoEhAmigo + ")"
						+ whereUsuario;
			List<String> argumentos = new ArrayList<String>();
			argumentos.add(eu.getId().toString());
			argumentos.add(eu.getId().toString());
			if (usarUsuario) {
				argumentos.add(usuario);
			}
			String[] selectionArgs = argumentos.toArray(new String[argumentos.size()]);
			cursor = getBD().rawQuery(sql, selectionArgs);
			
			Usuario u = null;
			if(cursor.getCount() > 0 && cursor.getColumnCount() >= 10 && cursor.moveToFirst()){
				while(!cursor.isAfterLast()){
					u = new Usuario();
					preencheVO(u);
					lista.add(u);
					cursor.moveToNext();
				}
			}else{
				u = null;
			}

		} catch (SQLiteException e) {
			Dialogos.Alerta.exibirMensagemErro(e, contexto.getContexto(), null);
		} catch (SysErr e) {
			e.printStackTrace();
			Dialogos.Alerta.exibirMensagemErro(e, contexto.getContexto(), null);
		} finally {
			finalizar();
		}
		return lista;
	}
	
	public List<Object[]> listarTodasAsSolicitacoesDeAmizade() {
		List<Object[]> lista = new ArrayList<Object[]>();
		try {
			String sqlSolicitacoes = " select * "
									+	"\n from TB_AMIZADES ";
			String sqlUsuario = "select * "
					+ "\n from "+GeradorSQLBean.getInstancia(Usuario.class).getNomeTabela()
					+ "\n where id = ?";
			List<String> argumentos = new ArrayList<String>();
			String[] selectionArgs = argumentos.toArray(new String[argumentos.size()]);
			Cursor resultado = getBD().rawQuery(sqlSolicitacoes, selectionArgs);
			
			Usuario u = null;
			if(resultado.getCount() > 0 && resultado.getColumnCount() >= 3 && resultado.moveToFirst()){
				while(!resultado.isAfterLast()){
					Long idUsuario = resultado.getLong(resultado.getColumnIndex("id_usuario"));
					Long idAmigo = resultado.getLong(resultado.getColumnIndex("id_amigo"));
					
					Usuario usuario = new Usuario();
					usuario.comId(idUsuario);
					usuario = findById(usuario);
					
					Usuario amigo = new Usuario();
					amigo.comId(idAmigo);
					amigo = findById(amigo);
					
					int indicadorAmizade = resultado.getInt(resultado.getColumnIndex("jaEhAmigo"));
					Boolean jaEhAmigo = (indicadorAmizade == 1);
					
					lista.add(new Object[]{usuario, amigo, jaEhAmigo});
					resultado.moveToNext();
				}
				
				resultado.close();
			}else{
				u = null;
			}
			
		} catch (SQLiteException e) {
			Dialogos.Alerta.exibirMensagemErro(e, contexto.getContexto(), null);
		} catch (SysErr e) {
			e.printStackTrace();
			Dialogos.Alerta.exibirMensagemErro(e, contexto.getContexto(), null);
		} finally {
			finalizar();
		}
		return lista;
	}
	
	@Override
	void preencheVO(Usuario o) {
		if (existe(o) && existe(cursor)) {
			o.comId(cursor.getLong(cursor.getColumnIndex("id")));
			o.comNome(cursor.getString(cursor.getColumnIndex("nome")));
			o.comEmail(cursor.getString(cursor.getColumnIndex("email")));
			o.comLogin(cursor.getString(cursor.getColumnIndex("login")));
			o.comSenha(cursor.getString(cursor.getColumnIndex("senha")));
			o.estaBloqueado(cursor.getInt(cursor.getColumnIndex("bloqueado")));
			o.comDataDeNascimento(UtilsData.timeInMillisString2Calendar(cursor.getString(cursor.getColumnIndex("dataDeNascimento"))));
			o.comLatitude(UtilsNumero.stringParaDouble(cursor.getString(cursor.getColumnIndex("latitude"))));
			o.comLongitude(UtilsNumero.stringParaDouble(cursor.getString(cursor.getColumnIndex("longitude"))));
		}
	}
	
	public static FinderUsuario getInstancia(ContextoPadrao contexto)  {
		if(instancia == null){
			try {
				instancia = new FinderUsuario(contexto);
			} catch (SysErr e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return instancia;
	}
}
