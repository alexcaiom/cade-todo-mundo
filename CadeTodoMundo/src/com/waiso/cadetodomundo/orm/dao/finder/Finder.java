package com.waiso.cadetodomundo.orm.dao.finder;

import android.database.sqlite.SQLiteDatabase;

import com.waiso.cadetodomundo.excecoes.SysErr;
import com.waiso.cadetodomundo.orm.dao.DAO;

public abstract class Finder<T> extends DAO<T> {

	protected String getNomeEntidade(){
		return CLASSE_NOME.substring(CLASSE_NOME.lastIndexOf("Finder"));
	}
	
	abstract void preencheVO(T o) throws Exception;
	
	protected SQLiteDatabase getBD() throws SysErr{
		return getBD(TIPO_BD_LEITURA);
	}
	
}
