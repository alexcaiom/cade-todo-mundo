/**
 * 
 */
package com.waiso.cadetodomundo.orm.dao;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.waiso.cadetodomundo.abstratas.ContextoPadrao;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Dialogos;
import com.waiso.cadetodomundo.utils.GeradorSQLBean;


/**
 * @author Alex
 *
 */
public class GerenciadorBD extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = Constantes.BANCO_DADOS_NOME;
	private static final int DATABASE_VERSION = Constantes.BANCO_DADOS_VERSAO;
	private static final String TB_AMIZADES = "CREATE TABLE TB_AMIZADES("
											+ "\n id_usuario int, "
											+ "\n id_amigo int,"
											+ "\n jaEhAmigo int );";
	
	private ContextoPadrao contexto;

	public GerenciadorBD(ContextoPadrao contexto) {
		super(contexto.getContexto(), DATABASE_NAME, null, DATABASE_VERSION);
		this.contexto = contexto;
	}

	
	/* (non-Javadoc)
	 * @see com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase, com.j256.ormlite.support.ConnectionSource)
	 */
	@Override
	public void onCreate(SQLiteDatabase bd) {
		try {
			Log.i(GerenciadorBD.class.getSimpleName(), "onCreate");
			String createTableUsuario = GeradorSQLBean.getInstancia(Usuario.class).getCreateTable();
			Dialogos.Alerta.exibirMensagemInformacao(contexto, false, "Criando tabela com Script: \n"+createTableUsuario, null);
			bd.execSQL(createTableUsuario);
			Dialogos.Alerta.exibirMensagemInformacao(contexto, false, "Criando tabela com Script: \n"+TB_AMIZADES, null);
			bd.execSQL(TB_AMIZADES);
//			bd.execSQL(GeradorSQLBean.getInstancia(Movimentacao.class).getCreateTable());
		} catch (SQLException e) {
			Log.e(GerenciadorBD.class.getSimpleName(), "Can't create database", e);
			throw new RuntimeException(e);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, com.j256.ormlite.support.ConnectionSource, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase bd, int versaoAnterior, int versaoNova) {
		try {
			bd.execSQL(GeradorSQLBean.getInstancia(Usuario.class).getDropTable());
			bd.execSQL("drop table TB_AMIZADES");
			onCreate(bd);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
