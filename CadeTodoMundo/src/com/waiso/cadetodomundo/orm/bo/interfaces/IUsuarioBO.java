package com.waiso.cadetodomundo.orm.bo.interfaces;

import com.waiso.cadetodomundo.orm.modelos.Usuario;


public interface IUsuarioBO extends IObjetoBO<Usuario> {

}
