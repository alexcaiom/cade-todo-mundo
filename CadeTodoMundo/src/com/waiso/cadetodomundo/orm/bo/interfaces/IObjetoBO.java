package com.waiso.cadetodomundo.orm.bo.interfaces;

import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.excecoes.SysErr;

public interface IObjetoBO<T> {

	T inserir(T o) throws Erro;
	void alterar(T o) throws SysErr;
	void excluir(T o) throws SysErr;
	
}
