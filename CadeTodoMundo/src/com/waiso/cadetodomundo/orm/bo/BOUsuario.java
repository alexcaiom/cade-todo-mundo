/**
 * 
 */
package com.waiso.cadetodomundo.orm.bo;

import java.util.List;

import android.util.Log;

import com.waiso.cadetodomundo.R;
import com.waiso.cadetodomundo.abstratas.Classe;
import com.waiso.cadetodomundo.abstratas.ContextoPadrao;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.excecoes.ErroNegocio;
import com.waiso.cadetodomundo.excecoes.SysErr;
import com.waiso.cadetodomundo.orm.bo.interfaces.IUsuarioBO;
import com.waiso.cadetodomundo.orm.dao.DAOUsuario;
import com.waiso.cadetodomundo.orm.dao.finder.FinderSolicitacao;
import com.waiso.cadetodomundo.orm.dao.finder.FinderUsuario;
import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Sessao;

/**
 * @author Alex
 *
 */
public class BOUsuario extends Classe implements IUsuarioBO {

	private static BOUsuario instancia;
	private final ContextoPadrao contexto;
	
	public BOUsuario(ContextoPadrao contexto){
		this.contexto = contexto;
	}
	
	/**
	 * Metodo de Autenticacao de Usuario
	 * @param login
	 * @param senha
	 * @return
	 * @throws SysErr 
	 * @throws ErroNegocio 
	 */
	public void autentica(String login, String senha) throws Erro {
		log("Autenticando "+getNomeEntidade());
//		ServicoUsuarioRemoto.getInstancia(contexto).logar(login, senha);
		Usuario u =  new Usuario();
		u.comLogin(login);
		u.comSenha(senha);
		u = FinderUsuario.getInstancia(contexto).findByLogin(u);
		if (existe(u)) {
			Sessao.addParametro(Constantes.USUARIO, u);
		} else {
			throw new ErroNegocio(contexto.getContexto().getString(R.string.mensagem_erro_login_usuario_senha));
		}
	}

	public static BOUsuario getInstancia(ContextoPadrao contexto) {
		if(instancia == null){
			instancia = new BOUsuario(contexto);
		}
		return instancia;
	}

	@Override
	public Usuario inserir(Usuario o) throws Erro {
		Log.i(CLASSE_NOME, "Inserindo "+getNomeEntidade());
//		ServicoUsuarioRemoto.getInstancia(contexto).cadastrar(o);
		Usuario u = DAOUsuario.getInstancia(contexto).incluir(o);
		Sessao.addParametro(Constantes.USUARIO, u);
		return u;
	}

	@Override
	public void alterar(Usuario o) throws SysErr {
		log("Alterando "+getNomeEntidade());
//		servico
		DAOUsuario.getInstancia(contexto).atualizar(o);
		Sessao.deslogar();
		Sessao.addParametro(Constantes.USUARIO, o);
	}

	@Override
	public void excluir(Usuario usuario) throws SysErr {
		log("Excluindo "+getNomeEntidade());
	}
	
	public boolean associar(Usuario eu, Usuario amigo) throws SysErr {
		return DAOUsuario.getInstancia(contexto).associarUsuarios(eu, amigo);
	}
	
	public List<Usuario> listar() throws Exception {
		return FinderUsuario.getInstancia(contexto).listar();
	}
	
	public List<Usuario> listarAmigosComNomeComo(String nome) throws Exception {
		return FinderUsuario.getInstancia(contexto).listarAmigosComNomeComo(nome);
	}
	
	public List<Usuario> listarOutrosUsuariosComNomeComo(String nome) throws Exception {
		return FinderUsuario.getInstancia(contexto).listarOutrosUsuariosComNomeComo(nome);
	}
	
	public List<SolicitacaoDeAmizade> listarSolicitacoesDeAmizade() throws Exception {
		return new FinderSolicitacao(contexto).listarSolicitacoesDeAmizade();
	}
	
	private String getNomeEntidade(){
		return CLASSE_NOME.substring(2);
	}
}
