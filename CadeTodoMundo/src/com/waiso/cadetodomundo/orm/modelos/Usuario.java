/**
 * 
 */
package com.waiso.cadetodomundo.orm.modelos;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author AlexDell
 *
 */
@DatabaseTable(tableName="tbl_usuario")
public class Usuario implements Serializable{
	
	@DatabaseField(id=true, generatedId=true)
	private Long id;
	private String login;
	private String senha;
	private int bloqueado;
	private String nome;
	private Integer idade;
	private Calendar dataDeNascimento;
	private String email;
	private List<Usuario> amigos;
	private Double longitude;
	private Double latitude;
	private String imei;
	private String status;
	
	public Usuario() {}
	public Usuario(String login, String senha, int bloqueado) {
		super();
		this.login = login;
		this.senha = senha;
		this.bloqueado = bloqueado;
	}
	
	public Usuario(String login, String senha, int bloqueado, String nome, Integer idade, GregorianCalendar dataDeNascimento, String email){
		this.login = login;
		this.senha = senha;
		this.bloqueado = bloqueado;
		this.nome = nome;
		this.idade = idade;
		this.dataDeNascimento = dataDeNascimento;
		this.email = email;
	}


	public String getLogin() {
		return login;
	}
	public Usuario comLogin(String login) {
		this.login = login;
		return this;
	}
	public String getSenha() {
		return senha;
	}
	public Usuario comSenha(String senha) {
		this.senha = senha;
		return this;
	}
	public int getBloqueado() {
		return bloqueado;
	}
	public Usuario estaBloqueado(int bloqueado) {
		this.bloqueado = bloqueado;
		return this;
	}
	public List<Usuario> getAmigos() {
		return amigos;
	}
	public Usuario comAmigos(List<Usuario> amigos) {
		this.amigos = amigos;
		return this;
	}
	
	public Usuario addAmigo(Usuario amigo) {
		this.amigos.add(amigo);
		return this;
	}
	public Long getId() {
		return id;
	}
	public Usuario comId(Long id) {
		this.id = id;
		return this;
	}
	public String getNome() {
		return nome;
	}
	public Usuario comNome(String nome) {
		this.nome = nome;
		return this;
	}
	public Integer getIdade() {
		return idade;
	}
	public Usuario comIdade(Integer idade) {
		this.idade = idade;
		return this;
	}
	public Calendar getDataDeNascimento() {
		return dataDeNascimento;
	}
	public Usuario comDataDeNascimento(Calendar dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
		return this;
	}
	public String getEmail() {
		return email;
	}
	public Usuario comEmail(String email) {
		this.email = email;
		return this;
	}
	public double getLongitude() {
		return longitude;
	}
	public Usuario comLongitude(Double longitude) {
		this.longitude = longitude;
		return this;
	}
	public double getLatitude() {
		return latitude;
	}
	public Usuario comLatitude(Double latitude) {
		this.latitude = latitude;
		return this;
	}
	public String getImei() {
		return imei;
	}
	public Usuario comImei(String imei) {
		this.imei = imei;
		return this;
	}
	public String getStatus() {
		return status;
	}
	public Usuario comStatus(String status) {
		this.status = status;
		return this;
	}
	
}
