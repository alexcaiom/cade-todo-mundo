package com.waiso.cadetodomundo;

import java.lang.annotation.Retention;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.j256.ormlite.field.types.IntegerObjectType;
import com.waiso.cadetodomundo.abstratas.ClasseActivity;
import com.waiso.cadetodomundo.abstratas.ContextoGenerico;
import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.tarefas.TarefaListagemSolicitacoes;
import com.waiso.cadetodomundo.tarefas.TarefaListagemUsuarios;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Sessao;
import com.waiso.cadetodomundo.utils.mapa.ServicoLocalizacao;
import com.waiso.cadetodomundo.utils.mapa.TracadorDeRota;

public class MainActivity extends ClasseActivity {

	Button btnAmigos, btnPerfil/*, btnApontar*/;
	GoogleMap mapa;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ocultarBarraDeAcoes();
		carregarTela();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case R.id.action_settings:
			return true;
		case R.id.action_logout:
			deslogar();
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void carregarTela() {
		btnAmigos = (Button) findViewById(R.id.btnAmigos);
		btnPerfil = (Button) findViewById(R.id.btnPerfil);
//		btnApontar = (Button) findViewById(R.id.btnApontar);
		mapa = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapa)).getMap();
		mapa.setMyLocationEnabled(true);
		carregarEventos();
	}

	@Override
	public void carregarEventos() {
		btnAmigos.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				irPara(ListagensUsuarioActivity.class);
				Intent intencao = new Intent(MainActivity.this, ListagensUsuarioActivity.class);
				try {
					Integer abaUsuarios = 1;
					Integer abaAmigos = 2;
					mostrarDialogoDeProgresso("Aguarde! Carregando listas de Usuario, Amigos e Solicitacoes de Amizade");
					ContextoGenerico contexto = new ContextoGenerico(MainActivity.this);
					List<Usuario> usuarios = (List<Usuario>) new TarefaListagemUsuarios().execute(abaUsuarios, contexto).get();
					List<Usuario> amigos = (List<Usuario>) new TarefaListagemUsuarios().execute(abaAmigos, contexto).get();
					
					List<SolicitacaoDeAmizade> solicitacoesDeAmizade = (List<SolicitacaoDeAmizade>) new TarefaListagemSolicitacoes().execute(contexto).get();
					Sessao.addParametro("usuarios", usuarios);
					Sessao.addParametro("amigos", amigos);
					Sessao.addParametro("solicitacoesDeAmizade", solicitacoesDeAmizade);
					fecharDialogoDeProgresso();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				startActivityForResult(intencao, 1);
			}
		});
		
		btnPerfil.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				irPara(CadastroUsuarioActivity.class);	
			}
		});
		
		/*btnApontar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				apontar();				
			}
		});*/
	}

	LatLng minhaPosicao = null;
	public void apontar(LatLng posicaoAmigo, String nome) {
		Location myLocation = existe(mapa.getMyLocation()) ? mapa.getMyLocation() : ServicoLocalizacao.getInstancia(MainActivity.this).getMinhaLocalizacaoAtual();
		if (existe(myLocation)) {
			double minhaLatitude = myLocation.getLatitude();
			double minhaLongitude = myLocation.getLongitude();
			 minhaPosicao = new LatLng(minhaLatitude, minhaLongitude);
		}
		
		BitmapDescriptor icone = BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher);
		adicionarMarcadoresAoMapa(posicaoAmigo, nome, "", icone);
		
		mapa.moveCamera(CameraUpdateFactory.newLatLngZoom(posicaoAmigo, 30));
		mapa.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
		
		TracadorDeRota tracador = new TracadorDeRota().de(minhaPosicao).ate(posicaoAmigo);
		tracador.aplicarRotaAoMapa(mapa);
	}

	private void adicionarMarcadoresAoMapa(LatLng posicao, String descricao, String snippet, BitmapDescriptor icone) {
		MarkerOptions posicaoMarcador = new MarkerOptions().position(posicao);
		posicaoMarcador.title(descricao);
		if (existe(snippet)) {
			posicaoMarcador.snippet(snippet);
		}
		if (existe(icone)) {
			posicaoMarcador.icon(icone);
		}
		mapa.addMarker(posicaoMarcador);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK && existe(data.getExtras())) {
			Bundle parametros = data.getExtras();
			if (parametros.containsKey(Constantes.ITEM_AMIGO)) {
				Usuario amigo = (Usuario) parametros.getSerializable(Constantes.ITEM_AMIGO);
				if (existe(amigo) && existe(amigo.getLatitude()) && existe(amigo.getLongitude())) {
					double latitude = amigo.getLatitude();
					double longitude = amigo.getLongitude();
					LatLng localizacao = new LatLng(latitude, longitude);
					mapa.clear();
					apontar(localizacao, amigo.getNome());
				}
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
}
