/**
 * 
 */
package com.waiso.cadetodomundo.abstratas;

import java.util.concurrent.ExecutionException;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.waiso.cadetodomundo.CadastroUsuarioActivity;
import com.waiso.cadetodomundo.LoginActivity;
import com.waiso.cadetodomundo.SplashActivity;
import com.waiso.cadetodomundo.controladores.ControladorDeUsuario;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.excecoes.ErroNegocio;
import com.waiso.cadetodomundo.excecoes.SysErr;
import com.waiso.cadetodomundo.interfaces.ClasseActivityInterface;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Dialogos;
import com.waiso.cadetodomundo.utils.Sessao;

/**
 * @author Alex
 *
 */
public abstract class ClasseFragmentoActivity extends FragmentActivity implements ClasseActivityInterface{

	public final String CLASSE_NOME = getClass().getSimpleName();
	public ContextoPadrao contexto;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		verificaSessao();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		verificaSessao();
	}

	public void irPara(Class tela, Bundle parametros){
		Intent intencao = new Intent(this, tela);
		if(parametros != null && !parametros.isEmpty()){
			intencao.putExtras(parametros);
		}
		startActivity(intencao);
	}
	
	public void irPara(Class tela){
		irPara(tela, null);
	}
	
	/**
	 * Metodo que lanca um aviso do tipo Toast ao usuario<br/>
	 * Aviso padrao: Longo
	 * @param aviso
	 */
	public void avisar(String aviso, Integer duracao) {
		if(duracao == null){
			Toast.makeText(getApplicationContext(), aviso, Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(getApplicationContext(), aviso, duracao).show();
		}
	}

	/**
	 * Metodo que lanca um aviso do tipo Toast ao usuario<br/>
	 * Aviso padrao: Longo
	 * @param aviso
	 */
	public void avisar(String aviso) {
		avisar(aviso, null);
	}
	
	private void verificaSessao() {
		if (!Sessao.usuarioLogado()) {
			if (!this.getClass().getSimpleName().equalsIgnoreCase(LoginActivity.class.getSimpleName()) && 
					!this.getClass().getSimpleName().equalsIgnoreCase(CadastroUsuarioActivity.class.getSimpleName()) &&
					!this.getClass().getSimpleName().equalsIgnoreCase(SplashActivity.class.getSimpleName())) {
				irPara(LoginActivity.class);
			}
		}
	}
	
	public void exibirMensagemDeProcessamento(){
		
	}
	
	protected void log(Object textoParaLog) {
		Log.i(CLASSE_NOME, textoParaLog.toString());
	}
	
	public void deslogar(){
		Void params = null;
		try {
			new TarefaDeslogar().execute(params).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Metodo que verifica se o objeto eh nulo
	 * @param o
	 * @return
	 */
	public static boolean naoExiste(Object o){
		boolean naoExiste = Classe.naoExiste(o);
		return naoExiste;
	}
	
	/**
	 * Metodo que verifica se o objeto nao eh nulo
	 * @param o
	 * @return
	 */
	public static boolean existe(Object o){
		boolean existe = !naoExiste(o);
		return existe;
	}
	
	class TarefaDeslogar extends AsyncTask<Void, Void, Void> {
		
		@Override
		protected void onPreExecute() {
			Dialogos.Progresso.exibirDialogoProgresso(ClasseFragmentoActivity.this);
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			try {
				//Este trecho vai demorar
				final Usuario usuarioLogado = (Usuario) Sessao.getParametro(Constantes.USUARIO);
				ControladorDeUsuario.getInstancia(contexto).logout(usuarioLogado.getLogin());
				Dialogos.Progresso.fecharDialogoProgresso();
			} catch (Erro e) {
				if (e instanceof ErroNegocio) {
					Dialogos.Progresso.fecharDialogoProgresso();
				} else if (e instanceof SysErr) {
					Dialogos.Alerta.exibirMensagemErro(e, ClasseFragmentoActivity.this, null);
					Dialogos.Alerta.fecharDialogo();
				}
			} catch (Exception e) {
				Dialogos.Progresso.fecharDialogoProgresso();
				Dialogos.Alerta.exibirMensagemErro(e, ClasseFragmentoActivity.this, null);
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			Sessao.deslogar();
			verificaSessao();
			super.onPostExecute(result);
			cancel(true);
		}
		
	}
	
}
