/**
 * 
 */
package com.waiso.cadetodomundo.abstratas;

import java.util.concurrent.ExecutionException;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.waiso.cadetodomundo.CadastroUsuarioActivity;
import com.waiso.cadetodomundo.LoginActivity;
import com.waiso.cadetodomundo.MainActivity;
import com.waiso.cadetodomundo.R;
import com.waiso.cadetodomundo.SplashActivity;
import com.waiso.cadetodomundo.controladores.ControladorDeUsuario;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.excecoes.ErroNegocio;
import com.waiso.cadetodomundo.excecoes.SysErr;
import com.waiso.cadetodomundo.interfaces.ClasseActivityInterface;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Dialogos;
import com.waiso.cadetodomundo.utils.Sessao;

/**
 * @author Alex
 *
 */
public abstract class ClasseActivity extends Activity implements ClasseActivityInterface{


	public ContextoPadrao contexto;
	public View tela;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		super.onCreate(savedInstanceState);
		contexto = new ContextoPadrao(this);
//		Point tamanhoDaTela = getTamanhoTela();
//		int x = tamanhoDaTela.x;
//		int y = tamanhoDaTela.y;
//		
//		String aviso = "comprimento: "+x + " altura: "+y;
//		
//		avisar(aviso);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		boolean pediuParaEncerrar = Sessao.temParametro(Constantes.SAIR);
		if (pediuParaEncerrar) {
			onBackPressed();
		}
		
		/** 
		 * Antes de Verificar se o Usuario esta Logado,
		 * Devemos verificar se estamos na area privada do Usuario
		 * ou se estamos apenas usando funcionalidades externas.
		 */
		
		boolean estouNaAreaInterna = !estouNaTela(LoginActivity.class) 
								&&   !estouNaTela(CadastroUsuarioActivity.class)
								&&	 !estouNaTela(SplashActivity.class);
		if (!estouNaAreaInterna) {
			return;
		}
		verificaSessao();
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
		return super.onMenuItemSelected(featureId, item);
	}
	
	public void irPara(Class tela, Bundle parametros){
		Intent intencao = new Intent(this, tela);
		if(parametros != null && !parametros.isEmpty()){
			intencao.putExtras(parametros);
		}
		startActivity(intencao);
	}
	
	public void irPara(Class tela){
		irPara(tela, null);
	}
	
	/**
	 * Metodo que lanca um aviso do tipo Toast ao usuario<br/>
	 * Aviso padrao: Longo
	 * @param aviso
	 */
	public void avisar(String aviso, Integer duracao) {
		if(duracao == null){
			Toast.makeText(getApplicationContext(), aviso, Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(getApplicationContext(), aviso, duracao).show();
		}
	}

	/**
	 * Metodo que lanca um aviso do tipo Toast ao usuario<br/>
	 * Aviso padrao: Longo
	 * @param aviso
	 */
	public void avisar(String aviso) {
		avisar(aviso, null);
	}
	
	public void verificaSessao() {
		/**
		 * Se estamos na Area Interna, verificamos se a Sessao esta Valida
		 */
		
		try {
			if (Sessao.usuarioLogado()) {
				Usuario usuario = (Usuario) Sessao.getParametro(Constantes.USUARIO);
//				try {
					Object retorno = new TarefaVerificacaoSessao().execute(usuario.getLogin(), usuario.getSenha()).get();
					if (retorno instanceof Boolean) {
						Boolean estaLogadoNoServidor = (Boolean) retorno;
						if (!estaLogadoNoServidor) {
							Sessao.deslogar();
							OnClickListener escutadorOk = new OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									irPara(LoginActivity.class);
								}
							};
							Dialogos.Alerta.exibirMensagemInformacao(getContexto(), false, "Sessao expirada! Logue-se novamente.", escutadorOk);
						}
					} else if (retorno instanceof Erro) {
						Dialogos.Alerta.exibirMensagemErro(((Erro)retorno), ClasseActivity.this, null);
					}
//				} catch (Erro e) {
//					e.printStackTrace();
//					Dialogos.Alerta.exibirMensagemErro(e, ClasseActivity.this, null);
//				}
			} else {
				if (!Classe.estouNaClasse(this.getClass(), LoginActivity.class)
						&& !Classe.estouNaClasse(this.getClass(), CadastroUsuarioActivity.class)
						&& !Classe.estouNaClasse(this.getClass(), SplashActivity.class)) {
					irPara(LoginActivity.class);
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deslogar(){
//		final Usuario usuarioLogado = (Usuario) Sessao.getParametro(Constantes.USUARIO);
//		Dialogos.Progresso.exibirDialogoProgresso(this);
//		Runnable thread = new Runnable() {
//			public void run() {
//				try {
					//Este trecho vai demorar
//					ControladorDeUsuario.getInstancia(contexto).logout(usuarioLogado.getLogin());
//					Dialogos.Progresso.fecharDialogoProgresso();
//				} catch (Erro e) {
//					if (e instanceof ErroNegocio) {
//						Dialogos.Progresso.fecharDialogoProgresso();
//					} else if (e instanceof SysErr) {
//						Dialogos.Alerta.exibirMensagemErro(e, ClasseActivity.this, null);
//						Dialogos.Alerta.fecharDialogo();
//					}
//				} catch (Exception e) {
//					Dialogos.Progresso.fecharDialogoProgresso();
//					Dialogos.Alerta.exibirMensagemErro(e, ClasseActivity.this, null);
//				}
//			}
//		};
		
//		new Thread(thread).start();
		
//		Sessao.deslogar();
		
		
		Void params = null;
		try {
			final Usuario usuarioLogado = (Usuario) Sessao.getParametro(Constantes.USUARIO);
			Throwable erro = new TarefaDeslogar().execute(usuarioLogado.getLogin()).get();
			if (existe(erro)) {
				if (erro instanceof ErroNegocio) {
					Dialogos.Progresso.fecharDialogoProgresso();
				} else if (erro instanceof SysErr) {
					Dialogos.Alerta.exibirMensagemErro(erro, ClasseActivity.this, null);
					Dialogos.Alerta.fecharDialogo();
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		super.onBackPressed();
	}
	
	/**
	 * Metodo que verifica se o objeto eh nulo
	 * @param o
	 * @return
	 */
	public static boolean naoExiste(Object o){
		boolean naoExiste = Classe.naoExiste(o);
		return naoExiste;
	}
	
	/**
	 * Metodo que verifica se o objeto nao eh nulo
	 * @param o
	 * @return
	 */
	public static boolean existe(Object o){
		boolean existe = !naoExiste(o);
		return existe;
	}
	
	public void ocultarBarraDeAcoes(){
		ActionBar barraDeAcoes = getActionBar();
		if (existe(barraDeAcoes) && barraDeAcoes.isShowing()) {
			barraDeAcoes.hide();
		}
	}
	
	public void exibirMensagemExcecao(Throwable e){
		Dialogos.Alerta.exibirMensagemErro(e, ClasseActivity.this, null);
	}
	
	@Override
	public void onBackPressed() {
		verificaSePediuParaEncerrar();
		boolean ehATelaPrincipal = estouNaTela(MainActivity.class);
		if (ehATelaPrincipal) {
			questionaLogout();
		} else if (estouNaTela(LoginActivity.class)) {
			questionaFecharApp();
		} else {
			super.onBackPressed();
		}
	}
	

	private void questionaFecharApp() {
		OnClickListener escutador = new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Sessao.addParametro(Constantes.SAIR, true);
				voltar();
			}
		};
		
		Dialogos.Alerta.exibirMensagemPergunta(ClasseActivity.this, false, "Deseja mesmo sair?", getString(R.string.app_name), escutador, null, null);
	}

	private void questionaLogout() {
		OnClickListener escutador = new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				deslogar();
			}
		};
		
		Dialogos.Alerta.exibirMensagemPergunta(ClasseActivity.this, false, "Deseja mesmo Desconectar?", getString(R.string.app_name), escutador, null, null);
	}
	
	public void verificaSePediuParaEncerrar(){
		boolean pediuParaEncerrar = Sessao.temParametro(Constantes.SAIR);
		if (pediuParaEncerrar) {
			voltar();
		}
	}

	public void notificar(String mensagem, Bundle parametros) {
		Dialogos.Notificacao.exibir(contexto.getContexto(), mensagem, parametros);
	}
	
	public void log(String textoParaLog) {
		contexto.log(textoParaLog);		
	}
	
	public ContextoPadrao getContexto(){
		return contexto;
	}
	
	/*public Point getTamanhoTela(){
		Display display = getWindowManager().getDefaultDisplay();
		Point tamanho = new Point();
		display.getSize(tamanho);
		return tamanho;
	}*/
	
	public void botaoHomeAtivo(boolean ativo){
		getActionBar().setDisplayHomeAsUpEnabled(ativo);
	}
	
	public Bundle getParametros(){
		return getIntent().getExtras();
	}
	

	public boolean estouNaTela(Class<? extends ClasseActivity> tela){
		return this.getClass().getSimpleName().equals(tela.getSimpleName());
	}
	
	public void voltar(){
		super.onBackPressed();
	}
	

	public <T> T mapear(int id) {
		return (T) super.findViewById(id);
	}
	
	public void mostrarBotaoHome() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void executarAnimacao(Animation animacao){
		tela = mapear(R.id.conteiner);
		tela.setAnimation(animacao);
		tela.animate().start();
	}
	
	public void executarAnimacao(int idAnimacao){
		tela = mapear(R.id.conteiner);
		Animation animacao = getAnimacao(idAnimacao);
		tela.setAnimation(animacao);
		tela.animate().start();
	}
	
	public Animation getAnimacao(int animacao) {
		return AnimationUtils.loadAnimation(getContexto().getContexto(), animacao);
	}
	
	protected ProgressDialog dialogoDeProgresso;
	
	public void mostrarDialogoDeProgresso (String texto) {
		dialogoDeProgresso = new ProgressDialog(getContexto().getContexto());
		if (!existe(texto)) {
			texto = "Processando aguarde";
		}
		dialogoDeProgresso.setTitle(texto);
		dialogoDeProgresso.show();
	}
	
	public void fecharDialogoDeProgresso (){
		if (existe(dialogoDeProgresso) && dialogoDeProgresso.isShowing()) {
			dialogoDeProgresso.dismiss();
			dialogoDeProgresso = null;
		}
	}

	private class TarefaVerificacaoSessao extends AsyncTask<String, Void, Object> {
		@Override
		protected Object doInBackground(String... params) {
			Object retorno = null;
			String login = params[0];
			String senha = params[1];
			try {
				retorno = ControladorDeUsuario.getInstancia(contexto).sessaoValida(login, senha);
			} catch (Erro e) {
				e.printStackTrace();
				return e;
			}
					
			return retorno;
		}
		
		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);
			cancel(true);
		}
		
	}
	
	class TarefaDeslogar extends AsyncTask<String, Void, Throwable> {
		
		@Override
		protected void onPreExecute() {
			Dialogos.Progresso.exibirDialogoProgresso(ClasseActivity.this);
			super.onPreExecute();
		}
		
		@Override
		protected Throwable doInBackground(String... params) {
			try {
				//Este trecho vai demorar
				String login = params[0];
				ControladorDeUsuario.getInstancia(contexto).logout(login);
			} catch (Throwable e) {
				return e;
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Throwable result) {
			Dialogos.Progresso.fecharDialogoProgresso();
			Sessao.deslogar();
			verificaSessao();
			super.onPostExecute(result);
			cancel(true);
		}
		
	}
}
