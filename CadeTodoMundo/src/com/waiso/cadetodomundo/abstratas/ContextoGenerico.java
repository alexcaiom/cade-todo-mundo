/**
 * 
 */
package com.waiso.cadetodomundo.abstratas;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * @author Waiso
 *
 */
public class ContextoGenerico {
	
	Context contexto;
	public final String CLASSE_NOME = getClass().getSimpleName();
	
	public ContextoGenerico(Context contexto) {
		this.contexto = contexto;
	}
	
	public Context getContexto() {
		return contexto;
	}

	public void setContexto(Context contexto) {
		this.contexto = contexto;
	}

	public void exibirMensagemDeProcessamento(){
		
	}
	
	protected void log(Object textoParaLog) {
		Log.i(CLASSE_NOME, textoParaLog.toString());
	}
	
	public boolean temInternet(){
		ConnectivityManager connectivityManager = (ConnectivityManager) contexto.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	public void avisar(String aviso){
		if (this.contexto instanceof ClasseActivity) {
			((ClasseActivity) contexto).getContexto().getContexto().avisar(aviso);
		}
	}

}
