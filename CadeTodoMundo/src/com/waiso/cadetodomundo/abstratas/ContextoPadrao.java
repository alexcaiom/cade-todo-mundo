/**
 * 
 */
package com.waiso.cadetodomundo.abstratas;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * @author Waiso
 *
 */
public class ContextoPadrao {
	
	ClasseActivity classeContexto;
	public final String CLASSE_NOME = getClass().getSimpleName();
	
	public ContextoPadrao(ClasseActivity contexto){
		if (contexto instanceof ClasseActivity) {
			this.classeContexto = contexto;
		}
	}

	public ClasseActivity getContexto() {
		return classeContexto;
	}

	public void setContexto(ClasseActivity classeContexto) {
		this.classeContexto = classeContexto;
	}
	
	
	public ClasseActivity getClasseContexto() {
		return classeContexto;
	}

	public void setClasseContexto(ClasseActivity classeContexto) {
		this.classeContexto = classeContexto;
	}

	public void exibirMensagemDeProcessamento(){
		
	}
	
	protected void log(Object textoParaLog) {
		Log.i(CLASSE_NOME, textoParaLog.toString());
	}
	
	public boolean temInternet(){
		ConnectivityManager connectivityManager = (ConnectivityManager) classeContexto.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	public void avisar(String aviso){
		if (this.classeContexto instanceof ClasseActivity) {
			((ClasseActivity) classeContexto).getContexto().getContexto().avisar(aviso);
		}
	}

}
