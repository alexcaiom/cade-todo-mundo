package com.waiso.cadetodomundo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.waiso.cadetodomundo.fragmentos.FragmentoAmigos;
import com.waiso.cadetodomundo.fragmentos.FragmentoBean;
import com.waiso.cadetodomundo.fragmentos.FragmentoMapa;
import com.waiso.cadetodomundo.fragmentos.FragmentoPerfil;

/*public class MainActivity2 extends ActionBarActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

	*//**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 *//*
	private NavigationDrawerFragment mNavigationDrawerFragment;

	*//**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 *//*
	private String mTitle;
	private static int tela = 0;
	private static FragmentoBean[] telas = {
			new FragmentoBean(R.layout.fragmento_mapa, 		R.menu.mapa, 	R.string.title_todos),
			new FragmentoBean(R.layout.fragmento_amigos, 	R.menu.amigos, 	R.string.title_amigos),
			new FragmentoBean(R.layout.fragmento_perfil, 	R.menu.perfil, 	R.string.title_perfil)
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main2);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.framento_mapa_navigation_drawer);
		mTitle = getTitle().toString();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.framento_mapa_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments

		FragmentManager fragmentManager = getSupportFragmentManager();
		Fragment tela = null;
		
		switch (position) {
		case 0:
			tela = new FragmentoMapa();
			break;
		case 1:
			tela = new FragmentoAmigos();		
			break;
		case 2:
			tela = new FragmentoPerfil();		
			
		default:
			break;
		}
		
//		FragmentoBean tela = telas[position];
//		if (tela.getFragmento() == null) {
//			tela.setFragmento(PlaceholderFragment.newInstance(position + 1));
//			tela.setFragmento(new FragmentoConteudo(tela.getId(), tela.get));
//		}
		
//		FrameLayout conteiner = (FrameLayout) findViewById(R.id.container);
//		if (conteiner != null && conteiner.getChildCount() > 0) {
//			conteiner.removeAllViews();
//		}
		
//		if (tela.getFragmento().getChildFragmentManager() != null){
//			 fragmentManager.beginTransaction().replace(tela.getFragmento().getChildFragmentManager()., tela.getFragmento()).commit();
//		} else {
//			fragmentManager.beginTransaction().replace(R.id.container, tela.getFragmento()) 
//			.commit();
//			fragmentManager.beginTransaction().replace(R.id.container, new FragmentoConteudo(tela.getId(), tela.getTitulo())).commit();
			fragmentManager.beginTransaction().replace(R.id.framento_mapa_container, tela).commit();
//		}
	}

	public void onSectionAttached(int number) {
		switch (number) {
		case 1:
			mTitle = getString(R.string.title_todos);
			break;
		case 2:
			mTitle = getString(R.string.title_amigos);
			break;
		case 3:
			mTitle = getString(R.string.title_perfil);
			break;
		}
		tela = number - 1;
	}

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			FragmentoBean fragmento = telas[tela];
			getMenuInflater().inflate(fragmento.getMenu(), menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}*/
