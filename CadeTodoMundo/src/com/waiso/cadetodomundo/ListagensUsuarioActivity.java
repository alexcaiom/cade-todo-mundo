package com.waiso.cadetodomundo;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.waiso.cadetodomundo.abstratas.Classe;
import com.waiso.cadetodomundo.abstratas.ContextoGenerico;
import com.waiso.cadetodomundo.controladores.ControladorDeUsuario;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.gui.adaptadores.AdaptadorAmigos;
import com.waiso.cadetodomundo.gui.adaptadores.AdaptadorSolicitacoesDeAmizade;
import com.waiso.cadetodomundo.gui.adaptadores.AdaptadorUsuarios;
import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.tarefas.TarefaListagemSolicitacoes;
import com.waiso.cadetodomundo.tarefas.TarefaListagemUsuarios;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Dialogos;
import com.waiso.cadetodomundo.utils.Sessao;

public class ListagensUsuarioActivity extends FragmentActivity implements ActionBar.TabListener {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
	 * derivative, which will keep every loaded fragment in memory. If this
	 * becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	int abaSelecionada = 0;
	EditText txtUsuario = null;
	ImageView btnAtualizar = null;
	ListView lista = null;
	List<Usuario> usuarios = null;
	List<Usuario> amigos = null;
	List<SolicitacaoDeAmizade> solicitacoes = null;
	public ListagensUsuarioActivity contexto = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_listagens_usuario);
		
		verificaSessao();
		
		usuarios = (List<Usuario>) Sessao.getParametro("usuarios");
		amigos = (List<Usuario>) Sessao.getParametro("amigos");
		solicitacoes = (List<SolicitacaoDeAmizade>) Sessao.getParametro("solicitacoes");
		
		if (Classe.existe(usuarios) && Classe.existe(amigos) && Classe.existe(solicitacoes)) {
			atualizarAdaptadoresEComponenteDeTela(this);
		}
		
		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab().setText(mSectionsPagerAdapter.getPageTitle(i)).setTabListener(this));
		}
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		contexto = this;
	}
	
	android.view.View.OnClickListener escutadorDeCliquesUsuario = new  android.view.View.OnClickListener() {
		@Override
		public void onClick(View v) {
			try {
				Object[] params = new Object[]{
						abaSelecionada,
						contexto,
						txtUsuario.getText().toString()						
				};
				usuarios = (List<Usuario>) new TarefaListagemUsuarios().execute(params).get();
				
				atualizarListas(ListagensUsuarioActivity.this);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}			
		}
	};
	
	android.view.View.OnClickListener escutadorDeCliquesAmigos = new  android.view.View.OnClickListener() {
		@Override
		public void onClick(View v) {
			try {
				Object[] params = new Object[]{
						abaSelecionada,
						contexto,
						txtUsuario.getText().toString()
				};
				amigos = (List<Usuario>) new TarefaListagemUsuarios().execute(params).get();
				
				atualizarListas(ListagensUsuarioActivity.this);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.listagens_usuario, menu);
		
//		View layoutCampoPesquisa = menu.findItem(R.id.action_pesquisa).getActionView();
//		EditText txtPesquisa = (EditText) layoutCampoPesquisa.findViewById(R.id.txt_search);
//		txtPesquisa.setOnEditorActionListener(new OnEditorActionListener() {
//			@Override
//			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//				Toast.makeText(getBaseContext(), "Search : " + v.getText(), Toast.LENGTH_SHORT).show();
//				return false;
//			}
//		});
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class
			// below).
			return new PlaceholderFragment().newInstance(position + 1);
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_listagens_usuario, container, false);
			txtUsuario = (EditText) rootView.findViewById(R.id.listagens_usuario);
			btnAtualizar = (ImageView) rootView.findViewById(R.id.btnPesquisar);
			lista = (ListView) rootView.findViewById(R.id.listagens_usuario_lista);
			Bundle parametros =  getArguments();
			if (parametros != null && parametros.containsKey(ARG_SECTION_NUMBER)) {
				abaSelecionada = parametros.getInt(ARG_SECTION_NUMBER);
			}
			
			atualizarListas(contexto);
			
			switch (abaSelecionada) {
			case 1:
				txtUsuario.setVisibility(View.VISIBLE);
				txtUsuario.setHint("Usuario(a)");
				btnAtualizar.setOnClickListener(escutadorDeCliquesUsuario);
				btnAtualizar.setVisibility(View.VISIBLE);
				break;
			case 2:
				txtUsuario.setVisibility(View.VISIBLE);
				txtUsuario.setHint("Amigo(a)");
				btnAtualizar.setOnClickListener(escutadorDeCliquesAmigos);
				btnAtualizar.setVisibility(View.VISIBLE);
				break;
			case 3:
				txtUsuario.setVisibility(View.GONE);
				btnAtualizar.setVisibility(View.GONE);
				break;

			default:
				break;
			}
			
			return rootView;
		}
	}

	@Override
	public void onTabReselected(Tab tab, android.app.FragmentTransaction arg1) {
		abaSelecionada = tab.getPosition();
		atualizarAdaptadoresEComponenteDeTela(this);
	}

	@Override
	public void onTabSelected(Tab tab, android.app.FragmentTransaction arg1) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab arg0, android.app.FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}

	public void atualizarListas(ListagensUsuarioActivity tela){
		atualizarListasComOsDadosRemotos(tela);
		atualizarAdaptadoresEComponenteDeTela(tela);
	}
	
	public void atualizarListasComOsDadosRemotos (ListagensUsuarioActivity tela){
		Object[] params = null;
		ContextoGenerico contexto = new ContextoGenerico(tela);
		switch(abaSelecionada){
		case 1:
			try {
				params = new Object[]{
						abaSelecionada,
						contexto
				};
				amigos = (List<Usuario>) new TarefaListagemUsuarios().execute(params).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		case 2:
			try {
				params = new Object[]{
						abaSelecionada,
						contexto
				};
				amigos = (List<Usuario>) new TarefaListagemUsuarios().execute(params).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		case 3:
			try {
				params = new Object[]{
						contexto
				};
				solicitacoes = (List<SolicitacaoDeAmizade>) new TarefaListagemSolicitacoes().execute(params).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private void atualizarAdaptadoresEComponenteDeTela(ListagensUsuarioActivity tela){
		ArrayAdapter<?> adaptador = null;
		switch (abaSelecionada) {
		case 1:
			 adaptador = new AdaptadorUsuarios(tela, amigos, abaSelecionada);	
			break;
		case 2:
			adaptador = new AdaptadorAmigos(tela, amigos, abaSelecionada);
			break;
		case 3:
			adaptador = new AdaptadorSolicitacoesDeAmizade(tela, solicitacoes, abaSelecionada);
		default:
			break;
		}
		
		lista.setAdapter(adaptador);
	}
	
	private void verificaSessao(){
		Usuario usuario = (Usuario) Sessao.getParametro(Constantes.USUARIO);
		Object retorno;
		try {
			retorno = new TarefaVerificacaoSessao().execute(usuario.getLogin(), usuario.getSenha()).get();
			if (retorno instanceof Boolean) {
				Boolean estaLogadoNoServidor = (Boolean) retorno;
				if (!estaLogadoNoServidor) {
					Sessao.deslogar();
					OnClickListener escutadorOk = new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent intencao = new Intent(ListagensUsuarioActivity.this, LoginActivity.class);
							startActivity(intencao);
						}
					};
					Toast.makeText(ListagensUsuarioActivity.this, "Sessao expirada! Logue-se novamente.", Toast.LENGTH_SHORT).show();
					Intent intencao = new Intent(ListagensUsuarioActivity.this, LoginActivity.class);
					startActivity(intencao);
				}
			} else if (retorno instanceof Erro) {
				Dialogos.Alerta.exibirMensagemErro(((Erro)retorno), ListagensUsuarioActivity.this, null);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	private class TarefaVerificacaoSessao extends AsyncTask<String, Void, Object> {
		@Override
		protected Object doInBackground(String... params) {
			Object retorno = null;
			String login = params[0];
			String senha = params[1];
			try {
				retorno = ControladorDeUsuario.getInstancia(new ContextoGenerico(contexto)).sessaoValida(login, senha);
			} catch (Erro e) {
				e.printStackTrace();
				return e;
			}
					
			return retorno;
		}
		
		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);
			cancel(true);
		}
		
	}
	
}
