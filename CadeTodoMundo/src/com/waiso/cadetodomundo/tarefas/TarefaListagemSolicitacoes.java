package com.waiso.cadetodomundo.tarefas;

import java.util.List;

import com.waiso.cadetodomundo.abstratas.ContextoGenerico;
import com.waiso.cadetodomundo.controladores.ControladorDeUsuario;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;
import android.os.AsyncTask;

public class TarefaListagemSolicitacoes extends AsyncTask<Object, Void, List<?>> {
		
		@Override
		protected List<?> doInBackground(Object... params) {
			List<SolicitacaoDeAmizade> solicitacoes = null;
			ContextoGenerico contexto = (ContextoGenerico) params[0];
			try {
					//Solicitacoes de Amizade
				solicitacoes = ControladorDeUsuario.getInstancia(contexto).listarSolicitacoesDeAmizade();
			} catch (Exception e) {
				e.printStackTrace();
			} catch (Erro e) {
				e.printStackTrace();
			}
			return solicitacoes;
		}
		
	}
	