package com.waiso.cadetodomundo.tarefas;

import java.util.List;

import com.waiso.cadetodomundo.abstratas.ContextoGenerico;
import com.waiso.cadetodomundo.controladores.ControladorDeUsuario;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import android.os.AsyncTask;

public class TarefaListagemUsuarios extends AsyncTask<Object, Void, List<?>> {

		@Override
		protected List<?> doInBackground(Object... params) {
			Integer abaSelecionada = (Integer) params[0];
			List<Usuario> amigos = null;
			ContextoGenerico contexto = (ContextoGenerico) params[1];
			String usuario = "";
			if (params.length == 3) {
				usuario = (String) params[2];
			}
			try {
				switch (abaSelecionada) {
				case 1:
					//Todos os Usuarios
					amigos = ControladorDeUsuario.getInstancia(contexto).listarOutrosUsuariosComNomeComo(usuario);
					break;
				case 2:
					//Amigos
					amigos = ControladorDeUsuario.getInstancia(contexto).listarAmigosComNomeComo(usuario);
					break;
				default:
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			} catch (Erro e) {
				e.printStackTrace();
			}
			return amigos;
		}
		
	}