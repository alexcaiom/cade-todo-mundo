package com.waiso.cadetodomundo.gui.adaptadores;

import java.util.List;
import java.util.concurrent.ExecutionException;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.waiso.cadetodomundo.CadastroUsuarioActivity;
import com.waiso.cadetodomundo.ListagensUsuarioActivity;
import com.waiso.cadetodomundo.R;
import com.waiso.cadetodomundo.abstratas.ContextoGenerico;
import com.waiso.cadetodomundo.abstratas.ContextoPadrao;
import com.waiso.cadetodomundo.controladores.ControladorDeUsuario;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Dialogos;
import com.waiso.cadetodomundo.utils.Sessao;

public class AdaptadorUsuarios extends ArrayAdapter<Usuario> {

	private ListagensUsuarioActivity contexto;
	private List<Usuario> usuarios;
	private int lista;
	
	public AdaptadorUsuarios(ListagensUsuarioActivity contexto, List<Usuario> amigos, int numeroDaLista) {
		super(contexto, R.id.tela_amigos_lista);
		this.contexto = contexto;
		this.usuarios = amigos;
		this.lista = numeroDaLista;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflador = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final RelativeLayout item = (RelativeLayout) inflador.inflate(R.layout.item_usuario, null);

		final Usuario usuario = usuarios.get(position);
		ImageView imgAmigo = (ImageView) item.findViewById(R.id.item_usuario_img);
		imgAmigo.setImageDrawable(contexto.getResources().getDrawable(R.drawable.ic_launcher));
		
		TextView labelNome = (TextView) item.findViewById(R.id.item_usuario_lblNome);
		String id = usuario.getId().toString();
		labelNome.setText(/*id + " - " + */usuario.getNome());
	
		TextView lblStatus = (TextView) item.findViewById(R.id.item_usuario_lblStatus);
//		lblStatus.setText(amigo.getNome());
		
		//Adicionando a acao de selecionar
		OnClickListener acaoSolicitacaoDeAmizade = new OnClickListener() {
			@Override
			public void onClick(View v) {
				android.content.DialogInterface.OnClickListener comportamentoSim = new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Usuario eu = (Usuario) Sessao.getParametro(Constantes.USUARIO);
						boolean sucesso = false;
						ContextoGenerico c = new ContextoGenerico(contexto);
						switch (lista) {
						case 1:
							try {
								sucesso = new TarefaSolicitacaoDeAmizade().execute(c, eu, usuario).get();
								if (sucesso) {
									Toast.makeText(c.getContexto(), "Sua solicitação de amizade foi enviada. Aguarde a aceitação de seu amigo(a). :)", Toast.LENGTH_LONG).show();
									contexto.atualizarListas(contexto);
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (ExecutionException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						default:
							break;
						}
					}
				};
				String mensagem = "";
				switch (lista) {
				case 1:
					mensagem = "Confirma adição de Amigo?";
					Dialogos.Alerta.exibirMensagemPergunta(getContext(), true, mensagem, null, comportamentoSim, null, null);
					break;
				default:
					break;
				}
			}
		};
		
		switch (lista) {
		case 1:
			item.setOnClickListener(acaoSolicitacaoDeAmizade);
			break;
		default:
			break;
		}
		
		ViewHolder holder = new ViewHolder();
		holder.imgAmigo = imgAmigo;
		holder.labelNome = labelNome;
		holder.labelStatus = lblStatus;
		if (convertView == null) {
			convertView = item;
		}
		convertView.setTag(holder);
		return convertView;
	}
	
	@Override
	public int getCount() {
		return usuarios.size();
	}
	
	@Override
	public Usuario getItem(int position) {
		return usuarios.get(position);
	}
	
	
	static class ViewHolder {
		ImageView imgAmigo;
		TextView labelStatus;
		TextView labelNome;
	}
	
	class TarefaSolicitacaoDeAmizade extends AsyncTask<Object, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Object... params) {
			
			ContextoGenerico c = (ContextoGenerico) (params[0]);
			Usuario eu = (Usuario) params[1];
			Usuario amigo = (Usuario) params[2];
			try {
				return ControladorDeUsuario.getInstancia(c).associarUsuarios(eu, amigo);
			} catch (Erro e) {
				e.printStackTrace();
				return false;
			}
		}
	}
	
}
