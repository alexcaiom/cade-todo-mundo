package com.waiso.cadetodomundo.gui.adaptadores;

import java.util.List;
import java.util.concurrent.ExecutionException;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.sax.StartElementListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.stmt.query.Exists;
import com.waiso.cadetodomundo.CadastroUsuarioActivity;
import com.waiso.cadetodomundo.ListagensUsuarioActivity;
import com.waiso.cadetodomundo.R;
import com.waiso.cadetodomundo.abstratas.Classe;
import com.waiso.cadetodomundo.abstratas.ContextoGenerico;
import com.waiso.cadetodomundo.abstratas.ContextoPadrao;
import com.waiso.cadetodomundo.controladores.ControladorDeUsuario;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Dialogos;
import com.waiso.cadetodomundo.utils.Sessao;

public class AdaptadorAmigos extends ArrayAdapter<Usuario> {

	private ListagensUsuarioActivity contexto;
	private List<Usuario> amigos;
	private int lista;
	
	public AdaptadorAmigos(ListagensUsuarioActivity contexto, List<Usuario> amigos, int numeroDaLista) {
		super(contexto, R.id.tela_amigos_lista);
		this.contexto = contexto;
		this.amigos = amigos;
		this.lista = numeroDaLista;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflador = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final RelativeLayout item = (RelativeLayout) inflador.inflate(R.layout.item_amigo, null);

		final Usuario amigo = amigos.get(position);
		ImageView imgAmigo = (ImageView) item.findViewById(R.id.item_amigo_img);
		imgAmigo.setImageDrawable(contexto.getResources().getDrawable(R.drawable.ic_launcher));
		
		TextView labelNome = (TextView) item.findViewById(R.id.item_amigo_lblNome);
		String id = amigo.getId().toString();
		labelNome.setText(/*id + " - " + */amigo.getNome());
	
		TextView lblStatus = (TextView) item.findViewById(R.id.item_amigo_lblStatus);
		String status = amigo.getStatus();
		if (Classe.existe(status) && !status.isEmpty()) {
			lblStatus.setText(status);
		} else {
			lblStatus.setText("");
		}
		
		ImageButton btnApontarAmigo = (ImageButton) item.findViewById(R.id.item_amigo_btnApontarAmigo);
		
		OnClickListener acaoVerAmigo = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intencao = new Intent(contexto, CadastroUsuarioActivity.class);
				Bundle parametros = new Bundle();
				parametros.putSerializable(Constantes.ITEM_AMIGO, amigo);
				parametros.putBoolean("editar", false);
				intencao.putExtras(parametros);
				contexto.startActivity(intencao);
			}
		};
		
		
		OnClickListener acaoApontarAmigo = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent returnIntent = new Intent();
				returnIntent.putExtra(Constantes.ITEM_AMIGO, amigo);
				contexto.setResult(ListagensUsuarioActivity.RESULT_OK,returnIntent);
				contexto.finish();
			}
		};
		
		
		
		switch (lista) {
		case 2:
			item.setOnClickListener(acaoVerAmigo);
			btnApontarAmigo.setOnClickListener(acaoApontarAmigo);
			break;
		default:
			break;
		}
		
		ViewHolder holder = new ViewHolder();
		holder.imgAmigo = imgAmigo;
		holder.labelNome = labelNome;
		holder.labelStatus = lblStatus;
		if (convertView == null) {
			convertView = item;
		}
		convertView.setTag(holder);
		return convertView;
	}
	
	@Override
	public int getCount() {
		return amigos.size();
	}
	
	@Override
	public Usuario getItem(int position) {
		return amigos.get(position);
	}
	
	
	static class ViewHolder {
		ImageView imgAmigo;
		TextView labelStatus;
		TextView labelNome;
	}
	
	class TarefaSolicitacaoDeAmizade extends AsyncTask<Object, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Object... params) {
			
			ContextoGenerico c = (ContextoGenerico) (params[0]);
			Usuario eu = (Usuario) params[1];
			Usuario amigo = (Usuario) params[2];
			try {
				return ControladorDeUsuario.getInstancia(c).associarUsuarios(eu, amigo);
			} catch (Erro e) {
				e.printStackTrace();
				return false;
			}
		}
	}
	
}
