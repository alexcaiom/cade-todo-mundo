package com.waiso.cadetodomundo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentoConteudo extends Fragment {
	
	private int idTela = 0;
	private int idTextoTitulo = 0 ;

	public FragmentoConteudo() {}
	public FragmentoConteudo(int idTela, int idTextoTitulo) {
		this.idTela = idTela;
		this.idTextoTitulo = idTextoTitulo; 
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(idTela, container, false);
		return rootView;
	}

}
