package com.waiso.cadetodomundo.fragmentos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.waiso.cadetodomundo.R;

public class FragmentoMapa extends Fragment {

	private GoogleMap mapa;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View tela = inflater.inflate(R.layout.fragmento_mapa, container, false);
		mapa = ((MapFragment)getActivity().getFragmentManager().findFragmentById(R.id.fragmento_mapa_mapa)).getMap();
		return tela;
	}
	
}
