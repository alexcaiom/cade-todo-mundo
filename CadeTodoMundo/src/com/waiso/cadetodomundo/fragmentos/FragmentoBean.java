package com.waiso.cadetodomundo.fragmentos;


public class FragmentoBean {

	private int id;
	private int menu;
	private int titulo;
	
	public FragmentoBean() {}
	public FragmentoBean(int id, int menu, int titulo) {
		this.id = id;
		this.menu = menu;
		this.titulo = titulo;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMenu() {
		return menu;
	}
	public void setMenu(int menu) {
		this.menu = menu;
	}
	public int getTitulo() {
		return titulo;
	}
	public void setTitulo(int titulo) {
		this.titulo = titulo;
	}
	
}
