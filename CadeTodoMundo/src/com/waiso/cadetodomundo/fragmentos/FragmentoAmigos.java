package com.waiso.cadetodomundo.fragmentos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.waiso.cadetodomundo.R;

public class FragmentoAmigos extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View tela = inflater.inflate(R.layout.fragmento_amigos, null);
		return tela;
	}
	
}
