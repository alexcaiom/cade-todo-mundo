package com.waiso.cadetodomundo;

import java.util.List;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.waiso.cadetodomundo.abstratas.ClasseActivity;
import com.waiso.cadetodomundo.controladores.ControladorDeUsuario;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.gui.adaptadores.AdaptadorAmigos;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Sessao;

public class ListaAmigosActivity extends ClasseActivity {

	private ListView lista;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lista_amigos);
		botaoHomeAtivo(true);
		carregarTela();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lista_amigos, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void carregarTela() {
		lista = (ListView) findViewById(R.id.tela_amigos_lista);
		preencheLista();
	}

	private void preencheLista() {
		/*boolean listaEstaNaSessao = Sessao.temParametro("listaAmigos");
		if (listaEstaNaSessao) {
			
		}*/
		
		List<Usuario> amigos = null;
		try {
			amigos = ControladorDeUsuario.getInstancia(contexto).listarOutrosUsuariosComNomeComo("");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Erro e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		AdaptadorAmigos adaptador = null;
//		if (existe(amigos)) {
//			adaptador = new AdaptadorAmigos(contexto, amigos,1);
//		}
		
		if (existe(adaptador)) {
			lista.setAdapter(adaptador);
		}
	}

	@Override
	public void carregarEventos() {
		// TODO Auto-generated method stub
		
	}
}
