/**
 * 
 */
package com.waiso.cadetodomundo.facade;

import java.util.List;

import com.waiso.cadetodomundo.abstratas.ContextoGenerico;
import com.waiso.cadetodomundo.abstratas.ContextoPadrao;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.orm.bo.BOUsuario;
import com.waiso.cadetodomundo.orm.dao.DAOUsuario;
import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.servicos.remotos.ServicoUsuarioRemoto;
import com.waiso.cadetodomundo.utils.Dialogos;
import com.waiso.cadetodomundo.utils.Sessao;

/**
 * @author Alex
 *
 */
public class FacadeUsuario extends Facade {

	private static FacadeUsuario instancia;
	
	public void logar(String login, String senha) throws Erro{
		//Se o usuario estiver logado e nao tiver internet, devera conectar-se novamente para Logar-se.
		boolean temInternet = temInternet(); 
		if (temInternet) {
			ServicoUsuarioRemoto.getInstancia(contextoPadrao).logar(login, senha);
		} else {
			Dialogos.Alerta.exibirMensagemInformacao(contextoPadrao, false, "Conecte-se e tente novamente!", null);
		}
		
//		BOUsuario.getInstancia(contextoPadrao).autentica(login, senha);
	}

	public void deslogar(String login) throws Erro {
		if (temInternet()) {
			ServicoUsuarioRemoto.getInstancia(contextoPadrao).deslogar(login);
		} else {
			Dialogos.Alerta.exibirMensagemInformacao(contextoPadrao, false, "Conecte-se e tente novamente!", null);
		}
	}

	public void cadastrar(Usuario usuario) throws Erro {
		if (temInternet()) {
			Usuario usuarioCadastrado = ServicoUsuarioRemoto.getInstancia(contextoPadrao).cadastrar(usuario);
			logar(usuarioCadastrado.getLogin(), usuarioCadastrado.getSenha());
		} else {
			Dialogos.Alerta.exibirMensagemInformacao(contextoPadrao, false, "Conecte-se e tente novamente!", null);
		}
		
//		BOUsuario.getInstancia(contextoPadrao).inserir(usuario);
	}
	
	public boolean associarUsuarios (Usuario eu, Usuario amigo) throws Erro {
		if (temInternet()) {
			return ServicoUsuarioRemoto.getInstancia(contextoPadrao).gerarSolicitacao(eu, amigo);
		}
		return DAOUsuario.getInstancia(contextoPadrao).associarUsuarios(eu, amigo);
	}
	
	public boolean confirmarSolicitacao (Usuario eu, Usuario amigo) throws Erro {
		if (temInternet()) {
			return ServicoUsuarioRemoto.getInstancia(contextoPadrao).confirmarSolicitacao(eu, amigo);
		}
		return DAOUsuario.getInstancia(contextoPadrao).confirmarAmizade(eu, amigo);
	}
	
	public void alterar(Usuario usuario) throws Erro {
		if (temInternet()) {
			ServicoUsuarioRemoto.getInstancia(contextoPadrao).alterar(usuario);		
		} else {
			Dialogos.Alerta.exibirMensagemInformacao(contextoPadrao, false, "Conecte-se e tente novamente!", null);
		}
		
		BOUsuario.getInstancia(contextoPadrao).alterar(usuario);
	}
	
	public boolean verificaSessao(String login, String senha) throws Erro {
		if (temInternet()) {
			return ServicoUsuarioRemoto.getInstancia(contextoPadrao).verificaSessao(login, senha);
		} else {
			return Sessao.usuarioLogado();
		}
	}
	
	
	public List<Usuario> listar() throws Exception, Erro {
		if (temInternet()) {
			return ServicoUsuarioRemoto.getInstancia(contextoPadrao).listar();
		}
		return BOUsuario.getInstancia(contextoPadrao).listar();
	}
	
	public List<Usuario> listarAmigosComNomeComo(String nome) throws Exception, Erro {
		boolean temInternet = temInternet(); 
		if (temInternet) {
			return ServicoUsuarioRemoto.getInstancia(getContextoPadrao()).listarAmigosComNomeComo(nome);
		}
		return BOUsuario.getInstancia(contextoPadrao).listarAmigosComNomeComo(nome);
	}
	
	public List<Usuario> listarOutrosUsuariosComNomeComo(String nome) throws Exception, Erro {
		boolean temInternet = temInternet(); 
		if (temInternet) {
			return ServicoUsuarioRemoto.getInstancia(getContextoPadrao()).listarOutrosUsuariosComNomeComo(nome);
		}
		return BOUsuario.getInstancia(contextoPadrao).listarOutrosUsuariosComNomeComo(nome);
	}
	
	public List<SolicitacaoDeAmizade> listarSolicitacoesDeAmizade() throws Exception, Erro {
		boolean temInternet = temInternet(); 
		if (temInternet) {
			return ServicoUsuarioRemoto.getInstancia(getContextoPadrao()).listarSolicitacoesDeAmizade();
		}
		return BOUsuario.getInstancia(contextoPadrao).listarSolicitacoesDeAmizade();
	}
	
	public static FacadeUsuario getInstancia(ContextoPadrao contexto){
		if (naoExiste(instancia)) {
			instancia = new FacadeUsuario();
			instancia.contextoPadrao = contexto;
		}
		return instancia;
	}
	
	public static FacadeUsuario getInstancia(ContextoGenerico contexto){
		if (naoExiste(instancia)) {
			instancia = new FacadeUsuario();
			instancia.contextoGenerico = contexto;
		}
		return instancia;
	}
}
