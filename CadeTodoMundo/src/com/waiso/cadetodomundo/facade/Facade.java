package com.waiso.cadetodomundo.facade;

import com.waiso.cadetodomundo.abstratas.Classe;
import com.waiso.cadetodomundo.abstratas.ContextoGenerico;
import com.waiso.cadetodomundo.abstratas.ContextoPadrao;

public class Facade extends Classe {
	
	protected ContextoPadrao contextoPadrao;
	protected ContextoGenerico contextoGenerico;
	
	protected boolean temInternet(){
		return contextoPadrao.temInternet();
	}

	public ContextoPadrao getContextoPadrao() {
		return contextoPadrao;
	}

	public void setContextoPadrao(ContextoPadrao contextoPadrao) {
		this.contextoPadrao = contextoPadrao;
	}

	public ContextoGenerico getContextoGenerico() {
		return contextoGenerico;
	}

	public void setContextoGenerico(ContextoGenerico contextoGenerico) {
		this.contextoGenerico = contextoGenerico;
	}

}
