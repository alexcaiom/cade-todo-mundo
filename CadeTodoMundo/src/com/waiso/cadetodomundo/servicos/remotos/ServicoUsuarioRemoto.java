/**
 * 
 */
package com.waiso.cadetodomundo.servicos.remotos;

import java.util.List;

import com.waiso.cadetodomundo.abstratas.Classe;
import com.waiso.cadetodomundo.abstratas.ContextoPadrao;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.excecoes.ErroNegocio;
import com.waiso.cadetodomundo.excecoes.SysErr;
import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Sessao;
import com.waiso.cadetodomundo.ws.implementacoes.SolicitacaoWS;
import com.waiso.cadetodomundo.ws.implementacoes.UsuarioWS;

/**
 * @author AlexDell
 *
 */
public class ServicoUsuarioRemoto extends Classe {

	private static ServicoUsuarioRemoto instancia;
	private ContextoPadrao contexto;
	
	public void logar(String login, String senha) throws Erro{
		Usuario usuario = null;
		try {
//			usuario = new Usuario(login, senha, EnumUsuarioAutenticado.SUCESSO.getCodigo());
			usuario = UsuarioWS.getInstancia(contexto).login(login, senha);
		} catch (Erro e){
			throw e;
		}
		Sessao.addParametro(Constantes.USUARIO, usuario);
	}

	public void deslogar(String login) throws Erro {
		UsuarioWS.getInstancia(contexto).logout(login);
	}

	public Usuario cadastrar(Usuario usuario) throws Erro {
		return UsuarioWS.getInstancia(contexto).cadastrar(usuario);
	}
	
	public void alterar(Usuario usuario) throws Erro {
		UsuarioWS.getInstancia(contexto).alterar(usuario);
	}

	public void excluir(Usuario usuario) throws ErroNegocio, SysErr {
		UsuarioWS.getInstancia(contexto).excluir(usuario);
	}
	
	public List<Usuario> listar() throws Erro {
		List<Usuario> usuarios = UsuarioWS.getInstancia(contexto).listar();
		return usuarios;
	}
	
	public List<Usuario> listarAmigosComNomeComo(String nome) throws Erro {
		return UsuarioWS.getInstancia(contexto).listarAmigosComNomeComo(nome);
	}

	public List<Usuario> listarOutrosUsuariosComNomeComo(String nome) throws Erro {
		return UsuarioWS.getInstancia(contexto).listarOutrosUsuariosComNomeComo(nome);
	}

	public List<SolicitacaoDeAmizade> listarSolicitacoesDeAmizade() throws Erro {
		return SolicitacaoWS.getInstancia(contexto).listarSolicitacoesDeAmizade();
	}
	
	public boolean verificaSessao(String login, String senha) throws Erro {
		return UsuarioWS.getInstancia(contexto).verificaSessao(login, senha);
	}
	
	public boolean gerarSolicitacao(Usuario eu, Usuario amigo) throws Erro {
		return SolicitacaoWS.getInstancia(contexto).gerarSolicitacao(eu, amigo);
	}
	
	public boolean confirmarSolicitacao(Usuario eu, Usuario amigo) throws Erro {
		return SolicitacaoWS.getInstancia(contexto).confirmarSolicitacao(eu, amigo);
	}

	public static ServicoUsuarioRemoto getInstancia(ContextoPadrao contexto){
		if (naoExiste(instancia)) {
			instancia = new ServicoUsuarioRemoto();
			instancia.contexto = contexto;
		}
		return instancia;
	}

}
