/**
 * 
 */
package com.waiso.cadetodomundo.ws;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.waiso.cadetodomundo.abstratas.Classe;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.excecoes.ErroNegocio;
import com.waiso.cadetodomundo.excecoes.SysErr;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.ws.implementacoes.utils.MetodoHTTP;

/**
 * @author Alex
 *
 */
public class WSAcoes extends Classe {
	
	private static WSAcoes instancia = null;

	//você precisar colocar o endereco do pc em que encontra seu servidor tomcat
//	protected final String enderecoServidor = "152.251.102.247";
//	protected final String enderecoServidor = "177.60.189.183";
//	protected final String enderecoServidor = "192.168.1.59";
//	protected final String enderecoServidor = "192.168.0.12";
	protected final String enderecoServidor = "alexcaiom.com.br";
	protected final String porta="";
//	protected final String porta=":8080";
//	protected final String porta=":8081";
//	protected final String porta=":8010";
//	protected final int porta=8081;
	protected String entidade = "";
	protected String url = "";
	protected List<NameValuePair> parametros = new ArrayList<NameValuePair>();
	ChamadaHTTP webService = null;
	protected static MetodoHTTP metodo = null;
	Context contexto = null;
	
	protected static String classeControladoraNoServidor = "";
	
    /**
     * Pega Lista de Usuarios do Servidor
     * @throws Erro 
     * @throws JSONException 
     */
    /*public void pesquisarUsuariosPorIPComo(String ip) {
    	url = "http://"+enderecoServidor+":"+porta+"/MyIPRest/rest/usuario/pesquisarPorIPComo/"+ip;
    	
    	webService = new WebService(url, contexto);
    	
    	
    	//Pega a resposta do servidor
    	String response = webService.webGet("", params);
    	
    	try {
    		//Seta a resposta como um objeto JSON para acessar as informações
    		if(response !=null && !"".equalsIgnoreCase(response)){
    			JSONObject o=new JSONObject(response);
    			String out=o.get("message").toString();
    		}
    	} catch (JSONException e1) {
    		// TODO Auto-generated catch block
    		e1.printStackTrace();
    	}
    }*/
    
    public JSONArray getJSONArray(String acao, String entidade, String...parametros) throws Erro, JSONException {
    	url = Constantes.CONEXAO_PROTOCOLO+"://"+enderecoServidor+porta+"/"+Constantes.CONEXAO_CONTEXTO+"/"+entidade+"/"+acao;

    	for (String p : parametros) {
    		url += "/" + p;
    	}

    	String resposta = comunicarComOServidor(url);

    	JSONArray array = null;
    	boolean aRespostaEhUmJSONArrayValido = existe(resposta)  && !resposta.isEmpty() && respostaEstaSemErros(resposta) &&  !resposta.equals("HTTP entity may not be null");
    	boolean aRespostaEhNenhumRegistroEncontrado = existe(resposta)  && !resposta.isEmpty() && resposta.equals("HTTP entity may not be null");
    	if (aRespostaEhUmJSONArrayValido) {
    		try{
    			array = new JSONArray(resposta);
    		} catch(JSONException e){
    			JSONObject conteinerDaListaJSON = new JSONObject(resposta);
    			if (conteinerDaListaJSON.get(entidade) instanceof JSONArray) {
    				array = conteinerDaListaJSON.getJSONArray(entidade);
				} else {
					array = new JSONArray();
					JSONObject o = conteinerDaListaJSON.getJSONObject(entidade);
					array.put(o);
				}
    		}
    	} else if (aRespostaEhNenhumRegistroEncontrado){
    		return new JSONArray();
    	} else {
    		throw new ErroNegocio(resposta);
    	}

    	return array;
    }
    
    public JSONObject getJSONObject() throws Erro{
    	url = Constantes.CONEXAO_PROTOCOLO+"://"+enderecoServidor+porta+"/"+Constantes.CONEXAO_CONTEXTO;
    	
    	String resposta = comunicarComOServidor(url);
    	
    	JSONObject o = null;
    	try{
    		boolean aRespostaEhUmJSONOBjectValido = existe(resposta)  && !resposta.isEmpty() && respostaEstaSemErros(resposta);
    		boolean aRespostaEhUmaMensagem = !resposta.isEmpty() && resposta.contains("Mensagem:");
    		if (aRespostaEhUmJSONOBjectValido) {
				o = new JSONObject(resposta);
			} else if (aRespostaEhUmaMensagem) {
				
			}else {
				throw new ErroNegocio(resposta);
			}
    	} catch (JSONException e){
    		e.printStackTrace();
    	}
    	
    	return o;
    }
    
    public String getMensagem() throws Erro{
    	url = Constantes.CONEXAO_PROTOCOLO+"://"+enderecoServidor+porta+"/"+Constantes.CONEXAO_CONTEXTO;
    	
    	String resposta = comunicarComOServidor(url);
    	
    	boolean aRespostaEhUmaMensagem = !resposta.isEmpty() && !resposta.contains("erro:");
    	boolean aRespostaEhUmErro = !resposta.isEmpty() && resposta.contains("erro:");
    	if (aRespostaEhUmaMensagem) {
    		return resposta;
    	}else if (aRespostaEhUmErro){
    		throw new ErroNegocio(resposta);
    	}
    	return null;
    }
    
    protected String comunicarComOServidor(String url) throws Erro {
    	webService = new ChamadaHTTP(url, contexto);
    	addParametro("classe", classeControladoraNoServidor);
    	String resultado = "";
    	try {
    		switch (metodo) {
			case GET:
				resultado = webService.webGet(url, parametros);
				break;
			case POST:
				resultado = webService.doPost("acoes", parametros);
			default:
				break;
			}
    	} catch (Throwable e) {
    		if (existe(e) && existe(e.getMessage())) {
    			resultado = e.getMessage();
    		}
    	}

    	return resultado;
    }
    
    private boolean respostaEstaSemErros(String resposta) throws SysErr, ErroNegocio {
    	
		boolean contemPalavraException = resposta.contains(Constantes.EXCECAO);
		if (contemPalavraException) {
			throw new SysErr(resposta);
		}
		boolean contemPalavraErro = resposta.contains("\"erro\"");
		if (contemPalavraErro) {
			throw new ErroNegocio(resposta);
		}
		
		boolean contemTagHTML = resposta.contains("<html>");
		if (contemTagHTML) {
			throw new SysErr(resposta);
		}
		
		boolean contemConexaoRecusada = resposta.contains("Conexão") && resposta.contains("recusada");
		
		boolean respostaSemErros = !contemConexaoRecusada && !contemPalavraException;
		return respostaSemErros;
	}
    
    public String getJSonStringSeExistir(JSONObject o, String chave) throws JSONException{
    	if (existe(o) && existe(chave)) {
			if (o.has(chave)) {
				return o.getString(chave);
			}
		}
    	return null;
    }

	public static WSAcoes getInstancia(Context contexto){
    	if (instancia == null){
    		instancia = new WSAcoes();
    		instancia.contexto = contexto;
    	}
    	return instancia;
    }
	
	private String decideEnderecoServidor(){
		return "";
	}
    
	protected void addParametro(String nome, String valor) {
		parametros.add(new BasicNameValuePair(nome, valor));
	}
}
