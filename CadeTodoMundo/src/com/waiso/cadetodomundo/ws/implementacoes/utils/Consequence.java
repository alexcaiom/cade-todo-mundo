package com.waiso.cadetodomundo.ws.implementacoes.utils;

public enum Consequence {
	
	SEM_ACESSO,
	DESLOGADO_COM_SUCESSO,
	ERRO,
	SUCCESSO,
	MUITOS_ERROS,
	ATENCAO,
	TENTE_NOVAMENTE,
	PARE_DE_TENTAR_NOVAMENTE;
}