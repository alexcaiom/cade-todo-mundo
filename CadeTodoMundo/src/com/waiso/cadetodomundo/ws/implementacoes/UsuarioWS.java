/**
 * 
 */
package com.waiso.cadetodomundo.ws.implementacoes;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.waiso.cadetodomundo.abstratas.ContextoPadrao;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.excecoes.ErroNegocio;
import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Sessao;
import com.waiso.cadetodomundo.utils.UtilsData;
import com.waiso.cadetodomundo.ws.WSAcoes;
import com.waiso.cadetodomundo.ws.implementacoes.utils.Consequence;
import com.waiso.cadetodomundo.ws.implementacoes.utils.MetodoHTTP;

/**
 * @author AlexDell
 *
 */
public class UsuarioWS extends WSAcoes {
	
	private static UsuarioWS instancia;
	private ContextoPadrao contexto;

	public Usuario login(String login, String senha) throws Erro{
		String acao = "autentica";
		addParametro("metodo", acao);
		Usuario usuario = null;
		try{
			addParametro("login", login);
			addParametro("senha", senha);
			JSONObject resposta = getJSONObject();
			String consequence = resposta.getString("consequence");
			if (consequence.equals(Consequence.ERRO.name())) {
				throw new ErroNegocio(resposta.getString("message"));
			} else if (consequence.equals(Consequence.SUCCESSO.name())) {
				JSONObject dado = resposta.getJSONObject("dado");
				usuario = new Usuario();
				preencheVo(usuario, dado);
			}
			
		}catch (Erro e){
			throw e;
		} catch (Exception e) {
			throw new Erro(e);
		}
		
		return usuario;
	}
	

	public void logout(String login) throws Erro {
		String acao = "logout";
		addParametro("metodo", acao);
		boolean sucesso = false;
		try{
			addParametro("login", login);
			String textoResposta = getMensagem();
			JSONObject resposta = new JSONObject(textoResposta);
			if (resposta.has("dado")) {
				sucesso = Boolean.valueOf(resposta.getBoolean("dado"));
			}
		}catch (Erro e){
			throw e;
		} catch (Exception e) {
			throw new Erro(e);
		}
	}

	public Usuario cadastrar(Usuario usuario) throws Erro {
		String acao = "inserir";
		addParametro("metodo", acao);
		try{
			String nome = usuario.getNome();
			String email = usuario.getEmail();
			String login = usuario.getLogin();
			String senha = usuario.getSenha();
			String imei = usuario.getImei();
			addParametro("nome", nome);
			addParametro("email", email);
			addParametro("login", login);
			addParametro("senha", senha);
			addParametro("imei", imei);
			
			JSONObject resposta = getJSONObject();
			JSONObject dado = resposta.getJSONObject("dado");
			usuario = new Usuario();
			preencheVo(usuario, dado);
			
		}catch (Erro e){
			throw e;
		} catch (Exception e) {
			throw new Erro(e);
		}
		return usuario;
	}

	public boolean alterar(Usuario usuario) throws Erro {
		String acao = "atualizar";
		addParametro("metodo", acao);
		try{
			String nome = usuario.getNome();
			String email = usuario.getEmail();
			String login = usuario.getLogin();
			String senha = usuario.getSenha();
			String imei = usuario.getImei();
			String status = usuario.getStatus();
			String data = "";
			if (existe(usuario.getDataDeNascimento())) {
				data = ((Long) usuario.getDataDeNascimento().getTimeInMillis()).toString();
				
			}
			addParametro("nome", nome);
			addParametro("email", email);
			addParametro("login", login);
			addParametro("senha", senha);
			addParametro("imei", imei);
			addParametro("status", status);
			addParametro("data", data);
			
			String resposta = getMensagem();
			boolean sucesso = Boolean.parseBoolean(resposta);
			return sucesso;
		}catch (Erro e){
			throw e;
		} catch (Exception e) {
			throw new Erro(e);
		}
	}

	public void excluir(Usuario usuario) {
		// TODO Auto-generated method stub
		
	}
	
	public List<Usuario> listar() throws Erro {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		String acao = "listarUsuarios";
		addParametro("metodo", acao);
		Usuario usuario = null;
		try{
			JSONObject resposta = getJSONObject();
			JSONArray dados = resposta.getJSONArray("dado");
			if (existe(dados)) {
				for (int indice = 0; indice < dados.length(); indice++) {
					JSONObject item = dados.getJSONObject(indice);
					usuario = new Usuario();
					preencheVo(usuario, item);
					usuarios.add(usuario);
				}
			}
			
		}catch (Erro e){
			throw e;
		} catch (Exception e) {
			throw new Erro(e);
		}
		return usuarios;
	}
	
	public List<Usuario> listarAmigosComNomeComo(String nome) throws Erro {
		List<Usuario> amigos = new ArrayList<Usuario>();
		String acao = "listarAmigosComNomeComo";
		addParametro("metodo", acao);
		Usuario usuarioDaSessao = (Usuario) Sessao.getParametro(Constantes.USUARIO);
		String idUsuario = usuarioDaSessao.getId().toString();
		Usuario amigo = null;
		try{
			addParametro("nome", nome);
			addParametro("idUsuario", idUsuario);
			JSONObject resposta = getJSONObject();
			JSONArray dados = resposta.getJSONArray("dado");
			if (existe(dados)) {
				for (int indice = 0; indice < dados.length(); indice++) {
					JSONObject item = dados.getJSONObject(indice);
					amigo = new Usuario();
					preencheVo(amigo, item);
					amigos.add(amigo);
				}
			}
			
		}catch (Erro e){
			throw e;
		} catch (Exception e) {
			throw new Erro(e);
		}
		return amigos;
	}
	
	
	public List<Usuario> listarOutrosUsuariosComNomeComo(String nome) throws Erro {
		List<Usuario> amigos = new ArrayList<Usuario>();
		String acao = "listarOutrosUsuariosComNomeComo";
		addParametro("metodo", acao);
		Usuario outroUsuario = null;
		try{
			addParametro("nome", nome);
			Usuario usuario = (Usuario) Sessao.getParametro(Constantes.USUARIO);
			addParametro("idUsuario", usuario.getId().toString());
			JSONObject resposta = getJSONObject();
			JSONArray dados = resposta.getJSONArray("dado");
			if (existe(dados)) {
				for (int indice = 0; indice < dados.length(); indice++) {
					JSONObject item = dados.getJSONObject(indice);
					outroUsuario = new Usuario();
					preencheVo(outroUsuario, item);
					amigos.add(outroUsuario);
				}
			}
			
		}catch (Erro e){
			throw e;
		} catch (Exception e) {
			throw new Erro(e);
		}
		return amigos;
	}
	
	public boolean verificaSessao(String login, String senha) throws Erro {
		String acao = "verificarSessao";
		addParametro("metodo", acao);
		Boolean logado = false;
		try{
			addParametro("login", login);
//			addParametro("senha", senha);
			String textoResposta = getMensagem();
			JSONObject resposta = new JSONObject(textoResposta);
			if (resposta.has("dado")) {
				logado = Boolean.valueOf(resposta.getBoolean("dado"));
			}
		}catch (Erro e){
			throw e;
		} catch (Exception e) {
			throw new Erro(e);
		}
		
		return logado;
	}

	public void preencheVo(Usuario u, JSONObject o) throws Exception{
		if (existe(o) && existe(u)) {
			if (o.has("id") && existe(o.getLong("id"))){ u.comId(o.getLong("id")); }
			u.comLogin(o.getString("login"));
			u.comSenha(o.getString("senha"));
			u.comEmail(o.getString("email"));
//			u.estaBloqueado(o.get)
			u.comNome(o.getString("nome"));
			if (o.has("latitude") && existe(o.getDouble("latitude"))) { u.comLatitude(o.getDouble("latitude")); }
			if (o.has("longitude") && existe(o.getDouble("longitude"))) { u.comLongitude(o.getDouble("longitude")); }
			if (o.has("imei") && existe(o.getString("imei"))) { u.comImei(o.getString("imei")); }
			u.comDataDeNascimento(UtilsData.strToCalendar(getJSonStringSeExistir(o, "dataNascimento")));
		}
	}
	
	public ContextoPadrao getContexto() {
		return contexto;
	}

	public void setContexto(ContextoPadrao contexto) {
		this.contexto = contexto;
	}

	public static UsuarioWS getInstancia(ContextoPadrao contexto){
		instancia = new UsuarioWS();
		instancia.setContexto(contexto);
		instancia.metodo = MetodoHTTP.POST;
		instancia.entidade = "usuario";
		instancia.classeControladoraNoServidor = "com.waiso.cadetodomundo.controladores.ControladorUsuario";
		return instancia;
	}


}
