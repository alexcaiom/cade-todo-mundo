package com.waiso.cadetodomundo.ws.implementacoes;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.waiso.cadetodomundo.abstratas.ContextoPadrao;
import com.waiso.cadetodomundo.excecoes.Erro;
import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Sessao;
import com.waiso.cadetodomundo.utils.UtilsData;
import com.waiso.cadetodomundo.ws.WSAcoes;
import com.waiso.cadetodomundo.ws.implementacoes.utils.MetodoHTTP;

public class SolicitacaoWS extends WSAcoes {

	private static SolicitacaoWS instancia = null;
	private ContextoPadrao contexto;

	public List<SolicitacaoDeAmizade> listarSolicitacoesDeAmizade() throws Erro {
		List<SolicitacaoDeAmizade> solicitacoes = new ArrayList<SolicitacaoDeAmizade>();
		String acao = "listarSolicitacoesDeAmizade";
		addParametro("metodo", acao);
		Usuario usuarioDaSessao = (Usuario) Sessao.getParametro(Constantes.USUARIO);
		String idUsuario = usuarioDaSessao.getId().toString();
		SolicitacaoDeAmizade solicitacao = null;
		try{
			addParametro("idUsuario", idUsuario);
			JSONObject resposta = getJSONObject();
			JSONArray dados = resposta.getJSONArray("dado");
			if (existe(dados)) {
				for (int indice = 0; indice < dados.length(); indice++) {
					JSONObject item = dados.getJSONObject(indice);
					solicitacao = new SolicitacaoDeAmizade();
					preencheVo(solicitacao, item);
					solicitacoes.add(solicitacao);
				}
			}
			
		}catch (Erro e){
			throw e;
		} catch (Exception e) {
			throw new Erro(e);
		}
		return solicitacoes;
	}
	
	public boolean gerarSolicitacao(Usuario eu, Usuario amigo) throws Erro {
		String acao = "gerarSolicitacao";
		addParametro("metodo", acao);
		Usuario usuarioDaSessao = (Usuario) Sessao.getParametro(Constantes.USUARIO);
		String idUsuario = usuarioDaSessao.getId().toString();
		String idAmigo = amigo.getId().toString();
		Boolean sucesso = false;
		try{
			addParametro("idUsuario", idUsuario);
			addParametro("idAmigo", idAmigo);
			JSONObject resposta = getJSONObject();
			if (existe(resposta.get("dado"))) {
				sucesso = Boolean.valueOf(resposta.getString("dado"));
			}
			
		}catch (Erro e){
			throw e;
		} catch (Exception e) {
			throw new Erro(e);
		}
		return sucesso;
	}

	public boolean confirmarSolicitacao(Usuario eu, Usuario amigo) throws Erro {
		String acao = "confirmarSolicitacao";
		addParametro("metodo", acao);
		String idUsuario = eu.getId().toString();
		String idAmigo = amigo.getId().toString();
		Boolean sucesso = false;
		try{
			addParametro("idUsuario", idUsuario);
			addParametro("idAmigo", idAmigo);
			JSONObject resposta = getJSONObject();
			if (existe(resposta.get("dado"))) {
				sucesso = Boolean.valueOf(resposta.getString("dado"));
			}
			
		}catch (Erro e){
			throw e;
		} catch (Exception e) {
			throw new Erro(e);
		}
		return sucesso;
	}

	private void preencheVo(SolicitacaoDeAmizade s, JSONObject o) throws Exception{
		if (existe(o) && existe(s)) {
			JSONObject solicitante = o.getJSONObject("usuarioSolicitante");
			Usuario usuarioSolicitante = new Usuario();
			new UsuarioWS().preencheVo(usuarioSolicitante, solicitante);
			s.setUsuarioSolicitante(usuarioSolicitante);
			
			JSONObject amigoDoSolicitante = o.getJSONObject("amigo");
			Usuario amigo = new Usuario();
			new UsuarioWS().preencheVo(amigo, amigoDoSolicitante);
			s.setAmigo(amigo);
			Boolean aceita = o.has("aceita") ? o.getBoolean("aceita") : null;
			s.setAceita(aceita);
			Calendar data = UtilsData.strToCalendar(o.getString("data"));
			s.setData(data);
			Calendar dataResposta = null;
			if (existe(o.getString("dataResposta"))) {
				dataResposta = UtilsData.strToCalendar(o.getString("dataResposta"));
			}
			s.setDataResposta(dataResposta);
		}
	}

	public ContextoPadrao getContexto() {
		return contexto;
	}

	public void setContexto(ContextoPadrao contexto) {
		this.contexto = contexto;
	}
	
	public static SolicitacaoWS getInstancia(ContextoPadrao contexto){
		instancia  = new SolicitacaoWS();
		instancia.setContexto(contexto);
		instancia.metodo = MetodoHTTP.POST;
		instancia.entidade = "usuario";
		instancia.classeControladoraNoServidor = "com.waiso.cadetodomundo.controladores.ControladorSolicitacao";
		return instancia;
	}
}
