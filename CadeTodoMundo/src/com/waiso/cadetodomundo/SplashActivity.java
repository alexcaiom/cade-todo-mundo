package com.waiso.cadetodomundo;

import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;

import com.waiso.cadetodomundo.abstratas.ClasseActivity;
import com.waiso.cadetodomundo.gui.adaptadores.GalleryImageAdapter;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Sessao;

public class SplashActivity extends ClasseActivity {
	
//	Gallery galeria;
	Button btnLogin;
	TextView lblMsgSemConexao;
	Integer[] IDS_IMAGENS = {
			R.drawable.splash_img
	};
	ImageView imagem;
	private int indice = 0;
	private int tempoDeAtraso = 5;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		

		boolean pediuParaEncerrar = Sessao.temParametro(Constantes.SAIR);
		if (pediuParaEncerrar) {
			Sessao.removerParametro(Constantes.SAIR);
		}
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		executarAnimacao(R.anim.fade_in);
		new Handler().postDelayed(new Runnable() {
            
            // Using handler with postDelayed called runnable run method
  
            @Override
            public void run() {
            	avancar();
            }
        }, tempoDeAtraso*1000);
		
		carregarTela();
		
	}

	protected void avancar() {
		View tela = mapear(R.id.conteiner);
    	tela.animate().alpha(0).setDuration(2000).setStartDelay(3000).start();
        irPara(MainActivity.class);

        // close this activity
        finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}

	@Override
	public void carregarTela() {
		ocultarBarraDeAcoes();
//		imagemInstrucoes = (ImageView) findViewById(R.id.img_instrucoes);
		imagem = mapear(R.id.img_splash);
//		galeria 		= (Gallery) findViewById(R.id.splash_imagens);
		/*btnLogin 		= (Button) findViewById(R.id.btnIrParaTelaLogin);
		lblMsgSemConexao = (TextView) findViewById(R.id.msgSemConexao);*/
		carregarEventos();
		
	}

	@Override
	public void carregarEventos() {
		/*btnLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				irPara(LoginActivity.class);
			}
		});*/
		
//		galeria.setAdapter(new GalleryImageAdapter(this, IDS_IMAGENS));
		
		imagem.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				avancar();
			}
		});
		
		/*galeria.setOnDragListener(new OnDragListener() {
			@Override
			public boolean onDrag(View v, DragEvent event) {
				avisar("drag começou: view ("+v.getX()+") evento ("+event.getX()+")", Toast.LENGTH_SHORT);
				return false;
			}
		});*/
		
	}
	
	

}
