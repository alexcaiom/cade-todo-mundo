package com.waiso.cadetodomundo.orm.dao;

import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.persistencia.jdbc.utils.ConstantesPersistencia;

public class DAO extends com.waiso.persistencia.jdbc.DAO {

	public DAO() {
		ConstantesPersistencia.BD_CONEXAO_LOCAL = Constantes.BANCO_DADOS_CONEXAO_LOCAL;
		ConstantesPersistencia.BD_CONEXAO_PORTA = Constantes.BANCO_DADOS_CONEXAO_PORTA;
		ConstantesPersistencia.BD_CONEXAO_NOME_BD = Constantes.BANCO_DADOS_NOME;
		ConstantesPersistencia.BD_CONEXAO_USUARIO = Constantes.BANCO_DADOS_CONEXAO_USUARIO;
		ConstantesPersistencia.BD_CONEXAO_SENHA = Constantes.BANCO_DADOS_CONEXAO_SENHA;
	}
	
	protected String entidade = "";
	
}
