package com.waiso.cadetodomundo.orm.dao.finder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.persistencia.jdbc.utils.ConstantesPersistencia;

public abstract class Finder<T> extends com.waiso.persistencia.jdbc.finder.Finder<T> {

	public Finder() {
		ConstantesPersistencia.BD_CONEXAO_LOCAL = Constantes.BANCO_DADOS_CONEXAO_LOCAL;
		ConstantesPersistencia.BD_CONEXAO_PORTA = Constantes.BANCO_DADOS_CONEXAO_PORTA;
		ConstantesPersistencia.BD_CONEXAO_NOME_BD = Constantes.BANCO_DADOS_NOME;
		ConstantesPersistencia.BD_CONEXAO_USUARIO = Constantes.BANCO_DADOS_CONEXAO_USUARIO;
		ConstantesPersistencia.BD_CONEXAO_SENHA = Constantes.BANCO_DADOS_CONEXAO_SENHA;
	}
	
	protected Connection novaConexao() throws ClassNotFoundException, SQLException{
		Class.forName ("com.mysql.jdbc.Driver");
		String url = "jdbc:"+ConstantesPersistencia.TIPO_BD_MySQL+"://"+ConstantesPersistencia.BD_CONEXAO_LOCAL+":"+ConstantesPersistencia.BD_CONEXAO_PORTA+"/"+ConstantesPersistencia.BD_CONEXAO_NOME_BD;
		return DriverManager.getConnection(url, ConstantesPersistencia.BD_CONEXAO_USUARIO, ConstantesPersistencia.BD_CONEXAO_SENHA);
	}
	
	protected String entidade = "";
	
}
