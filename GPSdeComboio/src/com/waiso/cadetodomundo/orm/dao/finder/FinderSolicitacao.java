package com.waiso.cadetodomundo.orm.dao.finder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.framework.exceptions.ErroUsuario;
import com.waiso.framework.utils.UtilsData;
import com.waiso.persistencia.jdbc.GerenciadorConexao;

public class FinderSolicitacao extends Finder<SolicitacaoDeAmizade> {

	public FinderSolicitacao() {
		this.entidade = "tbl_amizades";
	}
	
	public List<SolicitacaoDeAmizade> listarTodasAsSolicitacoesDeAmizade(Usuario usuarioDaSessao) {
		
		List<SolicitacaoDeAmizade> lista = new ArrayList<SolicitacaoDeAmizade>();
		try {
			String sqlSolicitacoes = " select * "
									+ "\n from " + entidade
									+ "\n where id_usuario = ?"
									+ "\n or	id_amigo = ?";
			setComandoPreparado(novoComandoPreparado(sqlSolicitacoes, null));
			int indice = 0;
			comandoPreparado.setLong(++indice , usuarioDaSessao.getId());
			comandoPreparado.setLong(++indice , usuarioDaSessao.getId());
			ResultSet resultado = executarPesquisaParametrizada(comandoPreparado);
			
			while(resultado.next()){
				Long idUsuario 	= resultado.getLong("id_usuario");
				Long idAmigo 	= resultado.getLong("id_amigo");

				Usuario usuario = new Usuario();
				usuario.comId(idUsuario);
				usuario = new FinderUsuario().findById(usuario);

				Usuario amigo = new Usuario();
				amigo.comId(idAmigo);
				amigo = new FinderUsuario().findById(amigo);

				int indicadorAmizade = resultado.getInt("ehAmigo");
				Boolean jaEhAmigo = (indicadorAmizade == 1);
				
				SolicitacaoDeAmizade solicitacao = new SolicitacaoDeAmizade();
				solicitacao.setUsuarioSolicitante(usuario);
				solicitacao.setAmigo(amigo);
				solicitacao.setAceita(jaEhAmigo);
				
				lista.add(solicitacao);
			}

			resultado.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ErroUsuario e) {
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return lista;
	}
	
	public List<SolicitacaoDeAmizade> listarSolicitacoesDeAmizade(Usuario usuarioDaSessao) {
		List<SolicitacaoDeAmizade> lista = new ArrayList<SolicitacaoDeAmizade>();
		Connection conexao = null;
		ResultSet resultadoDaPesquisaDeSolicitacoesDeAmizades = null;
		PreparedStatement comandoPreparadoAqui = null;
		try {
			String subQueryNaoEhAmigo = " select * "
					+	"\n from " + entidade
					+   "\n where id_amigo = ? "
//					+   "\n or id_usuario = ? "
					+   "\n and ehAmigo is null";
			int indice = 0;
			
			conexao = novaConexao();
			comandoPreparadoAqui = conexao.prepareStatement(subQueryNaoEhAmigo);
			comandoPreparadoAqui.setLong(++indice, usuarioDaSessao.getId());
//			comandoPreparadoAqui.setLong(++indice, usuarioDaSessao.getId());
			
			resultadoDaPesquisaDeSolicitacoesDeAmizades = comandoPreparadoAqui.executeQuery();
			while(resultadoDaPesquisaDeSolicitacoesDeAmizades.next()){
				Long idUsuario = resultadoDaPesquisaDeSolicitacoesDeAmizades.getLong("id_usuario");
				Long idAmigo = resultadoDaPesquisaDeSolicitacoesDeAmizades.getLong("id_amigo");
				Usuario amigo = new Usuario();
				amigo.comId(idAmigo);
				amigo = new FinderUsuario().findById(amigo);

				Usuario usuarioQueSolicitouAmizade = new Usuario();
				usuarioQueSolicitouAmizade.comId(idUsuario);
				usuarioQueSolicitouAmizade = new FinderUsuario().findById(usuarioQueSolicitouAmizade);

				SolicitacaoDeAmizade solicitacao = new SolicitacaoDeAmizade(); 
				solicitacao.setUsuarioSolicitante(usuarioQueSolicitouAmizade);
				solicitacao.setAmigo(amigo);
				solicitacao.setData(UtilsData.dateToCalendar(resultadoDaPesquisaDeSolicitacoesDeAmizades.getDate("data")));
				if (existe(resultadoDaPesquisaDeSolicitacoesDeAmizades.getInt("ehAmigo"))) {
					solicitacao.setAceita(resultadoDaPesquisaDeSolicitacoesDeAmizades.getInt("ehAmigo") == 1);
				}
				lista.add(solicitacao);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ErroUsuario e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (existe(resultadoDaPesquisaDeSolicitacoesDeAmizades) && !resultadoDaPesquisaDeSolicitacoesDeAmizades.isClosed()) { resultadoDaPesquisaDeSolicitacoesDeAmizades.close(); }
				if (existe(comandoPreparadoAqui) && !comandoPreparadoAqui.isClosed()) { comandoPreparadoAqui.close(); }
				if (existe(conexao) && !conexao.isClosed()) { conexao.close(); }
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return lista;
	}

	@Override
	public List<SolicitacaoDeAmizade> listar() {
		List<SolicitacaoDeAmizade> lista = new ArrayList<SolicitacaoDeAmizade>();
		try {
			String textoSelecao = " select * "
					+	"\n from " + entidade
					+   "\n where ehAmigo is null";
			ResultSet resultado = pesquisarSemParametro(textoSelecao);
			while(resultado.next()){
				Long idUsuario = resultado.getLong("id_usuario");
				Long idAmigo = resultado.getLong("id_amigo");
				Usuario amigo = new Usuario();
				amigo.comId(idAmigo);
				amigo = new FinderUsuario().findById(amigo);

				Usuario usuarioQueSolicitouAmizade = new Usuario();
				usuarioQueSolicitouAmizade.comId(idUsuario);
				usuarioQueSolicitouAmizade = new FinderUsuario().findById(usuarioQueSolicitouAmizade);

				SolicitacaoDeAmizade solicitacao = new SolicitacaoDeAmizade(); 
				solicitacao.setUsuarioSolicitante(usuarioQueSolicitouAmizade);
				solicitacao.setAmigo(amigo);
				Calendar data = UtilsData.dateToCalendar(resultado.getDate("data"));
				solicitacao.setData(data);
				Calendar dataResposta = UtilsData.dateToCalendar(resultado.getDate("dataResposta"));
				solicitacao.setDataResposta(dataResposta);
				Boolean aceita = null;
				Integer aceitaNumerico = resultado.getInt("ehAmigo");
				if (existe(aceitaNumerico)) {
					aceita = aceitaNumerico == 1;
				}
				solicitacao.setAceita(aceita);
				lista.add(solicitacao);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ErroUsuario e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return lista;
	}
	
	public List<SolicitacaoDeAmizade> procuraSolicitacoesDeAmizadeEntre(Usuario eu, Usuario amigo) {
 		List<SolicitacaoDeAmizade> lista = new ArrayList<SolicitacaoDeAmizade>();
		try {
			String subQueryNaoEhAmigo = " select * "
					+	"\n from " + entidade
					+   "\n where id_amigo = ? "
					+   "\n and id_usuario = ? "
					+   "\n and ehAmigo is null";
			setComandoPreparado(novoComandoPreparado(subQueryNaoEhAmigo, null));
			int indice = 0;
			comandoPreparado.setLong(++indice, eu.getId());
			comandoPreparado.setLong(++indice, amigo.getId());
			
			ResultSet resultadosPesquisaSolicitacoes = executarPesquisaParametrizada(comandoPreparado); 
			while(resultadosPesquisaSolicitacoes.next()){
				Long idUsuario = resultadosPesquisaSolicitacoes.getLong("id_usuario");
				Long idAmigo = resultadosPesquisaSolicitacoes.getLong("id_amigo");
				amigo = new Usuario();
				amigo.comId(idAmigo);
				amigo = new FinderUsuario().findById(amigo);

				Usuario usuarioQueSolicitouAmizade = new Usuario();
				usuarioQueSolicitouAmizade.comId(idUsuario);
				usuarioQueSolicitouAmizade = new FinderUsuario().findById(usuarioQueSolicitouAmizade);

				SolicitacaoDeAmizade solicitacao = new SolicitacaoDeAmizade();
				solicitacao.setUsuarioSolicitante(usuarioQueSolicitouAmizade);
				solicitacao.setAmigo(amigo);
				solicitacao.setData(UtilsData.dateToCalendar(resultadosPesquisaSolicitacoes.getDate("data")));
				if (existe(resultadosPesquisaSolicitacoes.getInt("ehAmigo"))) {
					solicitacao.setAceita(resultadosPesquisaSolicitacoes.getInt("ehAmigo") == 1);
				}
				lista.add(solicitacao);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ErroUsuario e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return lista;
	}
	
	@Override
	public void preencheVO(SolicitacaoDeAmizade o) throws SQLException {
		// TODO Auto-generated method stub
		
	}

}
