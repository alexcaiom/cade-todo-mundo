/**
 * 
 */
package com.waiso.cadetodomundo.orm.dao.finder;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.waiso.cadetodomundo.enums.EnumUsuarioAutenticado;
import com.waiso.cadetodomundo.orm.modelos.Role;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.framework.exceptions.ErroUsuario;
import com.waiso.persistencia.jdbc.GeradorSQLBean;

/**
 * @author Alex
 *
 */
public class FinderUsuario extends Finder<Usuario>{

	private static FinderUsuario instancia;

	@Override
	public List<Usuario> listar() {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		String comandoSelecao = "select * from tbl_usuario";
		try {
			resultados = pesquisarSemParametro(comandoSelecao);
			while (resultados.next()) {
				Usuario u = new Usuario();
				preencheVO(u);
				usuarios.add(u);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usuarios;
	}

	public Usuario findByLogin(String login) {
		Usuario usuario = null;
		String pesquisa = "select * from tbl_usuario";
		String filtro = "";
		
		if (existe(login) && !login.isEmpty()) {
			try{
				filtro = " \n where login = ?";
				comandoPreparado = novoComandoPreparado(pesquisa + filtro, null);
				comandoPreparado.setString(1, login);
				
				setResultados(executarPesquisaParametrizada(comandoPreparado));
				
				while (resultados.next()) {
					usuario = new Usuario();
					preencheVO(usuario);
				}
			}catch(SQLException e){
				e.printStackTrace();
			} finally {
				try {
					fechar();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return usuario;
	}

	public Usuario findById(Usuario u) {
		Usuario usuario = null;
		String pesquisa = "select * from tbl_usuario";
		String filtro = "";
		
		if (existe(u) && existe(u.getId())) {
			try{
				filtro = " \n where id = ?";
				comandoPreparado = novoComandoPreparado(pesquisa + filtro, null);
				comandoPreparado.setLong(1, u.getId());
				
				setResultados(executarPesquisaParametrizada(comandoPreparado));
				
				while (resultados.next()) {
					usuario = new Usuario();
					preencheVO(usuario);
				}
			}catch(SQLException e){
				e.printStackTrace();
			} finally {
				try {
					fechar();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return usuario;
	}

	public List<Usuario> findByLoginComo(String login) {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		String pesquisa = "select * from tbl_usuario";
		String filtro = "";
		
		if (existe(login) && !login.isEmpty()) {
			try{
				filtro = " \n where login like ?";
				comandoPreparado = novoComandoPreparado(pesquisa + filtro, null);
				comandoPreparado.setString(1, "%"+login+"%");
				
				setResultados(executarPesquisaParametrizada(comandoPreparado));
				
				while (resultados.next()) {
					Usuario u = new Usuario();
					preencheVO(u);
					usuarios.add(u);
				}
			}catch(SQLException e){

			} finally {
				try {
					fechar();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return usuarios;
	}
	

	public List<Usuario> listarAmigosComNomeComo(String nome, Usuario usuarioDaSessao) {
		List<Usuario> lista = new ArrayList<Usuario>();
		try {
			boolean usarUsuario = existe(nome) && !nome.isEmpty();
			String whereUsuario = "";
			if (usarUsuario) {
				whereUsuario = "\n and nome like '%"+nome+"%'";
			}
			String subQueryEhAmigo = "and id in (select id_amigo "
								+	"\n from tbl_amizades "
								+   "\n where id_usuario = ?"
								+   "\n "
								+   "\n and ehAmigo = 1)"
								+   "\n or id in (select id_usuario "
								+	"\n from tbl_amizades "
								+   "\n where id_amigo = ?"
								+   "\n "
								+   "\n and ehAmigo = 1) ";
			String sql = "select * "
					+ "\n from "+entidade//GeradorSQLBean.getInstancia(Usuario.class).getNomeTabela()
					+ "\n where id != ?"
					+ "\n " + subQueryEhAmigo + ""
					+ whereUsuario;
			
			setComandoPreparado(novoComandoPreparado(sql, null));
			int indice = 0;
			comandoPreparado.setLong(++indice, usuarioDaSessao.getId());
			comandoPreparado.setLong(++indice, usuarioDaSessao.getId());
//			comandoPreparado.setLong(++indice, usuarioDaSessao.getId());
//			comandoPreparado.setLong(++indice, usuarioDaSessao.getId());
			comandoPreparado.setLong(++indice, usuarioDaSessao.getId());
			if (usarUsuario) {
				comandoPreparado.setString(++indice, nome);
			}
			setResultados(executarPesquisaParametrizada(comandoPreparado));
			
			Usuario u = null;
			while(resultados.next()){
				u = new Usuario();
				preencheVO(u);
				lista.add(u);
			}			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ErroUsuario e) {
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return lista;
	}

	public List<Usuario> listarOutrosUsuariosComNomeComo(String nome, Usuario usuarioDaSessao) {
		List<Usuario> lista = new ArrayList<Usuario>();
		try {
			boolean usarUsuario = existe(nome) && !nome.isEmpty();
			String whereUsuario = "";
			if (usarUsuario) {
				whereUsuario = "\n and nome like '%"+nome+"%'";
			}
			String subQueryNaoEhAmigo = "\n and id not in ("
										+"\n        select id_usuario"
										+"\n          from tbl_amizades"
										+"\n          where (ehAmigo = 1 or ehAmigo is null)"
										+"\n			and id_usuario = ? "
										+ "\n			or id_amigo = ? "
										+"\n           )"
										+"\n and id not in ("
										+"\n        select id_amigo"
										+"\n          from tbl_amizades"
										+"\n          where (ehAmigo = 1 or ehAmigo is null)"
										+"\n			and id_usuario = ? "
										+ "\n			or id_amigo = ? "
										+"\n           )"
										+"\n ";
			String sql = "select * "
						+ "\n from "+entidade//GeradorSQLBean.getInstancia(Usuario.class).getNomeTabela()
						+ "\n where id != ?"
						+ "\n " + subQueryNaoEhAmigo
						+ whereUsuario;
			
			setComandoPreparado(novoComandoPreparado(sql, null));
			int indice = 0;
			comandoPreparado.setLong(++indice, usuarioDaSessao.getId());
			comandoPreparado.setLong(++indice, usuarioDaSessao.getId());
			comandoPreparado.setLong(++indice, usuarioDaSessao.getId());
			comandoPreparado.setLong(++indice, usuarioDaSessao.getId());
			comandoPreparado.setLong(++indice, usuarioDaSessao.getId());
			if (usarUsuario) {
				comandoPreparado.setString(++indice, nome);
			}
			setResultados(executarPesquisaParametrizada(comandoPreparado));
			
			Usuario u = null;
			while(resultados.next()){
				u = new Usuario();
				preencheVO(u);
				lista.add(u);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ErroUsuario e) {
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return lista;
	}
	
	@Override
	public void preencheVO(Usuario o) throws SQLException {
		if (existe(resultados) && existe(o)) {
			o.comId(resultados.getLong("id"));
			o.comLogin(resultados.getString("login"));
			o.comSenha(resultados.getString("senha"));
			o.comNome(resultados.getString("nome"));
			Date data = resultados.getDate("dataDeNascimento", GregorianCalendar.getInstance());
			Calendar dataDeNascimento = GregorianCalendar.getInstance();
			if (existe(data)) {
				dataDeNascimento.setTime(data);
				o.comDataDeNascimento(dataDeNascimento);
			}
			o.comEmail(resultados.getString("email"));
			o.comLongitude(resultados.getDouble("longitude"));
			o.comLatitude(resultados.getDouble("latitude"));
			o.comImei(resultados.getString("imei"));
			o.comContadorSenhaInvalida(resultados.getInt("contadorSenhaInvalida"));
			EnumUsuarioAutenticado status = EnumUsuarioAutenticado.get(resultados.getInt("status"));
			o.comStatus(status);
			Role perfil = Role.get(resultados.getInt("perfil"));
			o.comPerfil(perfil);
		}
	}

	public static FinderUsuario getInstancia()  {
		if(instancia == null){
			instancia = new FinderUsuario();
			instancia.entidade = "tbl_usuario";
		}
		return instancia;
	}

}
