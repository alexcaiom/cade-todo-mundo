/**
 * 
 */
package com.waiso.cadetodomundo.orm.dao;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.framework.exceptions.ErroUsuario;

/**
 * @author Alex
 *
 */
public class DAOUsuario extends DAO {

	public Usuario inserir(Usuario usuario) {
		String comandoInsercao = "insert into tbl_usuario (nome, login, senha, email, imei) \nvalues (?, ?, ?, ?, ?)";
		if (existe(usuario)) {
			try{
				setComandoPreparado(novoComandoPreparado(comandoInsercao, Statement.RETURN_GENERATED_KEYS));
				int indice = 0;
				getComandoPreparado().setString(++indice, usuario.getNome());
				getComandoPreparado().setString(++indice, usuario.getLogin());
				getComandoPreparado().setString(++indice, usuario.getSenha());
				getComandoPreparado().setString(++indice, usuario.getEmail());
//				getComandoPreparado().setInt(++indice, usuario.getStatus().getCodigo());
				getComandoPreparado().setString(++indice, usuario.getImei());
				
				setResultados(executarOperacaoERetornarChavesGeradas(comandoPreparado));
				
				if (resultados.next()) {
					Integer id = resultados.getInt(1);
					usuario.comId(id.longValue());
				}
			} catch(SQLException e){
				if (e instanceof MySQLIntegrityConstraintViolationException) {
					throw new ErroUsuario(e.getMessage());
				}
				e.printStackTrace();
			} finally {
				 try {
					fechar();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return usuario;
	}

	public void atualizar(Usuario usuario) {
		String parametroDataNascimento = "";
		String parametroStatus = "";
		String parametroPerfil = "";
		
		if (existe(usuario.getDataDeNascimento())) {
			parametroDataNascimento = "\n dataDeNascimento = ?, ";
		}
		
		if (existe(usuario.getStatus())) {
			parametroStatus = "\n status = ?, ";
		}
		
		if (existe(usuario.getPerfil())) {
			parametroPerfil = "\n perfil = ?, ";
		}
		
		String comandoAtualizacao = "update tbl_usuario "
								+ "\n set login = ?,"
								+ "\n senha = ?, "
								+ "\n nome = ?, "
								+ parametroDataNascimento
								+ "\n email = ?, "
								+ "\n longitude = ?, "
								+ "\n latitude = ?, "
								+ "\n imei = ?, "
								+ parametroStatus 
								+ parametroPerfil
								+ "\n contadorSenhaInvalida = ? ";
		String filtro = "\n where id = ?";
		if (existe(usuario)) {
			try{
				setComandoPreparado(novoComandoPreparado(comandoAtualizacao + filtro, null));
				int indice = 0;
				getComandoPreparado().setString(++indice, usuario.getLogin());
				getComandoPreparado().setString(++indice, usuario.getSenha());
				getComandoPreparado().setString(++indice, usuario.getNome());
				if (existe(usuario.getDataDeNascimento())) {
					getComandoPreparado().setDate(++indice, new Date(usuario.getDataDeNascimento().getTimeInMillis()), usuario.getDataDeNascimento());
				}
				
				getComandoPreparado().setString(++indice, usuario.getEmail());
				getComandoPreparado().setDouble(++indice, usuario.getLongitude());
				getComandoPreparado().setDouble(++indice, usuario.getLatitude());
				getComandoPreparado().setString(++indice, usuario.getImei());
				if (existe(usuario.getStatus())) {
					getComandoPreparado().setInt(++indice, usuario.getStatus().getCodigo());
				}
				if (existe(usuario.getPerfil())) {
					getComandoPreparado().setInt(++indice, usuario.getPerfil().getId());
				}
				getComandoPreparado().setInt(++indice, usuario.getContadorSenhaInvalida());
				getComandoPreparado().setLong(++indice, usuario.getId());
				
				
				comandoPreparado.execute();
			} catch(SQLException e){
				e.printStackTrace();
			} finally {
				 try {
					fechar();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void excluir(Usuario usuario) {
		String comandoExclusao = "delete from tbl_usuario ";
		String filtro = "\n where id = ? ";
		if (existe(usuario)) {
			try{
				setComandoPreparado(novoComandoPreparado(comandoExclusao + filtro, Statement.RETURN_GENERATED_KEYS));
				int indice = 0;
				comandoPreparado.setLong(++indice, usuario.getId());
				comandoPreparado.execute();
			} catch(SQLException e){
				e.printStackTrace();
			} finally {
				 try {
					fechar();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
