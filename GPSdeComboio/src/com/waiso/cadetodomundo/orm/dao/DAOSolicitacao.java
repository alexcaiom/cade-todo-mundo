package com.waiso.cadetodomundo.orm.dao;

import java.sql.SQLException;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.framework.exceptions.ErroUsuario;

public class DAOSolicitacao extends DAO {

	
	public DAOSolicitacao() {
		this.entidade = "tbl_amizades";
	}

	public boolean associarUsuarios(Usuario eu, Usuario amigo) {
		String comandoAssociacao = "insert into " + entidade + " (id_usuario, id_amigo) values (?, ?)";
		if (existe(eu) && existe(amigo)) {
			try{
				setComandoPreparado(novoComandoPreparado(comandoAssociacao, null));
				int indice = 0;
				comandoPreparado.setLong(++indice, eu.getId());
				comandoPreparado.setLong(++indice, amigo.getId());
				
				return comandoPreparado.executeUpdate() == 1;
			} catch (MySQLIntegrityConstraintViolationException e){
				throw new ErroUsuario("Ja existe uma solicitacao de Amizade para este usuario.");
			} catch (SQLException e){
				e.printStackTrace();
			} finally {
				try {
					fechar();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}
	
	public boolean concluir(Usuario eu, Usuario amigo) {
		String comandoAssociacao = "update " + entidade
								+ "\n set ehAmigo = 1,"
								+ "\n dataResposta = CURRENT_TIMESTAMP "
								+ "\n where id_usuario = ?  "
								+ "\n and id_amigo = ? ";
		try {
			setComandoPreparado(novoComandoPreparado(comandoAssociacao, null));
			int indice = 0;
			comandoPreparado.setLong(++indice, eu.getId());
			comandoPreparado.setLong(++indice, amigo.getId());
			
			return comandoPreparado.executeUpdate() == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return false;
	}
	
}
