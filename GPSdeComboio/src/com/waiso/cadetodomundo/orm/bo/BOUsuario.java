/**
 * 
 */
package com.waiso.cadetodomundo.orm.bo;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.waiso.cadetodomundo.enums.EnumUsuarioAutenticado;
import com.waiso.cadetodomundo.enums.EnumUsuarioCadastrado;
import com.waiso.cadetodomundo.orm.dao.DAOSolicitacao;
import com.waiso.cadetodomundo.orm.dao.DAOUsuario;
import com.waiso.cadetodomundo.orm.dao.finder.FinderSolicitacao;
import com.waiso.cadetodomundo.orm.dao.finder.FinderUsuario;
import com.waiso.cadetodomundo.orm.interfaces.IUsuarioBO;
import com.waiso.cadetodomundo.orm.modelos.Role;
import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.framework.abstratas.Classe;
import com.waiso.framework.exceptions.ErroNegocio;
import com.waiso.framework.exceptions.ErroUsuario;
import com.waiso.framework.utils.UtilsData;

/**
 * @author Alex
 *
 */
public class BOUsuario extends Classe
						implements IUsuarioBO, Serializable {

	private DAOUsuario dao;
	private FinderUsuario finder;
	
	/**
	 * Metodo de Autenticacao de Usuario
	 * @param login
	 * @param senha
	 * @return
	 */
	public Usuario autentica(String login, String senha) throws ErroUsuario {
		log("Autenticando "+getNomeEntidade());
		Usuario u = new Usuario();
		u.comLogin(login);
		Usuario usuarioRecuperadoDoBD = new  Usuario();
		usuarioRecuperadoDoBD = getFinder().findByLogin(u.getLogin());
		
		//Primeiro verificamos se o usuario existe.
		if(existe(usuarioRecuperadoDoBD)){
			//Verificamos se usuario e senha informado batem com o banco.
			boolean usuarioConfere = (login.equals(usuarioRecuperadoDoBD.getLogin()) && senha.equals(usuarioRecuperadoDoBD.getSenha()));
			boolean usuarioEstaBloqueado = !(usuarioRecuperadoDoBD.getStatus() == null || !usuarioRecuperadoDoBD.getStatus().equals(EnumUsuarioAutenticado.USUARIO_BLOQUEADO));
			boolean ehFinalDeSemana = UtilsData.ehFinalDeSemana();
			boolean ehHorarioComercial = UtilsData.ehHorarioComercial();
			boolean ehAdm = ehAdm(usuarioRecuperadoDoBD);
			
			boolean usuarioPodeAcessar = usuarioConfere 
								&& 
								((!usuarioEstaBloqueado 
								/*&& !ehFinalDeSemana 
								&& ehHorarioComercial*/)
								|| ehAdm); 
			
			if (usuarioPodeAcessar){
				log(getNomeEntidade()+ " autenticado com sucesso");
			} else if (!usuarioConfere) {
				log(getNomeEntidade()+ " informado com senha inválida...");
				usuarioRecuperadoDoBD.comContadorSenhaInvalida(usuarioRecuperadoDoBD.getContadorSenhaInvalida()+1);
				boolean avisaUsuarioBloqueioNoProximoErro = (3 - usuarioRecuperadoDoBD.getContadorSenhaInvalida()) == 1;
				trataSenhaInvalidaDoUsuario(usuarioRecuperadoDoBD);
				
				if (avisaUsuarioBloqueioNoProximoErro) {
					throw new ErroNegocio(EnumUsuarioAutenticado.SENHA_INVALIDA_ULTIMA_TENTATIVA.getMensagem());
				} else {
					throw new ErroNegocio(EnumUsuarioAutenticado.SENHA_INVALIDA.getMensagem());
				}
			} else if(usuarioEstaBloqueado) {
				throw new ErroNegocio(EnumUsuarioAutenticado.USUARIO_BLOQUEADO.getMensagem() );
			} else if (ehFinalDeSemana) {
				throw new ErroNegocio(EnumUsuarioAutenticado.FINAL_DE_SEMANA.getMensagem() );
			} else if (!ehHorarioComercial) {
				throw new ErroNegocio(EnumUsuarioAutenticado.FORA_DO_HORARIO_COMERCIAL.getMensagem() );
			}
		} else {
			log(EnumUsuarioAutenticado.USUARIO_INEXISTENTE.getMensagem());
			throw new ErroNegocio(EnumUsuarioAutenticado.USUARIO_INEXISTENTE.getMensagem());
		}
		return usuarioRecuperadoDoBD;
	}

	public List<Usuario> listarUsuarios(){
		log("Listando "+getNomeEntidade()+"s");
		List<Usuario> usuarios = new ArrayList<Usuario>();
		
		try {
			usuarios = getFinder().listar();

			for (Usuario usuario : usuarios) {
				System.out.println(usuario);
			}
			getFinder().fechar();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return usuarios;
	}
	
	public List<String> listarUsuariosString(){
		List<String> usuarios = new ArrayList<String>();
		for(Usuario u: listarUsuarios()){
			usuarios.add(u.toString());
		}
		return usuarios;
	}

	@Override
	public Usuario inserir(Usuario usuario) throws ErroUsuario, ErroNegocio {
		log("Inserindo "+getNomeEntidade());
		boolean usuarioJaExiste = getFinder().findByLogin(usuario.getLogin()) != null;
		if(usuarioJaExiste){
			throw new ErroNegocio(EnumUsuarioCadastrado.USUARIO_DUPLICADO.getMensagem());
		} else {
			Usuario uCadastrado = getDAO().inserir(usuario);
			boolean ocorreuAlgumErro = uCadastrado == null;
			if (ocorreuAlgumErro){
				throw new ErroNegocio(EnumUsuarioCadastrado.ERRO_AO_CRIAR_USUARIO.getMensagem());
			} else {
				return uCadastrado;
			}
		}
		
	}

	@Override
	public void alterar(Usuario usuario) throws ErroUsuario {
		log("Alterando "+getNomeEntidade());
		getDAO().atualizar(usuario);		
	}
	
	public Object liberarUsuario(String login) {
		Usuario u = pesquisarPorLogin(login);
		u.comContadorSenhaInvalida(0);
		u.comStatus(EnumUsuarioAutenticado.SUCESSO);
		alterar(u);
		return "Liberado";
	}

	@Override
	public void excluir(Usuario usuario) throws ErroUsuario {
		log("Excluindo "+getNomeEntidade());
		getDAO().excluir(usuario);		
	}
	
	public Usuario pesquisar(Long id){
		Usuario usuario = null;
		try{
			usuario = new Usuario();
			usuario.comId(id);
			usuario = getFinder().findById(usuario);
			System.out.println(usuario);
		} finally {
			try {
				getFinder().fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return usuario;
	}
	
	public Usuario pesquisarPorLogin(String login){
		return getFinder().findByLogin(login);
	}
	
	public List<Usuario> pesquisarPorLoginComo(String login) {
		List<Usuario> usuarios = getFinder().findByLoginComo(login);
		
		for (Usuario usuario : usuarios) {
			System.out.println(usuario);
		}
		
		return usuarios;
	}
	
	private void trataSenhaInvalidaDoUsuario(Usuario usuarioRecuperadoDoBD) {
		if (existe(usuarioRecuperadoDoBD.getContadorSenhaInvalida())) {
			int numeroDeChancesRestantes = 3 - usuarioRecuperadoDoBD.getContadorSenhaInvalida();
			boolean deveBloquearUsuario = numeroDeChancesRestantes == 0;
			if (deveBloquearUsuario) {
				usuarioRecuperadoDoBD.comStatus(EnumUsuarioAutenticado.USUARIO_BLOQUEADO);				
			}
		} else {
			
		}
		getDAO().atualizar(usuarioRecuperadoDoBD);
	}
	
	public List<SolicitacaoDeAmizade> temSolicitacaoGerada(Usuario eu, Usuario amigo) throws ErroUsuario {
		return new FinderSolicitacao().procuraSolicitacoesDeAmizadeEntre(eu, amigo);
	}
	
	public boolean gerarSolicitacao(Usuario eu, Usuario amigo) throws ErroUsuario {
		return new DAOSolicitacao().associarUsuarios(eu, amigo);
	}
	
	public boolean confirmarSolicitacao(Usuario eu, Usuario amigo) throws ErroUsuario {
		return new DAOSolicitacao().concluir(eu, amigo);
	}
	
	public List<Usuario> listar() throws Exception {
		return getFinder().listar();
	}
	
	public List<Usuario> listarAmigosComNomeComo(String nome, Usuario usuarioDaSessao) throws Exception {
		return getFinder().listarAmigosComNomeComo(nome, usuarioDaSessao);
	}
	
	public List<Usuario> listarOutrosUsuariosComNomeComo(String nome, Usuario usuarioDaSessao) throws Exception {
		return getFinder().listarOutrosUsuariosComNomeComo(nome, usuarioDaSessao);
	}
	
	public List<SolicitacaoDeAmizade> listarTodasSolicitacoesDeAmizade() {
		return new FinderSolicitacao().listar();
	}
	
	public List<SolicitacaoDeAmizade> listarSolicitacoesDeAmizade(Usuario usuarioDaSessao) {
		return new FinderSolicitacao().listarSolicitacoesDeAmizade(usuarioDaSessao);
	}
	
	private boolean ehAdm(Usuario usuarioRecuperadoDoBD) {
		if (existe(usuarioRecuperadoDoBD.getPerfil()) && usuarioRecuperadoDoBD.getPerfil().equals(Role.ADMINISTRADOR)) {
			return true;
		}
		return false;
	}

	private String getNomeEntidade(){
		return CLASSE_NOME.substring(2);
	}
	
	private DAOUsuario getDAO(){
		if (naoExiste(this.dao)) {
			this.dao = new DAOUsuario();
		}
		return dao;
	}
	
	private FinderUsuario getFinder(){
		if (naoExiste(this.finder)) {
			this.finder = FinderUsuario.getInstancia();
		}
		return finder;
	}
}
