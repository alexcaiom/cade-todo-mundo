/**
 * 
 */
package com.waiso.cadetodomundo.orm.interfaces;

import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.framework.exceptions.ErroCriticoUsuario;
import com.waiso.framework.exceptions.ErroNegocio;


/**
 * @author Alex
 *
 */
public interface IUsuarioBO {

	public Usuario inserir(Usuario usuario) throws ErroCriticoUsuario, ErroNegocio;
	public void alterar(Usuario usuario) throws ErroCriticoUsuario, ErroNegocio;
	public void excluir(Usuario usuario) throws ErroCriticoUsuario, ErroNegocio;
	
}
