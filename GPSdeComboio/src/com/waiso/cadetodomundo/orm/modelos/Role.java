package com.waiso.cadetodomundo.orm.modelos;

public enum Role {
	ADMINISTRADOR(1), 
	CLIENTE(2);
	
	int id;
	
	private Role(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
	
	public static Role get(int id){
		for (Role role : values()) {
			if (role.id == id) {
				return role;
			}
		}
		return null;
	}

}
