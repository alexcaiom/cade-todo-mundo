package com.waiso.cadetodomundo.orm.modelos;

import java.util.Calendar;

public class SolicitacaoDeAmizade {

	private Usuario usuarioSolicitante;
	private Usuario amigo;
	private Boolean aceita;
	private Calendar data;
	private Calendar dataResposta;
	
	public SolicitacaoDeAmizade() {}
	
	public SolicitacaoDeAmizade(Usuario usuarioSolicitante, Usuario amigo, boolean aceita, Calendar data) {
		this.usuarioSolicitante = usuarioSolicitante;
		this.amigo = amigo;
		this.aceita = aceita;
		this.data = data;
	}
	
	public Usuario getUsuarioSolicitante() {
		return usuarioSolicitante;
	}
	public void setUsuarioSolicitante(Usuario usuarioSolicitante) {
		this.usuarioSolicitante = usuarioSolicitante;
	}
	public Usuario getAmigo() {
		return amigo;
	}
	public void setAmigo(Usuario amigo) {
		this.amigo = amigo;
	}
	public Boolean isAceita() {
		return aceita;
	}
	public void setAceita(Boolean aceita) {
		this.aceita = aceita;
	}
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}

	public Calendar getDataResposta() {
		return dataResposta;
	}

	public void setDataResposta(Calendar dataResposta) {
		this.dataResposta = dataResposta;
	}

	@Override
	public String toString() {
		return "SolicitacaoDeAmizade [usuarioSolicitante=" + usuarioSolicitante + ", amigo=" + amigo + ", aceita="
				+ aceita + ", data=" + data + ", dataResposta=" + dataResposta + "]";
	}


}
