/**
 * 
 */
package com.waiso.cadetodomundo.orm.modelos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.waiso.cadetodomundo.enums.EnumUsuarioAutenticado;
import com.waiso.persistencia.anotacoes.ChavePrimaria;
import com.waiso.persistencia.anotacoes.Tabela;

/**
 * @author Alex
 *
 */
@Tabela(nome="tb_usuario")
public class Usuario implements Serializable{
	
	@ChavePrimaria(autoIncremental=true)
	private Long id;
	private String login;
	private String senha;
	private String nome;
	private Calendar dataDeNascimento;
	private String email;
	private List<Usuario> amigos = new ArrayList<Usuario>();
	private Double longitude;
	private Double latitude;
	private String imei;
	private int contadorSenhaInvalida;
	private EnumUsuarioAutenticado status;
	private String mensagemDeStatus;
	private Role perfil;
	
	public Usuario() {}
	public Usuario(String login, String senha, EnumUsuarioAutenticado status) {
		super();
		this.login = login;
		this.senha = senha;
		this.status = status;
	}
	
	public Usuario(String login, String senha, EnumUsuarioAutenticado status, String nome, GregorianCalendar dataDeNascimento, String email){
		this.login = login;
		this.senha = senha;
		this.status= status;
		this.nome = nome;
		this.dataDeNascimento = dataDeNascimento;
		this.email = email;
	}


	public String getLogin() {
		return login;
	}
	public Usuario comLogin(String login) {
		this.login = login;
		return this;
	}
	public String getSenha() {
		return senha;
	}
	public Usuario comSenha(String senha) {
		this.senha = senha;
		return this;
	}
	public List<Usuario> getAmigos() {
		return amigos;
	}
	public Usuario comAmigos(List<Usuario> amigos) {
		this.amigos = amigos;
		return this;
	}
	
	public Usuario addAmigo(Usuario amigo) {
		this.amigos.add(amigo);
		return this;
	}
	public Long getId() {
		return id;
	}
	public Usuario comId(Long id) {
		this.id = id;
		return this;
	}
	public String getNome() {
		return nome;
	}
	public Usuario comNome(String nome) {
		this.nome = nome;
		return this;
	}
	public Calendar getDataDeNascimento() {
		return dataDeNascimento;
	}
	public Usuario comDataDeNascimento(Calendar dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
		return this;
	}
	public String getEmail() {
		return email;
	}
	public Usuario comEmail(String email) {
		this.email = email;
		return this;
	}
	public double getLongitude() {
		return longitude;
	}
	public Usuario comLongitude(Double longitude) {
		this.longitude = longitude;
		return this;
	}
	public double getLatitude() {
		return latitude;
	}
	public Usuario comLatitude(Double latitude) {
		this.latitude = latitude;
		return this;
	}
	public String getImei() {
		return imei;
	}
	public Usuario comImei(String imei) {
		this.imei = imei;
		return this;
	}
	public Integer getContadorSenhaInvalida() {
		return contadorSenhaInvalida;
	}
	public Usuario comContadorSenhaInvalida(int contadorSenhaInvalida) {
		this.contadorSenhaInvalida = contadorSenhaInvalida;
		return this;
	}
	public EnumUsuarioAutenticado getStatus() {
		return status;
	}
	public Usuario comStatus(EnumUsuarioAutenticado status) {
		this.status = status;
		return this;
	}
	public String getMensagemDeStatus() {
		return mensagemDeStatus;
	}
	public Usuario comMensagemDeStatus(String mensagemDeStatus) {
		this.mensagemDeStatus = mensagemDeStatus;
		return this;
	}
	public Role getPerfil() {
		return perfil;
	}
	public Usuario comPerfil(Role perfil) {
		this.perfil = perfil;
		return this;
	}
	@Override
	public String toString() {
		return "Usuario [id=" + id + ", login=" + login + ", senha=" + senha + ", nome=" + nome + ", dataDeNascimento="
				+ dataDeNascimento + ", email=" + email + ", amigos=" + amigos + ", longitude=" + longitude
				+ ", latitude=" + latitude + ", imei=" + imei + ", contadorSenhaInvalida=" + contadorSenhaInvalida
				+ ", status=" + status + ", perfil=" + perfil + "]";
	}
	
}
