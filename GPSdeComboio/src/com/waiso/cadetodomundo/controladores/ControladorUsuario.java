package com.waiso.cadetodomundo.controladores;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.waiso.cadetodomundo.enums.EnumUsuarioAutenticado;
import com.waiso.cadetodomundo.orm.bo.BOUsuario;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.utils.Constantes;
import com.waiso.cadetodomundo.utils.Sessao;
import com.waiso.framework.exceptions.ErroNegocio;
import com.waiso.framework.exceptions.ErroUsuario;
import com.waiso.framework.json.Consequence;
import com.waiso.framework.json.JSONReturn;

public class ControladorUsuario extends Controlador{

	/**
	 * Metodo de Autenticacao de Usuario
	 * @param login
	 * @param senha
	 * @return
	 */
	public JSONReturn autentica(ServletRequest requisicao, ServletResponse resposta) throws ErroUsuario {
		String login = "";
		String senha = "";
		
		login = getStringDaRequisicao(requisicao, "login");
		senha = getStringDaRequisicao(requisicao, "senha");
		JSONReturn retorno = null;
		Usuario u = null;
		try{
			u = new BOUsuario().autentica(login, senha);
			retorno = JSONReturn.newInstance(Consequence.SUCCESSO, u);
			
			

			if (!Sessao.temParametro(Constantes.SESSOES_ABERTAS)) {
				Map<String, Usuario> usuarios = new HashMap<String, Usuario>();
				usuarios.put(u.getLogin(), u);
				Sessao.addParametro(Constantes.SESSOES_ABERTAS, usuarios);
			} else {
				Map<String, Usuario> usuarios  = (Map<String, Usuario>) Sessao.getInstancia().get(Constantes.SESSOES_ABERTAS);
				if (!usuarios.containsKey(u.getLogin())) {
					usuarios.put(u.getLogin(), u);
				}
			}
			
		} catch (ErroNegocio e){
			if (e.getMessage().contains(EnumUsuarioAutenticado.SENHA_INVALIDA.getMensagem()) 
					|| e.getMessage().contains(EnumUsuarioAutenticado.SENHA_INVALIDA_ULTIMA_TENTATIVA.getMensagem())) {
				retorno = JSONReturn.newInstance(Consequence.ERRO).message(e.getMessage());
			} else {
				throw e;
			}
		} catch (ErroUsuario e){
			if (e.getMessage().contains(EnumUsuarioAutenticado.USUARIO_INEXISTENTE.getMensagem())) {
				retorno = JSONReturn.newInstance(Consequence.ERRO).message(EnumUsuarioAutenticado.USUARIO_INEXISTENTE.getMensagem());
			} else {
				throw e;
			}
		}
		
		return retorno;
	}


	public JSONReturn inserir(ServletRequest requisicao, ServletResponse resposta) throws ErroUsuario {
		String nome = "";
		String login = "";
		String senha = "";
		String email = "";
//		String status = "";
		String imei = "";
		
		nome =  getStringDaRequisicao(requisicao, "nome");
		login = getStringDaRequisicao(requisicao, "login");
		senha = getStringDaRequisicao(requisicao, "senha");
		email = getStringDaRequisicao(requisicao, "email");
		imei =  getStringDaRequisicao(requisicao, "imei");
		
		
		Usuario usuario = new Usuario();
		JSONReturn retorno = null;
		try {
			usuario.comNome(nome)
				   .comLogin(login)
				   .comSenha(senha)
				   .comEmail(email)
				   .comImei(imei);
			
			usuario = new BOUsuario().inserir(usuario);
			
			retorno = JSONReturn.newInstance(Consequence.SUCCESSO, usuario)
					.message("Cadastro realizado com sucesso!");
			
//			if (naoExiste(usuario.getLatitude())) { 
				retorno.exclude("latitude");
//			}
//			if (naoExiste(usuario.getLongitude())) { 
				retorno.exclude("longitude"); 
//			}
//			if (naoExiste(usuario.getStatus())) { 
				retorno.exclude("status"); 
//			}
//			if (naoExiste(usuario.getPerfil())) { 
				retorno.exclude("perfil"); 
//			}
//			if (naoExiste(usuario.getAmigos())) { 
				retorno.exclude("amigos"); 
//			}
			
		} catch (ErroUsuario e){
			if (e.getMessage().contains("Este Usuário já existe!")) {
				retorno = JSONReturn.newInstance(Consequence.ERRO).message("Usuario ja existente");
			}
		}
		
		return retorno;
	}
	
	public JSONReturn atualizar(ServletRequest requisicao, ServletResponse resposta) throws ErroUsuario {
		String nome = "";
		String login = "";
		String senha = "";
		String email = "";
		String mensagemDeStatus = "";
		String status = "";
		String imei = "";
		String dataDeNascimento = "";
		
		nome =  getStringDaRequisicao(requisicao, "nome");
		login = getStringDaRequisicao(requisicao, "login");
		senha = getStringDaRequisicao(requisicao, "senha");
		email = getStringDaRequisicao(requisicao, "email");
		imei =  getStringDaRequisicao(requisicao, "imei");
		dataDeNascimento = getStringDaRequisicao(requisicao, "data");
		mensagemDeStatus = getStringDaRequisicao(requisicao, "mensagemDeStatus");
		status = getStringDaRequisicao(requisicao, "status");
		
		boolean sucesso = true;
		
		Usuario usuario = new Usuario();
		JSONReturn retorno = null;
		try {
			usuario.comNome(nome)
			.comLogin(login)
			.comSenha(senha)
			.comEmail(email)
			.comImei(imei);
			
			if (existe(status) && !status.isEmpty()) {
				usuario.comStatus(EnumUsuarioAutenticado.valueOf(status));
			}
			
			if (existe(mensagemDeStatus)) {
				usuario.comMensagemDeStatus(mensagemDeStatus);
			}
			
			if (existe(dataDeNascimento) && !dataDeNascimento.isEmpty()) {
				Calendar data = GregorianCalendar.getInstance();
				data.setTimeInMillis(Long.parseLong(dataDeNascimento));
				usuario.comDataDeNascimento(data);
			}
			
			usuario = new BOUsuario().inserir(usuario);
			
			retorno = JSONReturn.newInstance(Consequence.SUCCESSO, usuario)
					.message("Cadastro realizado com sucesso!");
			
			if (naoExiste(usuario.getStatus())) { 
				retorno.exclude("status"); 
			}
//			if (naoExiste(usuario.getPerfil())) { 
			retorno.exclude("perfil"); 
//			}
			if (naoExiste(usuario.getAmigos())) { 
				retorno.exclude("amigos"); 
			}
			
			retorno = JSONReturn.newInstance(Consequence.SUCCESSO, sucesso);
		} catch (ErroUsuario e){
			e.printStackTrace();
			sucesso = false;
			retorno = JSONReturn.newInstance(Consequence.ERRO, sucesso);
		}
		return retorno;
	}
	
	public JSONReturn pesquisarPorLoginComo(ServletRequest requisicao, ServletResponse resposta) throws ErroUsuario {
		String login = "";
		
		JSONReturn retorno = null;
		login = getStringDaRequisicao(requisicao, "login");
		login = (naoExiste(login) ? "" : login);
		List<Usuario> usuarios = new BOUsuario().pesquisarPorLoginComo(login);
		retorno = JSONReturn.newInstance(Consequence.SUCCESSO, usuarios);
		
		return retorno;
	}
	
	public JSONReturn listarOutrosUsuariosComNomeComo(ServletRequest requisicao, ServletResponse resposta) throws ErroUsuario {
		String nome = "";
		
		JSONReturn retorno = null;
		nome = getStringDaRequisicao(requisicao, "nome");
		nome = (naoExiste(nome)) ? "" : nome;
		
		String idUsuarioDaDessao = getStringDaRequisicao(requisicao, "idUsuario");
		Usuario usuarioDaSessao = null;
		if (existe(idUsuarioDaDessao)) {
			usuarioDaSessao = new BOUsuario().pesquisar(Long.parseLong(idUsuarioDaDessao));
			try {
				List<Usuario> outrosUsuarios = new BOUsuario().listarOutrosUsuariosComNomeComo(nome, usuarioDaSessao);
				retorno = JSONReturn.newInstance(Consequence.SUCCESSO, outrosUsuarios);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return retorno;
	}
	
	public JSONReturn listarAmigosComNomeComo(ServletRequest requisicao, ServletResponse resposta) throws ErroUsuario {
		String nome = "";
		
		JSONReturn retorno = null;
		nome = getStringDaRequisicao(requisicao, "nome");
		nome = (naoExiste(nome)) ? "" : nome;
		
		String idUsuarioDaDessao = getStringDaRequisicao(requisicao, "idUsuario");
		Usuario usuarioDaSessao = null;
		if (existe(idUsuarioDaDessao)) {
			usuarioDaSessao = new BOUsuario().pesquisar(Long.parseLong(idUsuarioDaDessao));
			try {
				List<Usuario> outrosUsuarios = new BOUsuario().listarAmigosComNomeComo(nome, usuarioDaSessao);
				retorno = JSONReturn.newInstance(Consequence.SUCCESSO, outrosUsuarios);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return retorno;
	}
	
	public JSONReturn listarUsuarios(ServletRequest requisicao, ServletResponse resposta) throws ErroUsuario {
		List<Usuario> usuarios = new BOUsuario().listarUsuarios();

		JSONReturn retorno = JSONReturn.newInstance(Consequence.SUCCESSO, usuarios);
		
		return retorno;
	}
	
	public JSONReturn verificarSessao(ServletRequest requisicao, ServletResponse resposta) throws ErroUsuario {
		log("Verificando sessão....");
		Map<String, Usuario> usuarios  = (Map<String, Usuario>) getAtributoDaSessao(Constantes.SESSOES_ABERTAS);
		String login = getStringDaRequisicao(requisicao, "login");
		Boolean estaLogado = false;
		if (existe(usuarios)) {
			estaLogado = usuarios.containsKey(login);
		}
		log("Usuario logado: "+estaLogado);
		return JSONReturn.newInstance(Consequence.SUCCESSO, estaLogado);
	}
	
	public JSONReturn logout(ServletRequest requisicao, ServletResponse resposta) throws ErroUsuario {
		JSONReturn verificacaoSessao = verificarSessao(requisicao, resposta);
		Boolean sessaoAtiva = (Boolean) verificacaoSessao.getDado();
		String login = getStringDaRequisicao(requisicao, "login");
		if (sessaoAtiva) {
			Map<String, Usuario> usuarios  = (Map<String, Usuario>) getAtributoDaSessao(Constantes.SESSOES_ABERTAS);
			log("Deslogando");
			usuarios.remove(login);
			Sessao.addParametro(Constantes.SESSOES_ABERTAS, usuarios);
		}
		return JSONReturn.newInstance(Consequence.SUCCESSO, true);
	}
	
	/*public List<String> listarUsuariosString(){
		List<String> usuarios = new ArrayList<String>();
		for(Usuario u: listarUsuarios()){
			usuarios.add(u.toString());
		}
		return usuarios;
	}*/

	/*@Override
	public void alterar(Usuario usuario) throws ErroUsuario {
		log("Alterando "+getNomeEntidade());
		getDAO().atualizar(usuario);		
	}
	
	public Object liberarUsuario(String login) {
		Usuario u = pesquisarPorLogin(login);
		u.comContadorSenhaInvalida(0);
		u.comStatus(EnumUsuarioAutenticado.SUCESSO);
		alterar(u);
		return "Liberado";
	}

	@Override
	public void excluir(Usuario usuario) throws ErroUsuario {
		log("Excluindo "+getNomeEntidade());
		getDAO().excluir(usuario);		
	}
	
	public Usuario pesquisar(Long id){
		Usuario usuario = null;
		try{
			usuario = getFinder().find(id);
			System.out.println(usuario);
		} finally {
			try {
				getFinder().fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return usuario;
	}
	
	public Usuario pesquisarPorLogin(String login){
		return getFinder().findByLogin(login);
	}
	
	public List<Usuario> pesquisarPorLoginComo(String login) {
		List<Usuario> usuarios = getFinder().findByLoginComo(login);
		
		for (Usuario usuario : usuarios) {
			System.out.println(usuario);
		}
		
		return usuarios;
	}
	
	private void trataSenhaInvalidaDoUsuario(Usuario usuarioRecuperadoDoBD) {
		int numeroDeChancesRestantes = 3 - usuarioRecuperadoDoBD.getContadorSenhaInvalida();
		boolean deveBloquearUsuario = numeroDeChancesRestantes == 0;
		if (deveBloquearUsuario) {
			usuarioRecuperadoDoBD.comStatus(EnumUsuarioAutenticado.USUARIO_BLOQUEADO);				
		}
		getDAO().atualizar(usuarioRecuperadoDoBD);
	}
	
	private boolean ehAdm(Usuario usuarioRecuperadoDoBD) {
		return usuarioRecuperadoDoBD.getPerfil().equals(Role.ADMINISTRADOR);
	}*/
	
}
