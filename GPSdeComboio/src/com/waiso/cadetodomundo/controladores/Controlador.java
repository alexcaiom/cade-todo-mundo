package com.waiso.cadetodomundo.controladores;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;

import com.waiso.cadetodomundo.utils.GerenciadorSessao;
import com.waiso.cadetodomundo.utils.Sessao;
import com.waiso.framework.abstratas.Classe;

public abstract class Controlador extends Classe {

	protected Object getAtributoDaSessao(String nome){
		return Sessao.getInstancia().get(nome);
	}
	
	protected String getStringDaRequisicao(ServletRequest requisicao, String chave) {
		if (existe(requisicao) && existe(requisicao.getParameter(chave))) {
			return (String) requisicao.getParameter(chave);
		}
		return null;
	}
	
}
