package com.waiso.cadetodomundo.controladores;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.waiso.cadetodomundo.orm.bo.BOUsuario;
import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;
import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.cadetodomundo.web.vo.SolicitacaoDeAmizadeVO;
import com.waiso.framework.exceptions.ErroUsuario;
import com.waiso.framework.json.Consequence;
import com.waiso.framework.json.JSONReturn;

public class ControladorSolicitacao extends Controlador {

	public JSONReturn gerarSolicitacao(ServletRequest requisicao, ServletResponse resposta) throws ErroUsuario {
		JSONReturn retorno = null;
		String idUsuario = getStringDaRequisicao(requisicao, "idUsuario");
		String idAmigo = getStringDaRequisicao(requisicao, "idAmigo");
		
		boolean solicitacaoOK = false;
		try{
			SolicitacaoDeAmizade solicitacao = new SolicitacaoDeAmizade();
			Usuario usuario = new BOUsuario().pesquisar(Long.parseLong(idUsuario));
			Usuario amigo = new BOUsuario().pesquisar(Long.parseLong(idAmigo));
			solicitacao.setData(GregorianCalendar.getInstance());
			solicitacao.setUsuarioSolicitante(usuario);
			solicitacao.setAmigo(amigo);
			
			boolean temSolicitacaoDeAmizadeRegistrada = true;
			List<SolicitacaoDeAmizade> solicitacaoGeradas = new BOUsuario().temSolicitacaoGerada(usuario, usuario);
			temSolicitacaoDeAmizadeRegistrada = !solicitacaoGeradas.isEmpty();
			if (!temSolicitacaoDeAmizadeRegistrada) {
				solicitacaoOK = new BOUsuario().gerarSolicitacao(usuario, amigo);
			}
			
			retorno = JSONReturn.newInstance(Consequence.SUCCESSO, solicitacaoOK);
		}catch (ErroUsuario e){
			retorno = JSONReturn.newInstance(Consequence.ERRO).message(e.getMessage());
		}
		
		
		return retorno;
	}
	
	public JSONReturn confirmarSolicitacao(ServletRequest requisicao, ServletResponse resposta) throws ErroUsuario {
		JSONReturn retorno = null;
		
		String idUsuario = getStringDaRequisicao(requisicao, "idUsuario");
		String idAmigo = getStringDaRequisicao(requisicao, "idAmigo");
		
		try {
			Usuario usuario = new BOUsuario().pesquisar(Long.parseLong(idUsuario));
			Usuario amigo = new BOUsuario().pesquisar(Long.parseLong(idAmigo));
			
			boolean solicitacaoOK = new BOUsuario().confirmarSolicitacao(usuario, amigo);
			
			retorno = JSONReturn.newInstance(Consequence.SUCCESSO, solicitacaoOK);
		} catch(ErroUsuario e) {
			retorno = JSONReturn.newInstance(Consequence.ERRO, e).message(e.getMessage());
		}
		
		
		return retorno;
	}

	public JSONReturn listarTodasSolicitacoesDeAmizade(ServletRequest requisicao, ServletResponse resposta) throws ErroUsuario {
		JSONReturn retorno = null;
		
		try {
			List<SolicitacaoDeAmizade> solicitacoesDeAmizade = new BOUsuario().listarTodasSolicitacoesDeAmizade();
			List<SolicitacaoDeAmizadeVO> vos = new ArrayList<SolicitacaoDeAmizadeVO>();
			
			for (SolicitacaoDeAmizade solicitacaoDeAmizade : solicitacoesDeAmizade) {
				vos.add(new SolicitacaoDeAmizadeVO(solicitacaoDeAmizade));
			}
			retorno = JSONReturn.newInstance(Consequence.SUCCESSO, vos);
		}  catch(ErroUsuario e) {
			retorno = JSONReturn.newInstance(Consequence.ERRO, e).message(e.getMessage());
		}
		
		return retorno;
	}
	
	public JSONReturn listarSolicitacoesDeAmizade(ServletRequest requisicao, ServletResponse resposta) throws ErroUsuario {
		JSONReturn retorno = null;
		
		String idUsuario = getStringDaRequisicao(requisicao, "idUsuario");
		
		try {
			Usuario usuario = new BOUsuario().pesquisar(Long.parseLong(idUsuario));
			List<SolicitacaoDeAmizade> solicitacoesDeAmizade = new BOUsuario().listarSolicitacoesDeAmizade(usuario);
			List<SolicitacaoDeAmizadeVO> vos = new ArrayList<SolicitacaoDeAmizadeVO>();
			
			for (SolicitacaoDeAmizade solicitacaoDeAmizade : solicitacoesDeAmizade) {
				vos.add(new SolicitacaoDeAmizadeVO(solicitacaoDeAmizade));
			}
			retorno = JSONReturn.newInstance(Consequence.SUCCESSO, vos);
		} catch(ErroUsuario e) {
			retorno = JSONReturn.newInstance(Consequence.ERRO, e).message(e.getMessage());
		}
		return retorno;
	}
	
}
