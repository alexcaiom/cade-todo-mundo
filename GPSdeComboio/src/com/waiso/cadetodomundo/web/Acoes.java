package com.waiso.cadetodomundo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.startup.Tomcat.ExistingStandardWrapper;

import com.waiso.cadetodomundo.orm.modelos.Usuario;
import com.waiso.framework.GerenciadorDeInstanciasDeClasse;
import com.waiso.framework.abstratas.Classe;
import com.waiso.framework.exceptions.ErroUsuario;
import com.waiso.framework.json.Consequence;
import com.waiso.framework.json.JSONReturn;
import com.waiso.framework.view.Servlet;

/**
 * Servlet implementation class ServletAcoes
 */
public class Acoes extends Servlet<JSONReturn> {
	private static final long serialVersionUID = 1L;
	PrintWriter out = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Acoes() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processar(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processar(request, response);
	}

	private void processar(HttpServletRequest requisicao, HttpServletResponse resposta) throws IOException {
		verificaSessoesDeUsuariosLogados(requisicao);
		JSONReturn retorno = null;
		String nomeDaClasseAserInvocada = requisicao.getParameter("classe");
		Object webClass = null;
		if (nomeDaClasseAserInvocada != null && !nomeDaClasseAserInvocada.isEmpty()) {
			try {
				webClass = GerenciadorDeInstanciasDeClasse.getClasse(nomeDaClasseAserInvocada);
				String metodoASerInvocado = requisicao.getParameter("metodo");
				retorno = executeWebClass(requisicao, resposta, webClass, metodoASerInvocado);
			} catch (ErroUsuario e) {
				e.printStackTrace();
				retorno = JSONReturn.newInstance(Consequence.ERRO).message(e.getMessage());
			} catch (InstantiationException e) {
				e.printStackTrace();
				retorno = JSONReturn.newInstance(Consequence.ERRO).message(e.getMessage());
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				retorno = JSONReturn.newInstance(Consequence.ERRO).message(e.getMessage());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				retorno = JSONReturn.newInstance(Consequence.ERRO).message("Classe nao encontrada!");
			}
		}
		String respostaASerImpressa = Classe.existe(retorno) ? retorno.serialize() : "Nenhuma acao foi informada!";
		getWriter(out, resposta).print(respostaASerImpressa);
	}

	private void verificaSessoesDeUsuariosLogados(HttpServletRequest requisicao) {
		HttpSession sessao = requisicao.getSession();
		
	}

}
