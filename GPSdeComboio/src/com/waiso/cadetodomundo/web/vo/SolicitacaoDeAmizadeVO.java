package com.waiso.cadetodomundo.web.vo;

import com.waiso.cadetodomundo.orm.modelos.SolicitacaoDeAmizade;

public class SolicitacaoDeAmizadeVO extends SolicitacaoDeAmizade{
	
	private Long dataLong;
	private Long dataRespostaLong;
	
	public SolicitacaoDeAmizadeVO(SolicitacaoDeAmizade s) {
		this.setAceita(s.isAceita());
		this.setAmigo(s.getAmigo());
		this.setUsuarioSolicitante(s.getUsuarioSolicitante());
		this.dataLong = s.getData().getTimeInMillis();
		if (s.getDataResposta() != null) {
			this.dataRespostaLong = s.getDataResposta().getTimeInMillis();
		}
	}

	public Long getDataLong() {
		return dataLong;
	}

	public void setDataLong(Long dataLong) {
		this.dataLong = dataLong;
	}

	public Long getDataRespostaLong() {
		return dataRespostaLong;
	}

	public void setDataRespostaLong(Long dataRespostaLong) {
		this.dataRespostaLong = dataRespostaLong;
	}
}
