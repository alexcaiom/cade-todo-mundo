package com.waiso.cadetodomundo.utils;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.waiso.framework.abstratas.Classe;

public class GerenciadorSessao extends Classe {

	private HttpSession sessao = null;
	private static GerenciadorSessao instancia = null;
	
	
	
	public static GerenciadorSessao getInstancia(ServletRequest requisicao){
		if (naoExiste(instancia) && existe(requisicao)) {
			instancia = new GerenciadorSessao();
			HttpSession sessaoAuxiliar = ((HttpServletRequest)requisicao).getSession(true);
			instancia.sessao = sessaoAuxiliar;			
		}
		return instancia;
	}

	public HttpSession getSessao() {
		return sessao;
	}

	public void setSessao(HttpSession sessao) {
		this.sessao = sessao;
	}
	
}
