package com.waiso.cadetodomundo.utils;

import java.util.HashMap;

public class Sessao extends HashMap<String, Object>{

	private static Sessao instancia = null;
	
	public static Sessao addParametro (String nome, Object valor) {
		if (instancia == null) {
			instancia = new Sessao();
		}
		
		instancia.put(nome, valor);
		
		return instancia; 
	}

	public static Sessao removerParametro (String nome) {
		if (instancia == null) {
			instancia = new Sessao();
		}
		
		instancia.remove(nome);
		
		return instancia; 
	}
	
	public static boolean temParametro (String nome) {
		if (instancia == null) {
			instancia = new Sessao();
		}
		
		return instancia.containsKey(nome); 
	}
	
	public static Sessao getInstancia (){
		if (instancia == null) {
			instancia = new Sessao();
		}
		
		return instancia;
	}
	
}
